<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\HostRegisterController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');
//CMS
Route::get('page/{name}', [HomeController::class, 'page'])->name('cms_page');
Route::get('about-us', [HomeController::class, 'aboutUs'])->name('about-us');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
        Route::post('dashboard/check-in-history', [DashboardController::class, 'checkInHistory'])->name('check-in-history');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account'); 

        //Host Register by User
        Route::get('host-register', [HostRegisterController::class, 'hostRegister'])->name('host-register');
        Route::get('host-register/get-city/{state_id}', [HostRegisterController::class, 'getCity'])->name('host-register.get-city');
        Route::get('host-register/get-township/{city_id}', [HostRegisterController::class, 'getTownship'])->name('host-register.get-township');

        Route::post('host-register/store', [HostRegisterController::class, 'store'])->name('host-register.store');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
