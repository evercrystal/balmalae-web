echo "Deploy started"
cd /var/www/laravel
git checkout master
git pull
php artisan migrate
php artisan optimize
php artisan config:clear
echo "Deploy finished"