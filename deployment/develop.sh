echo "Deploy started"
cd /var/www/laravel/develop
git checkout develop
git pull
php artisan migrate
php artisan optimize
php artisan config:clear
echo "Deploy finished"