echo "Deploy started"
cd /var/www/pre-test
git checkout pre-test
git pull
php artisan migrate
php artisan optimize
php artisan config:clear
echo "Deploy finished"