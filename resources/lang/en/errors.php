<?php

return [
    'bad_request' => 'Bad Request',
    'user_not_active' => 'Your account has been invalid or banned by system admin!',
    'user_reset_password' => 'Your password reset mail had been sent to your registered mail.',
    'user_not_found' => 'Can\'t found your email in Our system !',
    'invalid_hash_value' => 'Invalid Hash Valule !',
    'customer_does_not_exist' => 'Customer Does Not Exist !',
    'invalid_request_data' => 'Invalid Data , please check your API request Informations.',
    'user_not_active' => 'Your account has been invalid or banned by system admin!',
    'user_reset_password' => 'Your password reset mail had been sent to your registered mail.',
    'user_not_found' => 'Can\'t found your email in Our system !',
    'unverified_user' => 'Unverified User',
    'invalid_credentials' => 'Invalid Credentials',
    'invalid_credentials_message' => 'These credentials do not match our records.',
];