<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'          => 'Home',
        'logout'        => 'Logout',
        'spread'        => 'Spread',
        'symptoms'      => 'Symptoms',
        'prevention'    => 'Prevention',
        'faq'           => 'FAQ',
        'tnc'           => 'T&CS',
        'about'         => 'About Us',

    ],

    'frontend' => [
        'contact' => 'Contact',
        'dashboard' => 'Dashboard',
        'login' => 'Login',
        'macros' => 'Macros',
        'register' => 'Register',
        'host-register' => 'Host Register',
        'dont_have_acc' => 'Don`t have an account?',
        'have_acc' => 'Have an account?',
        'about-us-content' => 'During the Covid-19 pandemic, most of the public spaces, hospitals and banks record their visitors` information at the entrance. It is good to have a record but using the same pen and paper that have been touched by many people can bring higher risk of spreading virus..
To minimize the risk of getting infected from these pens and papers, we have created a website called “Balmalae” which can be used to record visitors’ information online just by using their phones. We aim to support our nation and government to fight against Covid-19 by making an effort with everything we can.',

        'user' => [
            'account' => 'My Account',
            'administration' => 'Administration',
            'change_password' => 'Change Password',
            'my_information' => 'My Information',
            'profile' => 'Profile',
        ],
    ],

    'hero-section' => [
        'prevention'        => 'CORONAVIRUS PREVENTION',
        'home-q'            => 'Keep yourself at your Home Quarantine',
        'check-in'          => 'Check-In Form',

    ],

    'updates-section' => [
        'pandemic'          => 'COVID-19 CORONAVIRUS PANDEMIC',
        'total-case'        => 'Total cases',
        'new-case'          => 'New cases',
        'recovered'         => 'Total recovered',
        'deaths'            => 'Total deaths',

    ],

    'about-covid' => [
        'about-covid'       => 'About Covid-19',
        'disease'           => 'Coronavirus disease (COVID-19)',
        'content-1'         => 'Coronavirus disease (COVID-19) is an infectious disease caused by a newly
                                discovered coronavirus. Most people infected with the COVID-19 will experience mild to
                                moderate respiratory illness and recover without requiring special
                                treatment. Older people, and those with underlying medical problems like cardiovascular
                                disease, diabetes, chronic respiratory disease, and cancer are more likely to develop
                                serious illness.',
        'content-2'         => 'The best way to prevent and slow down transmission is be well informed about the COVID-19
                                virus, the disease it causes and how it spreads. Protect yourself & others from infection by
                                washing your hands or using an alcohol based rub frequently and not touching your face.',
        'how'               => 'How we Protect?',
    ],

    'transmission' => [
        'transmission'          => 'COVID-19 Transmission',
        'how-spread'            => 'How Coronavirus COVID-19 Spread',
        'content-1'             => 'People can catch COVID-19 from others who have the virus. The disease can spread from person
                                    to person through small droplets from the nose or mouth which are spread when a person with
                                    COVID-19 coughs or exhales.',
        'coughs'                => 'Coughs from Infected Person',
        'sneezes'               => 'Sneezes from Infected Person',
        'touching'              => 'Touching Contaminated Object or Surfaces',
        'close'                 => 'Close Contact with Infected Person',
        'symptoms'              => 'Check COVID-19 Symptoms',

    ],

    'symptoms-section' => [
        'symptoms'              => 'COVID-19 symptoms',
        'symptoms-2'            => 'Coronavirus COVID-19 Symptoms',
        'content'               => 'The most common symptoms of COVID-19 are fever, tiredness, and dry cough. Some patients may
                                    have aches and pains, nasal congestion, runny nose, sore throat or diarrhea.',
        'fever'                 => 'High Fever',
        'fever-content'         => 'According to the WHO, the most common symptoms of Covid-19 are fever. Fever is a key
                                    symptom, experts say.',
        'throat'                => 'Sore Throat',
        'throat-content'        => 'A scratchy sensation in the throat, pain in the throat area that becomes worse when
                                    swallowing or talking.',
        'cough'                 => 'Dry Cough',
        'cough-content'         => 'Basically, a dry cough is "one where no mucus or phlegm is produced with the cough, A cough
                                    that doesn’t go away.',
        'sneezing'              => 'Sneezing',
        'sneezing-content'      => 'Sneezing is very common symptom in Coronavirus patient. Sneezing (sternutation) is the act
                                    of expelling a sudden.',
        'breath'                => 'Shortness of Breath',
        'breath-content'        => 'Commonly known as tightening in the chest, air hunger, difficulty breathing, feeling of
                                    suffocation. These symptoms.',
        'vomiting'              => 'Vomiting',
        'vomiting-content'      => 'It’s not a regular symptoms of coronavirus patients Symptoms that occur with nausea and
                                    vomiting include: abdominal pain.',

    ],

    'prevention-section' => [
        'prevention'            => 'COVID-19 Prevention',
        'prevention-2'          => 'Coronavirus COVID-19 Prevention',
        'content'               => 'You can protect yourself and help prevent spreading the coronavirus (COVID-19) to others if
                                    you do all the step shown as this infographics. You can safe from coronavirus and safe other
                                    if you are affected',
        'wash'                  => 'Wash Your Hands',
        'wash-content'          => 'Wash your hands frequently and regularly with an alcohol based hand rub or wash them with
                                    soap and water.',
        'mask'                  => 'Wear Face Mask',
        'mask-content'          => 'Wear sergical mask and avoid touching face. Hands touch many surfaces and can pick up
                                    viruses once contaminated.',
        'close'                 => 'Avoid Close Contact',
        'close-content'         => 'Please maintain at least 1 metre (3 feet) distance between yourself and anyone who is
                                    coughing or sneezing.',
        'stay'                  => 'Stay at Home',
        'stay-content'          => 'Stay home, stay safe. if you feel unwell. If you have a fever, cough and difficulty
                                    breathing, seek medical attention.',
        'soap'                  => 'Use Soap or Sanitizer',
        'soap-content'          => 'Use alcohol based hand sanitizer or soap when you wash your hands. Keep wash yor hands at
                                    least 20 minute during handwash.',
        'doctor'                => 'Keep Inform Doctors',
        'doctor-content'        => 'If you are think you are suffering with this above symptoms. Please call a doctor as soon as
                                    posible for your medical advice.',
    ],

    'hand-washing' => [
        'subheading'            => 'Hand wash process',
        'heading'               => 'Wash your hands very often',
        'content'               => 'You can protect yourself and help prevent spreading the coronavirus (COVID-19) to others if
                                    you do all the step shown as this infographics. You can safe from coronavirus and safe other
                                    if you are affected',
        'wet'                  => 'Wet Hands',
        'soap'                 => 'Apply Soap',
        'rub'                  => 'Rub Hands Palm to Palm',
        'finger'               => 'Wash Between Fingers',
        'wrists'               => 'Rub Wrists',
        'dry'                  => 'Dry with a Single use Towel',

    ],

    'faq-section' => [
        'subheading'            => 'COVID-19 FAQ',
        'heading'               => 'Frequently Asked Questions',
        'content'               => 'Coronavirus disease (COVID-19) is an infectious disease caused by a newly discovered
                                    coronavirus. Most people infected with the COVID-19 virus will experience',
        'general'               => 'General',
        'prevention'            => 'Prevention',
        'q-1'                   => 'What is Coronavirus (COVID -19) ?',
        'ans-1'                 => 'Coronaviruses are a large family of viruses which may cause illness in
                                    animals or humans. In humans, several coronaviruses are known to cause
                                    respiratory infections ranging from the common cold to more severe diseases
                                    such as Middle East Respiratory Syndrome (MERS) and Severe Acute Respiratory
                                    Syndrome (SARS). The most recently discovered coronavirus causes coronavirus
                                    disease COVID-19.',
        'q-2'                   => 'What are the Symptoms of COVID-19?',
        'ans-2'                 => 'Coronaviruses are a large family of viruses which may cause illness in
                                    animals or humans. In humans, several coronaviruses are known to cause
                                    respiratory infections ranging from the common cold to more severe diseases
                                    such as Middle East Respiratory Syndrome (MERS) and Severe Acute Respiratory
                                    Syndrome (SARS). The most recently discovered coronavirus causes coronavirus
                                    disease COVID-19.',
        'q-3'                   => 'How Does COVID -19 Spread?',
        'ans-3'                 => 'Coronaviruses are a large family of viruses which may cause illness in
                                    animals or humans. In humans, several coronaviruses are known to cause
                                    respiratory infections ranging from the common cold to more severe diseases
                                    such as Middle East Respiratory Syndrome (MERS) and Severe Acute Respiratory
                                    Syndrome (SARS). The most recently discovered coronavirus causes coronavirus
                                    disease COVID-19.',
        'q-4'                   => 'What Can I do to protect myself and prevent the spread of disease?',
        'ans-4'                 => 'Coronaviruses are a large family of viruses which may cause illness in
                                    animals or humans. In humans, several coronaviruses are known to cause
                                    respiratory infections ranging from the common cold to more severe diseases
                                    such as Middle East Respiratory Syndrome (MERS) and Severe Acute Respiratory
                                    Syndrome (SARS). The most recently discovered coronavirus causes coronavirus
                                    disease COVID-19.',

    ],

    'callout-section' => [
        'callout'               => 'For any questions related to COVID-19, you can call “2019” (National Call Centre for COVID-19) 
                                    between 9 AM to 5 PM. You can also discuss directly with doctors by dialing Extension No. 0. The 
                                    discussion period is limited to 6 minutes to minimize the waiting time for everyone.',

    ],

];
