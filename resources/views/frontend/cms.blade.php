@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.tnc'))

@section('content')
    <div class="container" style="padding-top: 180px">
        <h1 class="page-title">
            @if(session('locale') == 'mm')
                {!! $cms->mm_title !!}
            @else
                {!! $cms->title !!}
            @endif
        </h1>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="text-bigger" style="text-align: justify;">
                    @if(session('locale') == 'mm')
                        {!! $cms->mm_content !!}
                    @else
                        {!! $cms->content !!}
                    @endif
                </p>
            </div>
        </div>
        <div class="gap"></div>
    </div>
@endsection

@push('after-scripts')
    @if(config('access.captcha.login'))
        @captchaScripts
    @endif
@endpush
