@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))


@push('after-styles')
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
{{ style('assets/css/google-map.css') }}
<link href="{{ asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" type="text/css"/>
<style type="text/css" media="screen">
    .latitude-box{
        padding-left: 0;
    }
    .longitude-box{
        padding-right: 0;
    }
    .host-register-button{
        min-width: 90px; 
        padding: 7px;
    }
    
</style>
@endpush

@section('content')
<section class="covid-prevention-section py-lg" id="login">
    <div class="container">
    {{ html()->form('POST', route('frontend.user.host-register.store'))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            <strong>
                                @lang('labels.frontend.auth.host_register')
                            </strong>
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <hr />

                <div class="row mt-4 mb-4">
                    <div class="col">
                        
                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.host_category').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('host_category_id') }}

                            <div class="col-md-10">
                                <select name="host_category_id" id="host_category_id" class="form-control select2">
                                    <option value=""></option>
                                    @foreach($host_categories as $host_category)
                                        <option value="{{$host_category->host_category_id}}">{{ $host_category->host_category_name}}</option>
                                    @endforeach
                                </select>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.name').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('host_name') }}

                            <div class="col-md-10">
                                {{ html()->text('host_name')
                                    ->class('form-control')
                                    ->placeholder(__('labels.frontend.host.name'))
                                    ->attribute('maxlength', 100)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.mm_name').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('host_mmname') }}

                            <div class="col-md-10">
                                {{ html()->text('host_mmname')
                                    ->class('form-control')
                                    ->placeholder(__('labels.frontend.host.mm_name'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.mobile').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('host_mobileno') }}

                            <div class="col-md-10">
                                {{ html()->number('host_mobileno')
                                    ->class('form-control')
                                    ->placeholder(__('labels.frontend.host.mobile'))
                                    ->attribute('maxlength', 50)
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.description').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('description') }}

                            <div class="col-md-10">
                                {{ html()->textarea('description')
                                    ->class('form-control')
                                    ->placeholder(__('labels.frontend.host.description'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.mm_description').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('mm_description') }}

                            <div class="col-md-10">
                                {{ html()->textarea('mm_description')
                                    ->class('form-control')
                                    ->placeholder(__('labels.frontend.host.mm_description'))
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.logo').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('host_logo') }}

                            <div class="col-md-10">
                                {{ html()->file('host_logo')
                                    ->class('form-control-file')
                                    ->required() }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.state').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('state_id') }}

                            <div class="col-md-10">
                                <select name="state_id" id="state_id" class="form-control select2">
                                    <option selected value="">Choose State</option>
                                    @foreach($states as $state)
                                        <option value="{{$state->id}}">{{ $state->name}}</option>
                                    @endforeach
                                </select>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.city').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('city_id') }}

                            <div class="col-md-10">
                                <select name="city_id" id="city_id" class="form-control select2" disabled>
                                    <option value=""></option>
                                  
                                </select>
                                <span style="color:red;display:none;" id="city_text">City Not found in this State</span>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.township').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('township_id') }}

                            <div class="col-md-10">
                                <select name="township_id" id="township_id" class="form-control select2" disabled>
                                    <option value=""></option>
                                  
                                </select>
                                <span style="color:red;display:none;" id="township_text">Township Not found in this City</span>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.choose_location').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('choose_location') }}

                            <div class="col-md-10">
                                <input id="pac-input" class="controls" type="text" placeholder="Search Place">
                                <div id="map-canvas" style="width:97%;height:400px;"></div>
                                <div id="ajax_msg"></div>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.location').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('location') }}

                            <div class="col-md-10" style="display: flex;">
                                <div class="col-md-6 latitude-box">
                                    {{ html()->text('latitude')
                                    ->class('form-control')
                                    ->id('input-latitude')
                                    ->placeholder(__('labels.frontend.host.latitude')) }}
                                </div>

                                <div class="col-md-6 longitude-box">
                                    {{ html()->text('longitude')
                                    ->class('form-control')
                                    ->id('input-longitude')
                                    ->placeholder(__('labels.frontend.host.longitude')) }}
                                </div>
                                
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                        {{ html()->label(__('labels.frontend.host.address_detail').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('address_detail') }}

                            <div class="col-md-10">
                                {{ html()->textarea('address_detail')
                                    ->class('form-control')
                                    ->placeholder(__('labels.frontend.host.address_detail'))
                                    ->attribute('maxlength', 191) }}
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('labels.frontend.host.sick_status'))->class('col-md-2 form-control-label')->for('sick_status') }}

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    {{ html()->checkbox('sick_status', true, '1')->class('switch-input') }}
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->

                        <div class="form-group row">
                            {{ html()->label(__('labels.frontend.host.trip_status'))->class('col-md-2 form-control-label')->for('trip_status') }}

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    {{ html()->checkbox('trip_status', true, '1')->class('switch-input') }}
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->

            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        <a class="btn btn-danger btn-sm host-register-button" href="{{url('/')}}">{{__('buttons.general.cancel')}}</a>
                    </div><!--col-->

                    <div class="col text-right">
                        <button class="btn bmltheme btn-sm pull-right host-register-button" type="submit">{{__('buttons.general.crud.create')}}</button>
                    </div><!--row-->
                </div><!--row-->
            </div><!--card-footer-->
        </div><!--card-->
    {{ html()->closeModelForm() }}  
    </div>
</section>
@endsection

@push('after-scripts')

{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script src="{{ asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('appsetting.basic.map_key') }}&libraries=weather,geometry,visualization,places,drawing&callback=initMap" async defer></script>
<script>
     function run_waitMe() {
        $('body').waitMe({
            effect : 'bounce',
            text: 'Please wait',
            bg: 'rgba(255,255,255,0.7)',
            color: '#000',
            sizeW: '',
            sizeH: ''
        });
    }
    function stop_waitMe() {
        $('body').waitMe('hide');
    }
$(document).ready(function(){
    $('#state_id').on('select2:select', function(){
        run_waitMe();
        var stateId = $(this).val();
        if(stateId){
            var city_id= $('#city_id').val();
            $('#city_id').empty();
        
            $('#city_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{ url('host-register/get-city/') }}/"+stateId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {
                        $('#city_id').attr('disabled','disabled');
                        $('#township_id').attr('disabled','disabled');
                        $('#city_id').remove().end();
                        $('#city_id').hide();
                        $('#city_text').show();
                        $('#township_text').show();
                        stop_waitMe();
                    }else {
                        $('#city_id').find('option').remove().end();
                        $('#city_id').removeAttr('disabled','disabled');
                        $('#city_id').show();
                        $('#city_text').hide();
                        $('#township_text').hide();
                        $('#city_id').append($('<option></option>') .attr('selected',true).attr('value',0).text('Seletct City'));
                        $.each(data, function(i, value) {                            
                            $('#city_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#city_id').trigger('change');  
                            stop_waitMe();
                        });    
                            $('#city_id').trigger('change');
                    }  
                }, 
            });
        }else{
        $('#city_id').attr('disabled','disabled');  
        } 
    });
    $('#city_id').on('select2:select', function(){
        run_waitMe();
        var cityId = $(this).val();
        if(cityId){
            var township_id= $('#township_id').val();
            $('#township_id').empty();
        
            $('#township_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{ url('host-register/get-township/') }}/"+cityId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {

                        $('#township_id').attr('disabled','disabled');
                        $('#township_id').find('option').remove().end();
                        $('#township_id').hide();
                        $('#township_text').show();
                        stop_waitMe();

                    }else {
                        $('#township_id').show();
                        $('#township_id').removeAttr('disabled','disabled');
                        $('#township_text').hide();
                        $('#township_id').find('option').remove().end();
                        $('#township_id').append($('<option></option>') .attr('selected',true).attr('value',0).text('Seletct Township'));

                        $.each(data, function(i, value) {

                            $('#township_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#township_id').trigger('change');  
                            stop_waitMe();
                        });    
                        $('#township_id').removeAttr('disabled','disabled');

                            $('#township_id').trigger('change');
                            stop_waitMe();
                    }  
                }, 
            });
        }else{
        $('#township_id').attr('disabled','disabled');  
        } 
    });

});
    $(".select2, .select2-multiple").select2({
        placeholder: 'Choose ',
        width: '100%'
    });

    function initMap() {
        var mapOptions = {
            center: new google.maps.LatLng(16.798703652839684, 96.14947007373053),
            zoom: 13
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

        var marker_position = new google.maps.LatLng(16.798703652839684, 96.14947007373053);
        var input = /** @type {HTMLInputElement} */(
                document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: marker_position,
            draggable: true,
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });


        google.maps.event.addListener(marker, "mouseup", function (event) {
            $('#input-latitude').val(this.position.lat());
            $('#input-longitude').val(this.position.lng());
        });

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setIcon(/** @type {google.maps.Icon} */({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#input-latitude').val(place.geometry.location.lat());
            $('#input-longitude').val(place.geometry.location.lng());

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            $('input[name=address]').val(place.formatted_address);

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);
        });


        google.maps.event.addListener(marker, 'dragend', function() {

            $('#input-latitude').val(place.geometry.location.lat());
            $('#input-longitude').val(place.geometry.location.lng());

        });

    }
    (initMap)();

</script>
@endpush

