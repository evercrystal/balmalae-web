@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
 <section class="covid-prevention-section py-lg s-bg-light" >
    <div class="container">
            <div class="card">
                <div class="card-header">
                    <strong>
                        <i class="fas fa-tachometer-alt"></i> @lang('navs.frontend.dashboard')
                    </strong>
                </div><!--card-header-->

                <div class="card-body">
                    <div class="row">
                        <div class="col col-sm-4 mb-4">
                            <div class="card mb-4 bg-light">
                                <!-- <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture"> -->

                                <div class="card-body">
                                    <h4 class="card-title">
                                        {{ $logged_in_user->user_name }}<br/>
                                    </h4>

                                    <p class="card-text">
                                        <small>
                                            <i class="fas fa-envelope"></i> {{ $logged_in_user->email }}<br/>
                                            <i class="fas fa-calendar-check"></i> @lang('strings.frontend.general.joined') {{ timezone()->convertToLocal($logged_in_user->created_at, 'F jS, Y') }}
                                        </small>
                                    </p>

                                    <p class="card-text">

                                        <a href="{{ route('frontend.user.account')}}" class="btn btn-info btn-sm mb-1">
                                            <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.account')
                                        </a>

                                        @can('view backend')
                                            &nbsp;<a href="{{ route('admin.dashboard')}}" class="btn btn-danger btn-sm mb-1">
                                                <i class="fas fa-user-secret"></i> @lang('navs.frontend.user.administration')
                                            </a>
                                        @endcan
                                    </p>
                                </div>
                            </div>


                        </div><!--col-md-4-->
                        @if($hosts->count() != 0)
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col">
                                    <div class="card mb-4">
                                        <div class="card-header font-weight-bold">
                                            Host List
                                        </div><!--card-header-->

                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="host-table" class="table table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>{{ __('labels.frontend.host.id') }}</th>
                                                        <th>{{ __('labels.frontend.host.name') }}</th>
                                                        <th>{{ __('labels.frontend.host.mm_name') }}</th>
                                                        <th>{{ __('labels.frontend.host.mobile') }}</th>
                                                        <th>{{ __('labels.frontend.host.host_category') }}</th>
                                                        <th>{{ __('labels.frontend.host.township') }}</th>
                                                        <th>{{ __('labels.frontend.host.city') }}</th>
                                                        <th>{{ __('labels.frontend.host.qr') }}</th>
                                                        <th>{{ __('labels.frontend.host.status') }}</th>
                                                        <th>{{ __('labels.frontend.host.last_updated') }}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @forelse($hosts as $host)
                                                            <tr>
                                                                <td>{{ $host->host_id }}</td>
                                                                <td>{{ $host->host_name }}</td>
                                                                <td>{{ $host->host_mmname }}</td>
                                                                <td>{{ $host->host_mobileno }}</td>
                                                                <td>{{ $host->host_category->host_category_name }}</td>
                                                                <td>{{ $host->township->name }}</td>
                                                                <td>{{ $host->city->name }}</td>
                                                                <td>
                                                                    <img src="{{ $host->host_qrcode }}" width="100" height="100">
                                                                </td>
                                                                <td>{!! $host->status_label !!}</td>
                                                                <td>{{ $host->updated_at }}</td>
                                                                
                                                                
                                                            </tr>
                                                        @empty
                                                            <tr>    
                                                                <th colspan="11" style="text-align: center; color: red;">Not Found! There is no host.</th>
                                                            </tr>
                                                        @endforelse
                                                    </tbody>
                                                </table>
                                            </div><!-- table-responsive div-->
                                        </div><!--card-body-->
                                    </div><!--card-->
                                </div><!--col-md-6-->
                            </div><!--row-->

                        </div><!--col-md-12-->
                        @endif
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col">
                                    <div class="card mb-4">
                                        <div class="card-header font-weight-bold">
                                            Check In History
                                        </div><!--card-header-->

                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table id="checkin-table" class="table table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>{{ __('labels.frontend.checkin.id') }}</th>
                                                        <th>{{ __('labels.frontend.checkin.visitor_name') }}</th>
                                                        <th>{{ __('labels.frontend.checkin.visitor_ph') }}</th>
                                                        <th>{{ __('labels.frontend.checkin.host_name') }}</th>
                                                        <th>{{ __('labels.frontend.checkin.is_sick') }}</th>
                                                        <th>{{ __('labels.frontend.checkin.is_trip') }}</th>
                                                        <th>{{ __('labels.frontend.checkin.last_updated') }}</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div><!-- table-responsive div-->
                                        </div><!--card-body-->
                                    </div><!--card-->
                                </div><!--col-md-6-->
                            </div><!--row-->

                        </div><!--col-md-8-->
                    </div><!-- row -->
                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- row -->
</section>
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}
    {{ script("js/plugins.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#checkin-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("frontend.user.check-in-history") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'checkin_id', name: 'checkin_id'},
                    {data: 'visitor_id', name: 'visitor_id'},
                    {data: 'mobile_no', name: 'mobile_no', searchable: false, sortable: false},
                    {data: 'host_id', name: 'host_id'},
                    {data: 'is_sick', name: 'is_sick'},
                    {data: 'is_trip', name: 'is_trip'},
                    {data: 'updated_at', name: 'updated_at'},
                ],
                order: [[0, "asc"]],
                searching: false,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush
