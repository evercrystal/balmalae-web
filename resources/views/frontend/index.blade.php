@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    
<section class="hero-section s-bg-light" id="home">
    <div class="container">
        <div class="row flex-column">
            <div class="col-xl-10 mx-auto">
                <div class="section-header text-center">
                    <p class="subheading">@lang('navs.hero-section.prevention')</p>
                    <h1>
                        @lang('navs.hero-section.home-q') <br>
                        <span class="text-primary">Stay Home, Stay Safe.</span>
                    </h1>
                    <div class="buttons">
                        <a href="{{ route('frontend.auth.check-in-form') }}" class="btn btn-lg btn-primary" data-scroll>@lang('navs.hero-section.check-in')</a>
                        <a href="{{ route('frontend.auth.register') }}" class="btn btn-lg btn-pink" data-scroll>@lang('navs.frontend.register')</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-10 mx-auto">
                <div class="hero-img">
                    <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_wdXBRc.json"
                        background="transparent" speed="1" loop autoplay>
                    </lottie-player>
                </div>
            </div>
        </div>
        <div class="hero-graphics">
            <div class="graphic graphic-1">
                <lottie-player src="https://assets2.lottiefiles.com/packages/lf20_0r0csU.json"
                    background="transparent" speed=".7" style="width: 90px; height: 90px;" loop autoplay>
                </lottie-player>
            </div>
            <div class="graphic graphic-2">
                <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_wpGbRj.json"
                    background="transparent" speed=".7" style="width: 120px; height: 120px;" loop autoplay>
                </lottie-player>
            </div>
            <div class="graphic graphic-3">
                <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_wpGbRj.json"
                    background="transparent" speed=".7" style="width: 120px; height: 120px;" loop autoplay>
                </lottie-player>
            </div>
            <div class="graphic graphic-4">
                <lottie-player src="https://assets2.lottiefiles.com/packages/lf20_0r0csU.json"
                    background="transparent" speed=".7" style="width: 90px; height: 90px;" loop autoplay>
                </lottie-player>
            </div>
        </div>
    </div>
</section>

<!-- Corona news -->
<section class="corona-updates-section">
    <div class="container">
        <div class="corona-updates-inner">
            <div class="section-header text-center mb-4 mb-md-5">
                <h4 class="heading">@lang('navs.updates-section.pandemic')</h4>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-3 statistics-card card-info" data-aos="fade-up">
                    <div class="statistics-card-inner">
                        <h3 class="data-header" id="totalCases"></h3>
                        <p>@lang('navs.updates-section.total-case')</p>
                        <img src="template_assets/img/shapes/1.png" alt="" class="img-fluid shape">
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 statistics-card card-danger" data-aos="fade-up" data-aos-delay="50">
                    <div class="statistics-card-inner">
                        <h3 class="data-header" id="totalDeaths"></h3>
                        <p>@lang('navs.updates-section.deaths')</p>
                        <img src="template_assets/img/shapes/1.png" alt="" class="img-fluid shape">
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 statistics-card card-success" data-aos="fade-up" data-aos-delay="100">
                    <div class="statistics-card-inner">
                        <h3 class="data-header" id="totalRecovered"></h3>
                        <p>@lang('navs.updates-section.recovered')</p>
                        <img src="template_assets/img/shapes/1.png" alt="" class="img-fluid shape">
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 statistics-card card-warning" data-aos="fade-up" data-aos-delay="150">
                    <div class="statistics-card-inner">
                        <h3 class="data-header" id="newCases"></h3>
                        <p>@lang('navs.updates-section.new-case')</p>
                        <img src="template_assets/img/shapes/1.png" alt="" class="img-fluid shape">
                    </div>
                </div>
            </div>

            <div class="text-center mx-auto mt-4 mt-md-5">
                <a href="https://www.worldometers.info/coronavirus/" target="_blank" class="text-link">Check Live
                    Situation Dashboard</a>
            </div>
        </div>
    </div>
</section>

<!-- About -->
<section class="about-covid-section pt-lg" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-xl-6">
                <div class="about-covid">
                    <div class="section-header">
                        <p class="subheading">@lang('navs.about-covid.about-covid')</p>
                        <h2 class="heading">@lang('navs.about-covid.disease')</h2>
                    </div>

                    <p>
                        @lang('navs.about-covid.content-1')
                    </p>

                    <p>
                        @lang('navs.about-covid.content-2')
                    </p>
                    <a href="#prevention" class="btn btn-lg btn-primary mt-3" data-scroll>
                        <i class="las la-shield-alt"></i>
                        @lang('navs.about-covid.how')
                    </a>
                </div>
            </div>
            <div class="col-md-5 offset-xl-1">
                <div class="about-img">
                    <img src="template_assets/img/about/1.png" alt="" class="img-fluid">
                    <div class="about-graphics">
                        <div class="graphic graphic-1">
                            <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_wpGbRj.json"
                                background="transparent" speed=".7" style="width: 60px; height: 60px;" loop
                                autoplay>
                            </lottie-player>
                        </div>
                        <div class="graphic graphic-2">
                            <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_wpGbRj.json"
                                background="transparent" speed=".7" style="width: 100px; height: 100px;" loop
                                autoplay>
                            </lottie-player>
                        </div>
                        <div class="graphic graphic-3">
                            <lottie-player src="https://assets6.lottiefiles.com/packages/lf20_wpGbRj.json"
                                background="transparent" speed=".7" style="width: 80px; height: 80px;" loop
                                autoplay>
                            </lottie-player>
                        </div>
                        <div class="graphic graphic-4">
                            <lottie-player src="https://assets9.lottiefiles.com/packages/lf20_Iz3ReK.json"
                                background="transparent" speed="1" style="width: 80px; height: 80px;" loop autoplay>
                            </lottie-player>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Transmission -->
<section class="covid-transmission-section py-lg" id="transmission">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 mx-auto">
                <div class="section-header text-center">
                    <p class="subheading">@lang('navs.transmission.transmission')</p>
                    <h2 class="heading">@lang('navs.transmission.how-spread')</h2>
                    <p>@lang('navs.transmission.content-1')</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-3">
                <div class="transmission-card">
                    <img src="template_assets/img/transmission/1.png" alt="" class="img-fluid">
                    <h4>@lang('navs.transmission.coughs')</h4>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="transmission-card">
                    <img src="template_assets/img/transmission/2.png" alt="" class="img-fluid">
                    <h4>@lang('navs.transmission.sneezes')</h4>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="transmission-card">
                    <img src="template_assets/img/transmission/3.png" alt="" class="img-fluid">
                    <h4>@lang('navs.transmission.touching')</h4>
                </div>
            </div>
            <div class="col-md-6 col-lg-3">
                <div class="transmission-card">
                    <img src="template_assets/img/transmission/4.png" alt="" class="img-fluid">
                    <h4>@lang('navs.transmission.close')</h4>
                </div>
            </div>
        </div>

        <div class="text-center mx-auto mt-5">
            <a href="#symptoms" class="text-link" data-scroll>@lang('navs.transmission.symptoms')</a>
        </div>
    </div>
</section>

<!-- Symptoms -->
<section class="covid-symptoms-section py-lg s-bg-light" id="symptoms">
    <div class="graphics">
        <div class="graphic graphic-1">
            <img src="template_assets/img/shapes/1.png" alt="">
        </div>
        <div class="graphic graphic-2">
            <img src="template_assets/img/shapes/1.png" alt="">
        </div>
        <div class="graphic graphic-3">
            <img src="template_assets/img/shapes/1.png" alt="">
        </div>
        <div class="graphic graphic-4">
            <img src="template_assets/img/shapes/1.png" alt="">
        </div>
        <div class="graphic graphic-5">
            <img src="template_assets/img/shapes/1.png" alt="">
        </div>
        <div class="graphic graphic-6">
            <img src="template_assets/img/shapes/1.png" alt="">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="section-header text-center">
                    <p class="subheading">@lang('navs.symptoms-section.symptoms')</p>
                    <h2 class="heading">@lang('navs.symptoms-section.symptoms-2')</h2>
                    <p>
                        @lang('navs.symptoms-section.content')
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-4 symptom-card style-2 text-center" data-aos="fade-up">
                <div class="symptom-card-inner h-100">
                    <div class="card-img">
                        <img src="template_assets/img/symptoms/1.png" alt="" class="img-fluid rounded-circle">
                    </div>
                    <h4>@lang('navs.symptoms-section.fever')</h4>
                    <p>
                        @lang('navs.symptoms-section.fever-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 symptom-card style-2 text-center" data-aos="fade-up" data-aos-delay="50">
                <div class="symptom-card-inner h-100">
                    <div class="card-img">
                        <img src="template_assets/img/symptoms/2.png" alt="" class="img-fluid rounded-circle">
                    </div>
                    <h4>@lang('navs.symptoms-section.throat')</h4>
                    <p>
                        @lang('navs.symptoms-section.throat-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 symptom-card style-2 text-center" data-aos="fade-up" data-aos-delay="100">
                <div class="symptom-card-inner h-100">
                    <div class="card-img">
                        <img src="template_assets/img/symptoms/3.png" alt="" class="img-fluid rounded-circle">
                    </div>
                    <h4>@lang('navs.symptoms-section.cough')</h4>
                    <p>
                        @lang('navs.symptoms-section.cough-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 symptom-card style-2 text-center" data-aos="fade-up">
                <div class="symptom-card-inner h-100">
                    <div class="card-img">
                        <img src="template_assets/img/symptoms/4.png" alt="" class="img-fluid rounded-circle">
                    </div>
                    <h4>@lang('navs.symptoms-section.sneezing')</h4>
                    <p>
                        @lang('navs.symptoms-section.sneezing-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 symptom-card style-2 text-center" data-aos="fade-up" data-aos-delay="50">
                <div class="symptom-card-inner h-100">
                    <div class="card-img">
                        <img src="template_assets/img/symptoms/5.png" alt="" class="img-fluid rounded-circle">
                    </div>
                    <h4>@lang('navs.symptoms-section.breath')</h4>
                    <p>
                        @lang('navs.symptoms-section.breath-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 symptom-card style-2 text-center" data-aos="fade-up" data-aos-delay="100">
                <div class="symptom-card-inner h-100">
                    <div class="card-img">
                        <img src="template_assets/img/symptoms/6.png" alt="" class="img-fluid rounded-circle">
                    </div>
                    <h4>@lang('navs.symptoms-section.vomiting')</h4>
                    <p>
                        @lang('navs.symptoms-section.vomiting-content')
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Prevention -->
<section class="covid-prevention-section py-lg" id="prevention">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="section-header text-center">
                    <p class="subheading">@lang('navs.prevention-section.prevention')</p>
                    <h2 class="heading">@lang('navs.prevention-section.prevention-2')</h2>
                    <p>@lang('navs.prevention-section.content')</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-4 text-center prevention-card">
                <div class="prevention-card-inner">
                    <div class="icon">
                        <lottie-player class="mx-auto"
                            src="https://assets1.lottiefiles.com/private_files/lf30_Rq4Htd.json"
                            background="transparent" speed="1" style="width: 180px; height: 180px;" loop autoplay>
                        </lottie-player>
                    </div>
                    <h4>@lang('navs.prevention-section.wash')</h4>
                    <p>
                        @lang('navs.prevention-section.wash-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 text-center prevention-card">
                <div class="prevention-card-inner">
                    <div class="icon">
                        <lottie-player class="mx-auto"
                            src="https://assets1.lottiefiles.com/private_files/lf30_oGbdoA.json"
                            background="transparent" speed="1" style="width: 180px; height: 180px;" loop autoplay>
                        </lottie-player>
                    </div>
                    <h4>@lang('navs.prevention-section.mask')</h4>
                    <p>
                        @lang('navs.prevention-section.mask-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 text-center prevention-card">
                <div class="prevention-card-inner">
                    <div class="icon">
                        <lottie-player class="mx-auto"
                            src="https://assets1.lottiefiles.com/private_files/lf30_QLsD8M.json"
                            background="transparent" speed="1" style="width: 180px; height: 180px;" loop autoplay>
                        </lottie-player>
                    </div>
                    <h4>@lang('navs.prevention-section.close')</h4>
                    <p>
                        @lang('navs.prevention-section.close-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 text-center prevention-card">
                <div class="prevention-card-inner">
                    <div class="icon">
                        <lottie-player class="mx-auto"
                            src="https://assets9.lottiefiles.com/packages/lf20_qFttfS.json" background="transparent"
                            speed="1" style="width: 180px; height: 180px;" loop autoplay>
                        </lottie-player>
                    </div>
                    <h4>@lang('navs.prevention-section.stay')</h4>
                    <p>
                        @lang('navs.prevention-section.stay-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 text-center prevention-card">
                <div class="prevention-card-inner">
                    <div class="icon">
                        <lottie-player class="mx-auto"
                            src="https://assets1.lottiefiles.com/private_files/lf30_yQtj4O.json"
                            background="transparent" speed="1" style="width: 180px; height: 180px;" loop autoplay>
                        </lottie-player>
                    </div>
                    <h4>@lang('navs.prevention-section.soap')</h4>
                    <p>
                        @lang('navs.prevention-section.soap-content')
                    </p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 text-center prevention-card">
                <div class="prevention-card-inner">
                    <div class="icon">
                        <lottie-player class="mx-auto" src="https://assets9.lottiefiles.com/temp/lf20_qCBn5k.json"
                            background="transparent" speed="1" style="width: 180px; height: 180px;" loop autoplay>
                        </lottie-player>
                    </div>
                    <h4>@lang('navs.prevention-section.doctor')</h4>
                    <p>
                        @lang('navs.prevention-section.doctor-content')
                    </p>
                </div>
            </div>
        </div>

        <div class="text-center mx-auto mt-5">
            <a href="https://covid.amcharts.com/" target="_blank" class="text-link">
                Check out Coronavirus Live Situation Dashboard</a>
        </div>
    </div>
</section>

<!-- Callout -->
<section class="callout-section pt-md pb-lg s-bg-primary" id="contact">
    <img src="template_assets/img/bg/1.jpg" alt="" class="parallax-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl-11 mx-auto text-center">
                <div class="callout">
                    <lottie-player class="mx-auto" src="https://assets9.lottiefiles.com/packages/lf20_nKCnOy.json"
                        background="transparent" speed="1" style="width: 200px; height: 200px;" loop autoplay>
                    </lottie-player>
                    <h3 class="text-light" style="line-height: 40px">@lang('navs.callout-section.callout') </h3>
                    <br>
                    <div class="buttons">
                        <a href="#" class="btn btn-lg btn-white">
                            <i class="lab la-facebook-messenger"></i>
                            Live Chat with a Doctor
                        </a>
                        <a href="#" class="btn btn-lg btn-pink">
                            <i class="las la-user-nurse"></i>
                            Book an Appoinment
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Hand washing steps -->
<section class="hand-washing-steps-section style-2 py-lg">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-8 mx-auto">
                <div class="section-header text-center">
                    <p class="subheading">@lang('navs.hand-washing.subheading')</p>
                    <h2 class="heading">@lang('navs.hand-washing.heading')</h2>
                    <p>
                        @lang('navs.hand-washing.content')
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-10 mx-auto">
                <div class="hand-washing-steps style-2">
                    <div class="step">
                        <div class="img-box">
                            <img src="template_assets/img/hand-wash/1.png" alt="" class="img-fluid">
                        </div>
                        <h5>@lang('navs.hand-washing.wet')</h5>
                    </div>
                    <div class="step">
                        <div class="img-box">
                            <img src="template_assets/img/hand-wash/2.png" alt="" class="img-fluid">
                        </div>
                        <h5>@lang('navs.hand-washing.soap')</h5>
                    </div>
                    <div class="step">
                        <div class="img-box">
                            <img src="template_assets/img/hand-wash/3.png" alt="" class="img-fluid">
                        </div>
                        <h5>@lang('navs.hand-washing.rub')</h5>
                    </div>
                    <div class="step">
                        <div class="img-box">
                            <img src="template_assets/img/hand-wash/4.png" alt="" class="img-fluid">
                        </div>
                        <h5>@lang('navs.hand-washing.finger')</h5>
                    </div>
                    <div class="step">
                        <div class="img-box">
                            <img src="template_assets/img/hand-wash/5.png" alt="" class="img-fluid">
                        </div>
                        <h5>@lang('navs.hand-washing.wrists')</h5>
                    </div>
                    <div class="step">
                        <div class="img-box">
                            <img src="template_assets/img/hand-wash/6.png" alt="" class="img-fluid">
                        </div>
                        <h5>@lang('navs.hand-washing.dry')</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="hand-wash-img">
            <lottie-player class="anim-img" src="https://assets4.lottiefiles.com/private_files/lf30_kLBIzB.json"
                background="transparent" speed="1" loop autoplay>
            </lottie-player>
        </div>
    </div>
</section>

<!-- FAQ -->
<section class="faq-section py-lg s-bg-light" id="faq">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-8 mx-auto">
                <div class="section-header text-center">
                    <p class="subheading">@lang('navs.faq-section.subheading')</p>
                    <h2 class="heading">@lang('navs.faq-section.heading')</h2>
                    <p>
                        @lang('navs.faq-section.content')
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="nav flex-row flex-md-column nav-pills faq-tab" id="faq-tab" role="tablist"
                    aria-orientation="vertical">
                    <a class="nav-link active" id="general-tab" data-toggle="pill" href="#general-tab-pane"
                        role="tab">@lang('navs.faq-section.general')</a>
                    <a class="nav-link" id="covid-19-tab" data-toggle="pill" href="#covid-19-tab-pane"
                        role="tab">COVID-19</a>
                    <a class="nav-link" id="prevention-tab" data-toggle="pill" href="#prevention-tab-pane"
                        role="tab">@lang('navs.faq-section.prevention')</a>
                    <!-- <a class="nav-link" id="symptoms-tab" data-toggle="pill" href="#symptoms-tab-pane"
                        role="tab">Symtoms</a>
                    <a class="nav-link" id="medical-tab" data-toggle="pill" href="#medical-tab-pane"
                        role="tab">Medical</a>
                    <a class="nav-link" id="others-tab" data-toggle="pill" href="#others-tab-pane"
                        role="tab">Others</a> -->
                    @if(isset($page))
                    @if($page->page == 'faq')
                    <a class="nav-link" id="balmalae-tab" data-toggle="pill" href="#balmalae-tab-pane"
                        role="tab">{{$page->title}}</a>
                    @endif
                    @endif
                </div>
            </div>
            <div class="col-md-9 col-xl-8 ml-auto">
                <div class="tab-content faq-tab-content" id="faq-tab-content">
                @if(isset($page))
                    @if($page->page == 'faq')
                    <!-- Balmalae FAQ -->
                    <div class="tab-pane fade" id="balmalae-tab-pane" role="tabpanel">
                        <div class="accordion faq-accordion" id="bml-faq-accordion">
                        @foreach($page->my_content as $key=>$rows)
                            <h3>{{ $rows['title'] }}</h3><br>
                            @foreach($rows['result'] as $k=>$row)                               
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#collapse-{{ $key}}{{$k+1}}" aria-expanded="true">
                                        <span>{{ $row[0] }}</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>

                                <div id="collapse-{{ $key}}{{$k+1}}" class="collapse show" data-parent="#bml-faq-accordion">
                                    <div class="card-body">
                                        {!! $row[1] !!}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @endforeach
                        </div>
                    </div>
                    @endif
                @endif
                    <!--  -->

                    <!-- General tab content-->
                    <div class="tab-pane fade show active" id="general-tab-pane" role="tabpanel">
                        <div class="accordion faq-accordion" id="general-accordion">
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#g-collapseOne" aria-expanded="true">
                                        <span>1. @lang('navs.faq-section.q-1')</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>

                                <div id="g-collapseOne" class="collapse show" data-parent="#general-accordion">
                                    <div class="card-body">
                                        @lang('navs.faq-section.ans-1')
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#g-collapseTwo" aria-expanded="false">
                                        <span>2. @lang('navs.faq-section.q-2')</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>
                                <div id="g-collapseTwo" class="collapse" data-parent="#general-accordion">
                                    <div class="card-body">
                                        @lang('navs.faq-section.ans-2')
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#g-collapseThree" aria-expanded="false">
                                        <span>3. @lang('navs.faq-section.q-3')</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>
                                <div id="g-collapseThree" class="collapse" data-parent="#general-accordion">
                                    <div class="card-body">
                                        @lang('navs.faq-section.ans-3')
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#g-collapsefour" aria-expanded="false">
                                        <span>4. @lang('navs.faq-section.q-4')</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>
                                <div id="g-collapsefour" class="collapse" data-parent="#general-accordion">
                                    <div class="card-body">
                                        @lang('navs.faq-section.ans-4')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- COVID-19 tab content-->
                    <div class="tab-pane fade" id="covid-19-tab-pane" role="tabpanel">
                        <div class="accordion faq-accordion" id="covid-19-accordion">
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#c-collapseOne" aria-expanded="true">
                                        <span>1. @lang('navs.faq-section.q-1')</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>

                                <div id="c-collapseOne" class="collapse show" data-parent="#covid-19-accordion">
                                    <div class="card-body">
                                        @lang('navs.faq-section.ans-1')
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                        data-target="#c-collapseTwo" aria-expanded="false">
                                        <span>2. @lang('navs.faq-section.q-2')</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>
                                <div id="c-collapseTwo" class="collapse" data-parent="#covid-19-accordion">
                                    <div class="card-body">
                                        @lang('navs.faq-section.ans-2')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Prevention tab content -->
                    <div class="tab-pane fade" id="prevention-tab-pane" role="tabpanel">
                        <div class="accordion faq-accordion" id="prevention-accordion">
                            <div class="accordion-card">
                                <div class="card-header">
                                    <button class="btn btn-link" type="button" data-toggle="collapse"
                                        data-target="#p-collapseOne" aria-expanded="true">
                                        <span>4. @lang('navs.faq-section.q-4')</span>
                                        <span class="icon"></span>
                                    </button>
                                </div>

                                <div id="p-collapseOne" class="collapse show" data-parent="#prevention-accordion">
                                    <div class="card-body">
                                        @lang('navs.faq-section.ans-4')
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Blog -->
<!-- <section class="blog-section py-lg" id="blog">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-8 col-xl-7 mx-auto">
                <div class="section-header text-center">
                    <p class="subheading">Latest News</p>
                    <h2 class="heading">Recent Blog Post</h2>
                    <p>Coronavirus disease (COVID-19) is an infectious disease caused by a newly discovered
                        coronavirus. Most people infected with the COVID-19 virus will experience</p>
                </div>
            </div>
        </div>

        <div class="row blog-articles">
            <article class="col-md-6 col-lg-4 blog-article" data-aos="fade-up">
                <div class="article-inner">
                    <div class="article-thumb">
                        <a href="article.html">
                            <img src="template_assets/img/blog/1.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="article-details">
                        <div class="article-tags">
                            <a href="#">Treatment</a>
                        </div>
                        <h3 class="article-title"><a href="article.html">How to save yourself from Coronavirus?</a>
                        </h3>
                        <p>
                            Coronaviruses are a large family of viruses which may cause illness in animals or
                            humans. In humans, several COVID -19 are known to cause respiratory.
                        </p>
                    </div>
                    <div class="read-more">
                        <a href="article.html" class="read-more-btn">
                            <i class="las la-plus"></i>
                            <span>Read more</span>
                        </a>
                    </div>
                </div>
            </article>
            <article class="col-md-6 col-lg-4 blog-article" data-aos="fade-up" data-aos-delay="50">
                <div class="article-inner">
                    <div class="article-thumb">
                        <a href="article.html">
                            <img src="template_assets/img/blog/2.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="article-details">
                        <div class="article-tags">
                            <a href="#">COVID -19</a>
                            <a href="#">Coronavirus</a>
                        </div>
                        <h3 class="article-title"><a href="article.html">What can I do to protect myself &
                                prevent the spread of disease?</a></h3>
                        <p>
                            Coronaviruses are a large family of viruses which may cause illness in animals or
                            humans. In humans, several COVID -19 are known to cause respiratory.
                        </p>
                    </div>
                    <div class="read-more">
                        <a href="article.html" class="read-more-btn">
                            <i class="las la-plus"></i>
                            <span>read more</span>
                        </a>
                    </div>
                </div>
            </article>
            <article class="col-md-6 col-lg-4 blog-article d-block d-md-none d-lg-block" data-aos="fade-up"
                data-aos-delay="100">
                <div class="article-inner">
                    <div class="article-thumb">
                        <a href="article.html">
                            <img src="template_assets/img/blog/3.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="article-details">
                        <div class="article-tags">
                            <a href="#">Medical</a>
                        </div>
                        <h3 class="article-title">
                            <a href="article.html">Coronavirus disease (COVID-19)
                                Online training</a>
                        </h3>
                        <p>
                            Coronaviruses are a large family of viruses which may cause illness in animals or
                            humans. In humans, several COVID -19 are known to cause respiratory.
                        </p>
                    </div>
                    <div class="read-more">
                        <a href="article.html" class="read-more-btn">
                            <i class="las la-plus"></i>
                            <span>read more</span>
                        </a>
                    </div>
                </div>
            </article>
        </div>

        <div class="text-center mx-auto mt-5">
            <a href="blog.html" class="text-link">Check out all latest news</a>
        </div>
    </div>
</section> -->

@endsection
