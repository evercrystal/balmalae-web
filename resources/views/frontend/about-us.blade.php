@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.about'))

@section('content')
    <section class="team-section py-lg" id="team">
        <div class="container">

            <div class="row my-5">
                <div class="col-lg-8 col-xl-10 mx-auto">
                    <div class="section-header text-center">
                        <h2 class="heading">About Us</h2>
                        <p class="text-justify">
                            @lang('navs.frontend.about-us-content')
                        </p>
                    </div>
                </div>
            </div>
            <hr>
            <h2 class="text-center">Meet our team</h2>
            <div class="row">

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/th.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Thet Hein </h5>
                        <p class="card-text"> Implementation Director </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/thethein/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>


                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/tyz.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Thura Zaw </h5>
                        <p class="card-text"> Managing Director </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/thurazaw/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/pz.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Phyo Zaw </h5>
                        <p class="card-text"> Business Development Director</p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/phyozaw/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/htay.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Htay Htay Tun </h5>
                        <p class="card-text"> Project Leader </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/htay/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/sai.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Sai Yan Naung </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/sai-yan-naung-378595181" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/tzo.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Thant Zin Oo </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/thant-zin-oo-a324a6158/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/pkk.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Phyo Ko Ko Aung </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/phyokokoaung/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

				<div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/kyaw.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Kyaw Naing Thu </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/kyaw-naing-thu-237142137/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/smz.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Shane Min Zaw </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/shane-min-zaw-262001160/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/mt.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Min Thway </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/min-thway-a27014160/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/ye.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Ye Yint Ohn Kyaing </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/ye-yint-ohn-kyaing-621337149/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/wk.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Win Ko Ko Hein </h5>
                        <p class="card-text"> Software Developer </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/win-ko-ko-hein-599549191" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>
                
                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/aye_su.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Aye Su Linn </h5>
                        <p class="card-text"> Impelmentation </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/ayesu-lynn-a26148176/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/mee.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Khin Myint Myint Thu </h5>
                        <p class="card-text"> Impelmentation </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/khin-myint-myint-thu-679621185/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/aye_htet.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Aye Htet Htet Cho </h5>
                        <p class="card-text"> Impelmentation </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/aye-htet-htet-cho-5a72831b9/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4 col-lg-3 p-3">                                                
                    <div class="team-card ">
                    <img class="img-fluid rounded-circle w-75 m-auto py-3" src="{{url('uploads/about-us/mtk.jpg')}}"  alt="team member">

                    <div class="card-body py-0 text-center">
                        <h5 class="team-member-name card-title mb-1"> Min Thu Khant </h5>
                        <p class="card-text"> Impelmentation </p>
                    </div>
                    <div class="card-body text-center">
                        <a href="https://www.linkedin.com/in/min-thu-khant-961981188/" class="read-more-btn" target="_blank"> <i class="lab la-2x la-linkedin"></i></a>
                    </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@endsection

@push('after-scripts')
    @if(config('access.captcha.login'))
        @captchaScripts
    @endif
@endpush
