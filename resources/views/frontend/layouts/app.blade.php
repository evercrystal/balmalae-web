<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="keywords" content="Balmalae,balmalae">
        <meta name="description" content="@yield('meta_description', 'Balmalae Tracker System')">
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')
        <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('/template_assets/css/bootstrap.min.css') }}">

    <!-- Icon Font -->
    <link rel="stylesheet" href="{{ url('/template_assets/css/line-awesome.min.css') }}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Barlow:ital,wght@0,400;0,500;0,600;0,700;1,400&display=swap"
        rel="stylesheet">

    <!-- AOS -->
    <link rel="stylesheet" href="{{ url('/template_assets/css/aos.css') }}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ url('/template_assets/css/style.css') }}">

    <!-- Custom CSS for end users -->
    <link rel="stylesheet" href="{{ url('/template_assets/css/custom.css') }}">
    <link rel="shortcut icon" href="{{ config('appsetting.basic.favicon') }}">

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        <!-- {{ style(mix('css/frontend.css')) }} -->

        @stack('after-styles')
    </head>
    <body id="body" style="word-wrap: break-word;">
        <!-- Preloader -->
        <div id="preloader-wrapper">
            <div class="preloader-wave-effect"></div>
            <div class="preloader-text">Loading</div>
        </div>

        @include('includes.partials.read-only')

            @include('includes.partials.logged-in-as')
            @include('frontend.includes.nav')

            <!-- <div class="container"> -->    
            @yield('content')
            <!-- </div> -->
            @include('frontend.includes.footer')

        {{--<div class="fixed-container position-fixed">  
            <button class="btn-circle btn-dark" id="btn-theme-switch" data-current_theme="light" data-next_theme="dark">
                <i class="las la-moon text-center d-flex flex-column justify-content-center h-100"></i>
            </button>

            <a href="#body" data-scroll class="btn-circle btn-primary" style="display: none;" id="btn-scroll-up">
                <i class="las la-angle-up text-center d-flex flex-column justify-content-center h-100"></i>
            </a>
        </div>--}}
        <!-- Scripts -->
        @stack('before-scripts')
        <!-- jQuery first, then Bootstrap bundle JS, then others -->
        <script src="{{ url('/template_assets/js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ url('/template_assets/js/bootstrap.bundle.min.js') }}"></script>

        <!-- Plugins -->
        <script src="{{ url('/template_assets/js/jquery-ui.min.js') }}"></script>
        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
        <script src="{{ url('/template_assets/js/corona-script.js') }}"></script>
        <script src="{{ url('/template_assets/js/aos.js') }}"></script>
        <script src="{{ url('/template_assets/js/simpleParallax.min.js') }}"></script>
        <script src="{{ url('/template_assets/js/parallax.min.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAhrdEzlfpnsnfq4MgU1e1CCsrvVx2d59s"></script>
        <script src="{{ url('/template_assets/js/gmaps.js') }}"></script>
        <script src="{{ url('/template_assets/js/ajax-mail.js') }}"></script>
        <script src="{{ url('/template_assets/js/ajax-subscribe.js') }}"></script>

        <!-- Template JS -->
        <script src="{{ url('/template_assets/js/scripts.js') }}"></script>

        <!-- {!! script(mix('js/manifest.js')) !!} -->
        <!-- {!! script(mix('js/vendor.js')) !!} -->
        <!-- {!! script(mix('js/frontend.js')) !!} -->
        <!-- {!! script(mix('js/backend.js')) !!} -->
        @stack('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>
