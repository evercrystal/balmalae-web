<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'BNF Bus V2')">
        <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
        @yield('meta')


        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}

        {{ style('assets/plugins/select2/css/select2.min.css') }}
        {{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
        <link href="{{ asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" type="text/css"/> 

    </head>
    <body>
        @include('includes.partials.read-only')

        <div id="app">
            @include('includes.partials.logged-in-as')

            <div class="container form-position">
                <div class="text-center">
                    <img class="img-custom" src="{{ config('appsetting.basic.main_logo') }}" alt="">
                </div>
                @include('includes.partials.messages')
                {{ html()->form('POST', route('frontend.auth.check-in.store'))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
                    <div class="card check-in-box-shadow">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h4 class="card-title mb-0">
                                        <strong>
                                            @lang('labels.frontend.auth.check_in')
                                        </strong>
                                    </h4>
                                </div><!--col-->
                            </div><!--row-->

                            <hr />

                            <div class="row mt-4 mb-4">
                                <div class="col">
                                    <input type="hidden" name="visitor_id" value="">
                                    <div class="form-group row">
                                    {{ html()->label(__('labels.frontend.host.host_category').' (ရောက်ရှိနေရာအမျိုးအစား)'.'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('host_category_id') }}

                                        <div class="col-md-10">
                                            <select name="host_category_id" id="host_category_id" class="form-control select2">
                                                <option value=""></option>
                                                @foreach($host_categories as $host_category)
                                                    <option value="{{$host_category->host_category_id}}">{{ $host_category->host_category_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div><!--form-group-->

                                    <div class="form-group row">
                                    {{ html()->label(__('labels.frontend.host.host_name').' (ရောက်ရှိနေရာ)'.'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('host_id') }}

                                        <div class="col-md-10">
                                            <select name="host_id" id="host_id" class="form-control select2" disabled>
                                                <option value=""></option>
                                            </select>
                                            <span style="color:red;display:none;" id="host_text">Host Not found in this Host Category</span>
                                        </div><!--col-->
                                    </div><!--form-group-->
                                    @if(auth()->user() != null)
                                        <div class="form-group row">
                                        {{ html()->label(__('labels.frontend.host.name').' (အမည်)'.'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('name') }}

                                            <div class="col-md-10">
                                                {{ html()->text('name')
                                                    ->class('form-control')
                                                    ->placeholder(__('labels.frontend.host.name'))
                                                    ->attribute('maxlength', 100)
                                                    ->attribute('readonly')
                                                    ->value(auth()->user()->user_name)
                                                    ->required() }}
                                            </div><!--col-->
                                        </div><!--form-group-->

                                        <div class="form-group row">
                                        {{ html()->label(__('labels.frontend.host.mobile').' (ဖုန်းနံပါတ်)'.'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('mobile') }}

                                            <div class="col-md-10">
                                                {{ html()->number('mobile')
                                                    ->class('form-control')
                                                    ->placeholder(__('labels.frontend.host.mobile'))
                                                    ->attribute('maxlength', 50)
                                                    ->attribute('readonly')
                                                    ->value(auth()->user()->mobile)
                                                    ->required() }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    @else
                                        <div class="form-group row">
                                        {{ html()->label(__('labels.frontend.host.name').' (အမည်)'.'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('name') }}

                                            <div class="col-md-10">
                                                {{ html()->text('name')
                                                    ->class('form-control')
                                                    ->placeholder(__('labels.frontend.host.name'))
                                                    ->attribute('maxlength', 100)
                                                    ->required() }}
                                            </div><!--col-->
                                        </div><!--form-group-->

                                        <div class="form-group row">
                                        {{ html()->label(__('labels.frontend.host.mobile').' (ဖုန်းနံပါတ်)'.'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('mobile') }}

                                            <div class="col-md-10">
                                                {{ html()->number('mobile')
                                                    ->class('form-control')
                                                    ->placeholder(__('labels.frontend.host.mobile'))
                                                    ->attribute('maxlength', 50)
                                                    ->required() }}
                                            </div><!--col-->
                                        </div><!--form-group-->
                                    @endif

                                    <div class="form-group row">
                                    {{ html()->label(__('labels.frontend.host.address_detail').' (နေရပ်လိပ်စာ)'.'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('address_detail') }}

                                            <div class="col-md-10">
                                                {{ html()->textarea('address')
                                                    ->class('form-control')
                                                    ->placeholder(__('labels.frontend.host.address_detail'))
                                                    ->attribute('maxlength', 191) }}
                                            </div><!--col-->
                                    </div><!--form-group-->
                                    <br><hr>
                                    <div class="form-group row">
                                        <label for="is_trip" class="col-md-8 status-font-size form-control-label font-weight-bold">Have you travelled within 14 days? (လွန်ခဲ့သော ၁၄ ရက်အတွင်း ခရီးသွားလာဖူး ပါသလား)</label>
                                        
                                        <div class="col-md-4">
                                            <label class="switch switch-label switch-pill switch-primary">
                                                {{ html()->checkbox('is_trip', false, '0')->class('switch-input') }}
                                                <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                            </label>
                                        </div><!--col-->
                                    </div><!--form-group-->
                                    <div class="form-group row">
                                        <label for="is_sick" class="col-md-8 status-font-size form-control-label font-weight-bold">Do you have a fever or experienced a fever within the past 14 days? (လွန်ခဲ့သော ၁၄ ရက်အတွင်း ဖျားနာခဲ့ဖူး ပါသလား)</label>
                                        
                                        <div class="col-md-4">
                                            <label class="switch switch-label switch-pill switch-primary">
                                                {{ html()->checkbox('is_sick', false, '0')->class('switch-input') }}
                                                <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                            </label>
                                        </div><!--col-->
                                    </div><!--form-group-->

                                </div><!--col-->
                            </div><!--row-->
                        </div><!--card-body-->

                        <div class="card-footer">
                            <div class="row">
                                <div class="col">
                                    {{ form_cancel(url('/'), __('buttons.general.cancel')) }}
                                </div><!--col-->

                                <div class="col text-right">
                                    {{ form_submit(__('buttons.general.crud.create')) }}
                                </div><!--row-->
                            </div><!--row-->
                        </div><!--card-footer-->
                    </div><!--card-->
                {{ html()->closeModelForm() }}  
            </div><!-- container -->
        </div><!-- #app -->

        <!-- Scripts -->
        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {!! script(mix('js/frontend.js')) !!}
        {!! script(mix('js/backend.js')) !!}
        {{ script('assets/plugins/select2/js/select2.full.min.js')}}
        {{ script("assets/plugins/select2/component/components-select2.js") }}
        <script src="{{ asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>
        <script>
             function run_waitMe() {
                $('body').waitMe({
                    effect : 'bounce',
                    text: 'Please wait',
                    bg: 'rgba(255,255,255,0.7)',
                    color: '#000',
                    sizeW: '',
                    sizeH: ''
                });
            }
            function stop_waitMe() {
                $('body').waitMe('hide');
            }

            $('#host_category_id').on('select2:select', function(){
                run_waitMe();
                var host_category_id = $(this).val();
                if(host_category_id){
                    var host_id= $('#host_id').val();
                    $('#host_id').empty();
                
                    $('#host_id').removeAttr('disabled','disabled');
                    $.ajax({
                        url: "{{ url('check-in/get-host/') }}/"+host_category_id,
                        type: 'GET',
                        success: function (data){
                            if(data.length == 0) {

                                $('#host_id').attr('disabled','disabled');
                                $('#host_id').find('option').remove().end();
                                $('#host_id').hide();
                                $('#host_text').show();
                                stop_waitMe();

                            }else {
                                $('#host_id').show();
                                $('#host_id').removeAttr('disabled','disabled');
                                $('#host_text').hide();
                                $('#host_id').find('option').remove().end();
                                $('#host_id').append($('<option></option>') .attr('selected',true).attr('value',0).text('Seletct Host'));

                                $.each(data, function(i, value) {

                                    $('#host_id').append($('<option></option>').attr('value', value.host_id).text(value.host_name ));
                                    $('#host_id').trigger('change');  
                                    stop_waitMe();
                                });    
                                $('#host_id').removeAttr('disabled','disabled');

                                    $('#host_id').trigger('change');
                                    stop_waitMe();
                            }  
                        }, 
                    });
                }else{
                $('#host_id').attr('disabled','disabled');  
                } 
            });

        </script>

        @include('includes.partials.ga')
    </body>
</html>






