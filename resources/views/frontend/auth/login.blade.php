@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')

<section class="team-section py-lg my-5" id="team">
    <div class="container">

        <div class="row my-5"> 
                <!--  -->
            <div class="col-sm-12 col-lg-8 col-xl-5 m-auto">
                <div class="contact-form p-4 m-0 shadow-lg rounded-lg">
                    {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">

                                    {{ html()->text('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.emailorphone'))
                                        ->required() }}
                                    
                                    @error('email')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">

                                    {{ html()->password('password')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password'))
                                        ->required() }}
                                    @error('password')
                                        <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group clearfix">
                                    
                                    <button class="btn btn-block bmltheme btn-sm pull-right" type="submit">
                                        {{__('labels.frontend.auth.login_button')}}
                                    </button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        @if(config('access.captcha.login'))
                            <div class="row">
                                <div class="col">
                                    @captcha
                                    {{ html()->hidden('captcha_status', 'true') }}
                                </div><!--col-->
                            </div><!--row-->
                        @endif

                        <div class="row">
                            <div class="col">
                                <div class="form-group text-right">
                                    <a class ="btn btn-block bg-light text-black-50" href="{{ route('frontend.auth.password.reset') }}">@lang('labels.frontend.passwords.forgot_password')</a>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    {{ html()->form()->close() }}
                    <hr>

                    <div class="form-group text-center my-5">
                        <span class="d-inline-block">  @lang('navs.frontend.dont_have_acc')
                            <a class="text-link" href="{{route('frontend.auth.register')}}">@lang('navs.frontend.register')</a>
                        </span>
                    </div>
                </div><!-- contact-form -->
            </div><!-- col-sm-12 -->
        </div><!-- row -->
    </div> <!-- container -->
        <!-- </div> -->
</section>
@endsection

@push('after-scripts')
    @if(config('access.captcha.login'))
        @captchaScripts
    @endif
@endpush
