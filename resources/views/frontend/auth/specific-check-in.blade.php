@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.check_in'))

@push('after-styles') 

@endpush

@section('content')

            <section class="section py-lg my-5" style="min-height:600px;" id="team">
                <div class="container">

                    <div class="row my-5"> 
                    <div class="col-md-8 m-auto">
                        <div class="checkin-form p-3 m-0 shadow-lg rounded-lg">
               
                        {{ html()->form('POST', route('frontend.auth.check-in.store'))->class('needs-validation')->attribute('enctype', 'multipart/form-data')->open() }}
                        <input type="hidden" name="visitor_id" value="">
                        
                        <input type="hidden" class="host-info" value="{{ $host->host_id }}" id="host_id" name="host_id">

                            <div class="form-group text-right mb-0">
                                <a href="{{ url('/check-in') }}" class="btn btn-sm btn-light"> {{ trans('buttons.general.change_host') }} </a>
                            </div>

                            <div class="list-group host-list" id="host_list">

                                <a href="#" class="btn-select-host list-group-item list-group-item-action my-3 active" data-host="{{ $host->host_id }}">
                                    <strong>{{ $host->host_name }}</strong>
                                    <p class="m-0"> {{ $host->address_detail }} </p>
                                </a>
                            
                            </div>

                            <div class="checkin-step2">

                            <div class="form-group mt-5 mb-2">
                                <strong>Please fill your informations</strong>
                            </div>
                            @if(auth()->user() != null)
                                <div class="form-group">
                                {{ html()->label(__('labels.frontend.host.name').' (အမည်)'.'<span class="text-danger">*</span>')->class('form-control-label')->for('name') }}

                                {{ html()->text('name')
                                    ->class('form-control')
                                    ->attribute('maxlength', 100)
                                    ->attribute('readonly')
                                    ->value(auth()->user()->user_name)
                                    ->required() }}
                                </div><!--col-->

                                <div class="form-group ">
                                {{ html()->label(__('labels.frontend.host.mobile').' (ဖုန်းနံပါတ်)'.'<span class="text-danger">*</span>')->class('form-control-label')->for('mobile') }}

                                {{ html()->number('mobile')
                                    ->class('form-control')
                                    ->attribute('maxlength', 50)
                                    ->attribute('readonly')
                                    ->value(auth()->user()->mobile)
                                    ->required() }}

                                    @error('mobile')
                                    <p class="text-danger">{{ $message }}</p>
                                    @enderror
                                </div><!--col-->
                                
                            @else
                            
                                <div class="form-group">
                                
                                {{ html()->label(__('labels.frontend.host.name').' (အမည်)'.'<span class="text-danger">*</span>')->class('form-control-label')->for('name') }}

                                {{ html()->text('name')
                                ->class('form-control')
                                ->attribute('maxlength', 100)
                                ->required() }}
                                </div><!--col-->


                                <div class="form-group">
                                {{ html()->label(__('labels.frontend.host.mobile').' (ဖုန်းနံပါတ်)'.'<span class="text-danger">*</span>')->class('form-control-label')->for('mobile') }}

                                {{ html()->number('mobile')
                                ->class('form-control')
                                ->attribute('maxlength', 50)
                                ->required() }}

                                @error('mobile')
                                <p class="text-danger">{{ $message }}</p>
                                @enderror
                                </div><!--col-->
                                
                            @endif

                            <div class="form-group">
                                {{ html()->label(__('labels.frontend.host.address_detail').' (နေရပ်လိပ်စာ)'.'<span class="text-danger">*</span>')->class('form-control-label')->for('address_detail') }}
                                {{ html()->textarea('address')
                                    ->class('form-control')
                                    ->attribute('maxlength', 191)
                                    ->required()
                                }}
                            </div><!--col-->

                            @if($host->is_trip == 1) 
                            <div class="form-group">
                 
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="is_trip"  name="is_trip">
                                    <label class="custom-control-label" for="is_trip"> Have you travelled within 14 days? (လွန်ခဲ့သော ၁၄ ရက်အတွင်း ခရီးသွားလာဖူး ပါသလား)</label>
                                </div>
                                
                            </div><!--form-group-->
                            @endif

                            @if($host->is_sick == 1) 
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="is_sick"  name="is_sick">
                                    <label class="custom-control-label" for="is_sick">Do you have a fever or experienced a fever within the past 14 days? (လွန်ခဲ့သော ၁၄ ရက်အတွင်း ဖျားနာခဲ့ဖူး ပါသလား)</label>
                                </div>
                            </div><!--form-group-->
                            @endif
                        
                            <div class="row mb-3">
                                <div class="col">
                                    <button type="submit" class="btn btn-primary btn-block">{{ trans('buttons.general.crud.check_in') }}</button>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col">
                                    <a href="{{ url('/') }}" class="btn btn-block bg-light text-black-50">{{ trans('buttons.general.cancel') }}</a>
                                </div>
                            </div>
                            </div>
                            
                        {{ html()->closeModelForm() }}  
                    </div>
                    </div>
            </div>
            </section>
    
@endsection

@push('after-scripts')

<script>

</script>
@endpush
     







