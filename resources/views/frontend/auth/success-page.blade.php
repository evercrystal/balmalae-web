@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.check_in'))

@push('after-styles') 

@endpush

@section('content')
<section class="fullscreen py-lg" id="team">
     
    
        <div class="container">
            <div class="row my-5">
            
            <div class="col-sm-12 col-lg-8 col-xl-5 m-auto">
            <div class="p-4 m-0 shadow-lg rounded-lg">
            @if ($status != 'fail')

                <div class="container mb-5">
                    <lottie-player src="https://assets4.lottiefiles.com/packages/lf20_q4yld5m8.json"  background="transparent"  speed="1"  style="width: 200px; height: 200px; margin:auto;"  loop autoplay></lottie-player>
                    <h4 class="text-center"> Thank You </h4>
                    <p class="my-3 text-center">  
                        Checked in at <strong>{{$host->host_name}}</strong> 
                        <br> 
                        {{ $checkin->updated_at->toDayDateTimeString() }}
                    </p>
                </div>
                
                <div class="row mb-3">
                    <div class="col">
                        <a href="{{ url('/') }}" class="btn btn-primary btn-block">Back to Home</a>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <a href="{{ url('check-in') }}" class="btn btn-light btn-block"> Check-in again</a>
                    </div>
                </div>

            @else

                <div class="container mb-5">
                    <lottie-player src="https://assets2.lottiefiles.com/temp/lf20_QYm9j9.json"  background="transparent"  speed="1"  style="width: 200px; height: 200px; margin:auto;"  loop autoplay></lottie-player>
                    <h4 class="text-center"> Oops! </h4>
                    <p class="my-3 text-center">  
                        This host is not available.
                    </p>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <a href="{{ url('check-in') }}" class="btn btn-primary btn-block"> Check-in again</a>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <a href="{{ url('/') }}" class="btn btn-light btn-block">Back to Home</a>
                    </div>
                </div>

            @endif
            </div>
            </div>

            </div>
        </div>

</section>
@endsection


@push('after-scripts')

@endpush
               
                         
                     

