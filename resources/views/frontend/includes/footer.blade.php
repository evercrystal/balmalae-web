<footer class="footer">
    <!-- <div class="footer-top s-bg-dark-blue py-md">
        <div class="graphics">
            <div class="graphic graphic-1">
                <img src="assets/img/shapes/1.png" alt="">
            </div>
            <div class="graphic graphic-2">
                <img src="assets/img/shapes/1.png" alt="">
            </div>
            <div class="graphic graphic-3">
                <img src="assets/img/shapes/1.png" alt="">
            </div>
            <div class="graphic graphic-4">
                <img src="assets/img/shapes/1.png" alt="">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3 footer-widget order-1">
                    <div class="footer-widget-inner">
                        <a href="" class="footer-logo">
                            <img src="assets/img/logo/logo-white.png" alt="" class="img-fluid">
                        </a>
                        <p>Covira has been developed on the besis of guidelines from the WHO and WHFW for
                            prevention of all human from Coronavirus COVID -19 diseases. </p>
                        <ul class="social">
                            <li>
                                <a href="#"><i class="lab la-facebook-f"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="lab la-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="lab la-linkedin-in"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="lab la-youtube"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 footer-widget order-3 order-lg-2 mt-5 mt-lg-0">
                    <div class="footer-widget-inner">
                        <h4 class="f-widget-title">Situation updates</h4>
                        <ul>
                            <li><a href="#">Disease outbreak news</a></li>
                            <li><a href="#">COVID-19 situation dashboard</a></li>
                            <li><a href="#">Coronavirus pandemic</a></li>
                            <li><a href="#">Global situation dashboard</a></li>
                            <li><a href="#">Youtube live situation reports</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 footer-widget order-4 order-lg-3 mt-5 mt-lg-0">
                    <div class="footer-widget-inner">
                        <h4 class="f-widget-title">Useful Links</h4>
                        <ul>
                            <li><a href="#">Directors Board</a></li>
                            <li><a href="#">World Health Report</a></li>
                            <li><a href="#">Corona Pandemic</a></li>
                            <li><a href="#">Member States</a></li>
                            <li><a href="blog.html">Latest News</a></li>
                            <li><a href="#symptoms" data-scroll>Symptoms</a></li>
                            <li><a href="#prevention" data-scroll>Prevention</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 footer-widget covid-fund order-2 order-lg-4 mt-5 mt-md-0">
                    <div class="footer-widget-inner">
                        <h4 class="f-widget-title">COVID-19 Response Fund</h4>
                        <h5>We need your Help</h5>
                        <p>Would You like to Help people ? Third World
                            Country people need your help</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="las la-donate"></i>
                            Donate Us
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <div class="footer-bottom py-5" style="background-color: #1c7de9">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 mx-auto text-center">
                    <div class="f-bottom-info">
                        <h6 class="text-white">Copyright &copy; {{date('Y')}} <a href="{{ url('/') }}">{{app_name()}}</a>. All rights
                            reserved.</h6>
                        <span style="color: #d4e5ff ">Created by</span> <a href="https://evercrystal.com" target="_blank" style="color: #611c95;">Evercrystal IT Solution</a>
                        <p class="mb-0" style="color: #d4e5ff">All informations are collected from the World Health Organaization (WHO).
                            <br>
                            If you have any concerns please visit the respective website.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>