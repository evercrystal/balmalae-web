<nav class="navbar navbar-expand-lg navbar-light fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{ route('frontend.index') }}">
            <img src="{{ url('/template_assets/img/logo/balmalal_logo.png') }}" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent"
            aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="las la-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarContent">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                    <a class="nav-link {{ active_class(Route::is('frontend.index')) }}" href="{{ route('frontend.index') }}" data-scroll>@lang('navs.general.home')</a>
                </li>
                @if (\Request::path() == '/')
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuUser" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">COVID-19</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuUser">
                            <a class="dropdown-item" href="#about" data-scroll>COVID-19</a>
                            <a class="dropdown-item" href="#transmission" data-scroll>@lang('navs.general.spread')</a>
                            <a class="dropdown-item" href="#symptoms" data-scroll>@lang('navs.general.symptoms')</a>
                            <a class="dropdown-item" href="#prevention" data-scroll>@lang('navs.general.prevention')</a>
                            <a class="dropdown-item" href="#contact" data-scroll>@lang('navs.frontend.contact')</a>
                            <a class="dropdown-item" href="#faq" data-scroll>@lang('navs.general.faq')</a>
                        </div>
                    </li>
                    {{--<li class="nav-item">
                        <a class="nav-link" href="#about" data-scroll>COVID-19</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#transmission" data-scroll>@lang('navs.general.spread')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#symptoms" data-scroll>@lang('navs.general.symptoms')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#prevention" data-scroll>@lang('navs.general.prevention')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact" data-scroll>@lang('navs.frontend.contact')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#faq" data-scroll>@lang('navs.general.faq')</a>
                    </li>--}}
                @else
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuUser" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">COVID-19</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuUser">
                            <a class="dropdown-item" href="{{ route('frontend.index') }}#about" data-scroll>COVID-19</a>
                            <a class="dropdown-item" href="{{ route('frontend.index') }}#transmission" data-scroll>@lang('navs.general.spread')</a>
                            <a class="dropdown-item" href="{{ route('frontend.index') }}#symptoms" data-scroll>@lang('navs.general.symptoms')</a>
                            <a class="dropdown-item" href="{{ route('frontend.index') }}#prevention" data-scroll>@lang('navs.general.prevention')</a>
                            <a class="dropdown-item" href="{{ route('frontend.index') }}#contact" data-scroll>@lang('navs.frontend.contact')</a>
                            <a class="dropdown-item" href="{{ route('frontend.index') }}#faq" data-scroll>@lang('navs.general.faq')</a>
                        </div>
                    </li>
                    {{--<li class="nav-item">
                        <a class="nav-link" href="{{ route('frontend.index') }}#about" data-scroll>COVID-19</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('frontend.index') }}#transmission" data-scroll>@lang('navs.general.spread')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('frontend.index') }}#symptoms" data-scroll>@lang('navs.general.symptoms')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('frontend.index') }}#prevention" data-scroll>@lang('navs.general.prevention')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('frontend.index') }}#contact" data-scroll>@lang('navs.frontend.contact')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('frontend.index') }}#faq" data-scroll>@lang('navs.general.faq')</a>
                    </li>--}}
                @endif

                <li class="nav-item">
                    <a href="{{ url('page/terms-conditions') }}" class="nav-link {{ active_class(Route::is('frontend.cms_page','terms-conditions')) }}">@lang('navs.general.tnc')</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('frontend.about-us') }}" class="nav-link {{ active_class(Route::is('frontend.about-us')) }}">@lang('navs.general.about')</a>
                </li>

                @if(config('locale.status') && count(config('locale.languages')) > 1)
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">@lang('menus.language-picker.language') ({{ strtoupper(app()->getLocale()) }})</a>
                        @include('includes.partials.lang')
                        
                    </li>
                @endif
                
                @guest
                    <li class="nav-item"><a href="{{route('frontend.auth.login')}}" class="nav-link {{ active_class(Route::is('frontend.auth.login')) }}">@lang('navs.frontend.login')</a></li>

                    @if(config('access.registration'))
                        <li class="nav-item"><a href="{{route('frontend.auth.register')}}" class="nav-link {{ active_class(Route::is('frontend.auth.register')) }}">@lang('navs.frontend.register')</a></li>
                    @endif
                @else
                
                    @if(config('access.registration'))
                        <li class="nav-item">
                            <a href="{{route('frontend.user.host-register')}}" class="nav-link {{ active_class(Route::is('frontend.user.host-register')) }}">@lang('navs.frontend.host-register')</a>
                        </li>
                    @endif
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuUser" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">{{ $logged_in_user->user_name }}</a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuUser">
                            @can('view backend')
                                <a href="{{ route('admin.dashboard') }}" class="dropdown-item">@lang('navs.frontend.user.administration')</a>
                            @endcan
                            @auth
                                <a class="dropdown-item {{ active_class(Route::is('frontend.user.dashboard')) }}" href="{{route('frontend.user.dashboard')}}" data-scroll>@lang('navs.frontend.dashboard')</a>
                            @endauth
                            <a href="{{ route('frontend.user.account') }}" class="dropdown-item {{ active_class(Route::is('frontend.user.account')) }}">@lang('navs.frontend.user.account')</a>
                            <a href="{{ route('frontend.auth.logout') }}" class="dropdown-item">@lang('navs.general.logout')</a>
                        </div>
                    </li>
                @endguest

            <!-- <li class="nav-item">
                <a class="nav-link {{ active_class(Route::is('frontend.contact')) }}" href="{{ route('frontend.contact') }}" data-scroll>Contact Us</a>
            </li> -->

            </ul>
            <!-- <div class="navbar-cta d-none d-lg-block">
                <a href="#contact" class="btn btn-primary" data-scroll>
                    <i class="las la-headset"></i>
                    <span>Online Help</span>
                </a>
            </div> -->
        </div>
    </div>
</nav>