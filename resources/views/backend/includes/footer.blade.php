<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }}
            <a href="/">
                @lang('strings.backend.general.app_link')
            </a>
        </strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

    <div class="ml-auto">Power by <a href="http://evercrystal.com/" target="_blank">Ever Crystal</a></div>
</footer>
