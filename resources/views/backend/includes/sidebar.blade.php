<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>


            @foreach(Module::group() as $module)
                @if($module->enabled())
                    <?php 
                        $module = $module->getLowerName();
                        $route = 'admin.'.$module.'.index';
                        $active = 'admin/'.$module.'*';
                        $mod_trans = $module.'::menus.backend.sidebar.'.$module;
                    ?>
                    @can('manage '.$module)
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is($active)) }}" href="{{ route($route) }}">
                                <i class="{{ config($module.'.icon') }}"></i>
                                <span>{{ trans($mod_trans) }}</span>
                            </a>
                        </li>
                    @endcan
                @endif
            @endforeach

            <li class="divider"></li>

            @if ($logged_in_user->isAdmin())
                <li class="nav-title">
                    @lang('menus.backend.sidebar.system')
                </li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Route::is('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Route::is('admin/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/log-viewer*'), 'open')
                }}">
                        <a class="nav-link nav-dropdown-toggle {{
                            active_class(Route::is('admin/log-viewer*'))
                        }}" href="#">
                        <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer'))
                        }}" href="{{ route('log-viewer::dashboard') }}">
                                @lang('menus.backend.log-viewer.dashboard')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Route::is('admin/log-viewer/logs*'))
                        }}" href="{{ route('log-viewer::logs.list') }}">
                                @lang('menus.backend.log-viewer.logs')
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
            
            <?php 
                $module = 'recyclebin';
                $route = 'admin.'.$module.'.index';
                $active = 'admin/'.$module.'*';
                $mod_trans = $module.'::menus.backend.sidebar.'.$module;
            ?>
            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{ active_class(Request::is($active), 'open') }}">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="{{ config($module.'.icon') }}"></i> {{ trans($mod_trans) }}
                    </a>

                    <ul class="nav-dropdown-items">
                    @foreach(Module::getOrdered() as $submodule)
                        @can('manage '.$submodule->getLowerName())
                            @if($submodule->enabled() && !in_array($submodule->getLowerName(),config('backend.ignored_bin')))
                            <?php 
                                $mod_sub_trans = $submodule->getLowerName().'::menus.backend.sidebar.'.$submodule->getLowerName().'_bin';
                                $active = url('/admin/recyclebin?submodule='.$submodule->getName());
                            ?>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::fullUrl() == $active) ? 'active' : '' }}" href="{{ route($route,['submodule' => $submodule->getName() ]) }}">
                                    <i class="{{ config($submodule->getLowerName().'.icon') }}"></i>  {{ trans($mod_sub_trans) }}
                                </a>
                            </li>
                            @endif
                        @endcan
                    @endforeach
                    </ul>
                </li>
            @endif
            <li class="divider"></li>

            <li class="nav-title">
                Setup
            </li>
            
            @foreach(Module::group(1) as $module)
                @if($module->enabled())
                    <?php 
                        $module = $module->getLowerName();
                        $route = 'admin.'.$module.'.index';
                        $active = 'admin/'.$module.'*';
                        $mod_trans = $module.'::menus.backend.sidebar.'.$module;
                    ?>
                    @can('manage '.$module)
                        <li class="nav-item">
                            <a class="nav-link {{ active_class(Route::is($active)) }}" href="{{ route($route) }}">
                                <i class="{{ config($module.'.icon') }}"></i>
                                <span>{{ trans($mod_trans) }}</span>
                            </a>
                        </li>
                    @endcan
                @endif
            @endforeach

            <li class="nav-title">
                Features
            </li>
            
            @foreach(Module::group(2) as $module)
                @if($module->enabled())
                    <?php 
                        $module = $module->getLowerName();
                        $route = 'admin.'.$module.'.index';
                        $active = 'admin/'.$module.'*';
                        $mod_trans = $module.'::menus.backend.sidebar.'.$module;
                    ?>
                    @can('manage '.$module)
                        @if($module == "order")
                            <li class="nav-item nav-dropdown {{ active_class(Route::is('admin/order*'), 'open') }}">
                                <a class="nav-link nav-dropdown-toggle {{ active_class(Route::is('admin/order/order*')) }}" href="#">
                                    <i class="{{ config($module.'.icon') }}"></i>
                                    {{ trans($module.'::menus.backend.sidebar.order') }}

                                    @if ($pending_approval > 0)
                                        <span class="badge badge-danger">{{ $pending_approval }}</span>
                                    @endif
                                </a>

                                <ul class="nav-dropdown-items">
                                    <li class="nav-item">
                                        <a class="nav-link {{ active_class(Route::is('admin/order/order*')) }}" href="{{ route('admin.order.index') }}">
                                        <i class="nav-icon fas fa-book"></i>
                                            {{ trans($module.'::menus.backend.sidebar.order_list') }}

                                            @if ($pending_approval > 0)
                                                <span class="badge badge-danger">{{ $pending_approval }}</span>
                                            @endif
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link {{ active_class(Route::is('admin/order/progress_order*')) }}" href="{{ route('admin.order.progress_order') }}">
                                        <i class="nav-icon fas fa-book"></i>
                                            {{ trans($module.'::menus.backend.sidebar.progress_list') }}
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link {{ active_class(Route::is('admin/order/deleted_order*')) }}" href="{{ route('admin.order.deleted_order') }}">
                                        <i class="nav-icon fas fa-trash-alt"></i>
                                            {{ trans($module.'::menus.backend.sidebar.deleted_list') }}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @elseif($module == "route")
                            <li class="nav-item nav-dropdown open">
                                <a class="nav-link nav-dropdown-toggle {{
                                    active_class(Route::is('admin/route*'))
                                }}" href="#">
                                    <i class="{{ config($module.'.icon') }}"></i>                                    
                                    {{ trans($module.'::menus.backend.sidebar.route') }}

                                </a>

                                <ul class="nav-dropdown-items">
                                   
                                    <li class="nav-item">
                                        <a class="nav-link {{
                                        active_class(Route::is('admin/route*'))
                                            }}" href="{{ route('admin.route.index') }}">
                                            {{ trans($module.'::menus.backend.sidebar.route') }}
                                        </a>
                                    </li>
                            
                                    <li class="nav-item">
                                        <a class="nav-link {{
                                            active_class(Route::is('admin/subroute*'))
                                            }}" href="{{ route('admin.subroute.index') }}">
                                            Sub Route
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>

                        @else 
                            <li class="nav-item">
                                <a class="nav-link {{ active_class(Route::is($active)) }}" href="{{ route($route) }}">
                                    <i class="{{ config($module.'.icon') }}"></i>
                                    <span>{{ trans($mod_trans) }}</span>
                                </a>
                            </li>
                        @endif

                    @endcan

                @endif
            @endforeach
            
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->