<?php

namespace App\Http\Controllers\Frontend;

use Modules\Host\Entities\Host;
use Modules\HostCategory\Entities\HostCategory;
use Modules\Checkin\Entities\Checkin;
use Modules\Host\Repositories\HostRepository;
use Modules\Checkin\Repositories\CheckinRepository;
use Modules\Visitor\Repositories\VisitorRepository;
use Modules\Checkin\Http\Requests\CreateCheckinRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class CheckInController extends Controller
{
	protected $host;
	protected $checkin;
	protected $visitor;


	public function __construct(HostRepository $host, CheckinRepository $checkin, VisitorRepository $visitor)
    {
        $this->host = $host;
        $this->checkin = $checkin;
        $this->visitor = $visitor;
    }

    public function specificCheckInForm($slug)
    {
        $host = Host::where('slug', $slug)->where('is_active', 1)->select('host_id', 'host_name','trip_status','sick_status', 'address_detail')->first();
        if(!isset($host)){
            return redirect()->route('frontend.auth.check-in-message', 'fail');
        }
        return view('frontend.auth.specific-check-in', compact('host'));
    }

    public function checkInForm()
    {
        return view('frontend.auth.check-in');
    }

    public function checkInMessage($status)
    {
        if ($status != 'fail') {
            $checkin = Checkin::find($status);
            $host = Host::where('host_id', $checkin->host_id)->select('host_name')->first();
            return view('frontend.auth.success-page', compact('checkin','host','status'));

        }
        return view('frontend.auth.success-page', compact('status'));
    }

    public function getHostByCategory($host_category_id)
    {
       $hosts = Host::where('host_category_id', $host_category_id)->where('is_active', 1)->select('host_id', 'host_name')->get();
       return $hosts;
    }

    public function store(CreateCheckinRequest $request)
    {
        $input = $request->except('_token', '_method');
        $visitor_id = $this->visitor->createCheckinVisitor($input);
        $input['visitor_id'] = $visitor_id;
        $checkin = $this->checkin->create($input);

        return redirect()->route('frontend.auth.check-in-message', $checkin);
    }

    public function searchHost(Request $request)
    {    
        $term = $request->keyword.'%';

        $host = Host::where('host_name','like',$term)->orWhere('host_mmname','like',$term)
                ->select('host_id','host_name','address_detail')
                ->limit(10)
                ->get();
        
        
        return response()->json( $host ,200);
    }

}
