<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Modules\State\Entities\State;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\Host\Entities\Host;
use Modules\Host\Http\Requests\CreateHostRequest;
use Modules\HostCategory\Entities\HostCategory;
use Modules\City\Repositories\CityRepository;
use Modules\Township\Repositories\TownshipRepository;
use Modules\State\Repositories\StateRepository;
use Modules\Host\Repositories\HostRepository;
use Modules\HostCategory\Repositories\HostCategoryRepository;

class HostRegisterController extends Controller
{
    protected $host;
    protected $city;
    protected $township;
    protected $state;


    public function __construct(HostRepository $host, CityRepository $city, StateRepository $state, TownshipRepository $township)
    {
        $this->state = $state;
        $this->host = $host;
        $this->city = $city;
        $this->township = $township;
    }

    public function hostRegister()
    {
        $states = $this->state->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id','asc');
        $townships = $this->township->getAll('id','asc');
        $host_categories = HostCategory::select('host_category_id','host_category_name')->get();
        return view('frontend.user.host-register', compact('states', 'cities', 'townships', 'host_categories'));
    }

    public function store(CreateHostRequest $request)
    {
        $input = $request->except('_token', '_method');

        $host_id = $this->host->create($input);
        
        $this->host->createHostUser($host_id);

        return redirect()->route('frontend.user.dashboard')->withFlashSuccess(trans('alerts.frontend.host.created'));
    }

    public function getCity(int $id)
    {
        $cities = City::Where('state_id',$id)->get();
        return $cities;
    }

    public function getTownship(int $id)
    {
        $township = Township::Where('city_id', $id)->get();
        return $township;
    }
}
