<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Modules\Host\Entities\Host;
use Modules\Host\Entities\HostUser;
use DataTables;
use Modules\Checkin\Repositories\CheckinRepository;
use Modules\Checkin\Http\Requests\ManageCheckinRequest;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected $checkin;

    /**
     * @param CheckinRepository $checkin
     */
    public function __construct(CheckinRepository $checkin)
    {
        $this->checkin = $checkin;
    }


    public function index()
    {
    	$user_id = auth()->user()->user_id;
    	$host_user = HostUser::where('user_id',$user_id)->select('host_id')->get()->toArray();
    	$host_id=[];
    	for ($i=0; $i < count($host_user) ; $i++) { 
    		$host_id[] = $host_user[$i]['host_id'];
    	}
    	$hosts = Host::whereIn('host_id', $host_id)->get();
        return view('frontend.user.dashboard', compact('hosts'));
    }

    public function checkInHistory(ManageCheckinRequest $request)
    {
        return DataTables::of($this->checkin->getForCheckInHistory())
            ->editColumn('visitor_id', function ($checkin) {
                return $checkin->visitor->visitor_name;
            })
            ->editColumn('mobile_no', function ($checkin) {
                return $checkin->visitor->mobile_no;
            })
            ->editColumn('host_id', function ($checkin) {
                return $checkin->host->host_name;
            })
            ->editColumn('is_sick', function ($checkin) {
                if ($checkin->is_sick == 1) {
                    return 'Yes';
                }
                return 'No';
            })
            ->editColumn('is_trip', function ($checkin) {
                if ($checkin->is_trip == 1) {
                    return 'Yes';
                }
                return 'No';
            })
            ->make(true);
    }
}
