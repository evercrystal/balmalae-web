<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Modules\CMS\Entities\CMS;
use Modules\CMS\Repositories\CMSRepository;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */

    public function __construct(CMSRepository $cms)
    {
        $this->cms = $cms;
    }


    public function index()
    {
        $page = $this->cms->getFaqPage();

        return view('frontend.index',compact('page'));
    }

    public function page($name)
    {
        $active=$name;
        $name = str_replace('-', '_', $name);
        $cms = $this->cms->getCmsPage($name);
        if($cms){
            return view("frontend.cms",compact('cms','active'));
        }
        return redirect()->route('frontend.index');
    }

    public function aboutUs()
    {
        return view("frontend.about-us");
    }
}
