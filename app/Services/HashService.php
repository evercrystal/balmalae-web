<?php
namespace App\Services;

class HashService
{
    public function encrypt(string $data,string $secretKey) : string
    {
        $toString = $data . "&key=" . $secretKey;
        return strtoupper(hash("sha256", $toString));
    }

    public function hashEqual(string $data,string $secretKey,string $hashValue) : bool
    {
        return hash_equals($this->encrypt($data,$secretKey), $hashValue);
    }

    
}
