<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use App\Models\Auth\Traits\Relationship\UserRelationship;
use Modules\Host\Entities\Host;
use Modules\Host\Entities\HostUser;
use Laravel\Passport\HasApiTokens;
use GMBF\PhoneNumber;

/**
 * Class User.
 */
class User extends BaseUser
{
    use HasApiTokens,
    	UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope;

    protected $primaryKey = 'user_id';


    public function host_user()
    {
        return $this->hasMany(HostUser::class, 'user_id');
    }

    public function findForPassport($identifier)
    {
        $phoneNumber = new PhoneNumber();
        $mobile = $phoneNumber->add_prefix($identifier);

        return self::select(['users.*'])
            ->Where('users.mobile', $mobile)
            ->orWhere('users.email', $identifier)
            ->first();
    }

}
