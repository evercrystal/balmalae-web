<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\SocialAccount;
use App\Models\Auth\PasswordHistory;
use Modules\Customer\Entities\Customer;
use Modules\Visitor\Entities\Visitor;

/**
 * Class UserRelationship.
 */
trait UserRelationship
{
    
    /**
     * @return mixed
     */
    public function customer()
    {
        return $this->hasOne(Customer::class, 'user_id', 'user_id');
    }

    public function visitor()
    {
        return $this->hasOne(Visitor::class, 'user_id', 'user_id');
    }

    /**
     * @return mixed
     */
    public function providers()
    {
        return $this->hasMany(SocialAccount::class, 'user_id', 'user_id');
    }

    /**
     * @return mixed
     */
    public function passwordHistories()
    {
        return $this->hasMany(PasswordHistory::class, 'user_id', 'user_id');
    }
}
