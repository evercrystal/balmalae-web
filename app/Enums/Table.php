<?php

namespace App\Enums;

class Table
{
    const RECYCLE_BIN = 'recycle_bin';
    const COUNTRY = 'country';
    const CUSTOMER= 'customer';
    const CUSTOMER_LOCATION = 'customer_location';
    const CITY = 'city';
    const CMS = 'cms';
    const TOWNSHIP = 'township';
    const STATE = 'state';
    const VISITOR = 'visitor';
    const HOST = 'host';
    const HOST_CATEGORY = 'host_category';
    const ORDER = 'orders';
    const DELIVERY_SUPPLIER = 'delivery_supplier';
    const DELIVERYFEE = 'delivery_fee';
    const ROUTE = 'route';
    const SUB_ROUTE = 'sub_route';
    const PIVOT_ROUTE = 'pivot_route';

}
