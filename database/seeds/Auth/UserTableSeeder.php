<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([
            // 'first_name' => 'Super',
            // 'last_name' => 'Admin',
            'user_id'   => 1,
            'user_name' => 'Super Admin',
            'email' => 'admin@admin.com',
            'mobile' => '09895711208',
            'password' => 'gmbf9876',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed' => true,
        ]);

        User::create([
            // 'first_name' => 'Default',
            // 'last_name' => 'User',
            'user_id'   => 2,
            'user_name' => 'Super Admin',
            'email' => 'user@user.com',
            'mobile' => '09799999999',
            'password' => 'gmbf9876',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed' => true,
        ]);

        $this->enableForeignKeys();
    }
}
