<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\Township\Entities\Township;

// use Modules\City\Entities\City;

class TownshipTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $township = [
            ['id' => '1', 'name' => 'Ahlon', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '2', 'name' => 'Bahan', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '3', 'name' => 'Dagon', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '4', 'name' => 'Hlaing', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '5', 'name' => 'Kamayut', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '6', 'name' => 'Kyauktada', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '7', 'name' => 'Kyimyindaing', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '8', 'name' => 'Lanmadaw', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '9', 'name' => 'Latha', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '10', 'name' => 'Mayangon', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '11', 'name' => 'Pabedan', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '12', 'name' => 'Sanchaung', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '13', 'name' => 'Seikkan', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '14', 'name' => 'Botataung', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '15', 'name' => 'Dagon Seikkan', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '16', 'name' => 'Dawbon', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '17', 'name' => 'Mingala Taungnyunt', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '18', 'name' => 'New Dagon East', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '19', 'name' => 'New Dagon North', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '20', 'name' => 'New Dagon South', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '21', 'name' => 'North Okkalapa', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '22', 'name' => 'Pazundaung', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '23', 'name' => 'South Okkalapa', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '24', 'name' => 'Tamwe', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '25', 'name' => 'Thaketa', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '26', 'name' => 'Thingangyun', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '27', 'name' => 'Yankin', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '28', 'name' => 'Cocokyun', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '29', 'name' => 'Dala', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '30', 'name' => 'Kawhmu', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '31', 'name' => 'Khayan', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '32', 'name' => 'Kungyangon', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '33', 'name' => 'Kyauktan', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '34', 'name' => 'Seikkyi Kanaungto', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '35', 'name' => 'Thanlyin', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '36', 'name' => 'Thongwa', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '37', 'name' => 'Twante', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '38', 'name' => 'Hlaingthaya', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '39', 'name' => 'Hlegu', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '40', 'name' => 'Hmawbi', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '41', 'name' => 'Htantabin', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '42', 'name' => 'Insein', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '43', 'name' => 'Mingaladon', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '44', 'name' => 'Shwepyitha', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '45', 'name' => 'Taikkyi', 'city_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '46', 'name' => 'Aungmyethazan Township', 'city_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '47', 'name' => 'Chanayethazan Township', 'city_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '48', 'name' => 'Chanmyathazi Township', 'city_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '49', 'name' => 'Maha Aungmye Township', 'city_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '50', 'name' => 'Patheingyi Township', 'city_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '51', 'name' => 'Pyigyidagun Township', 'city_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],



        ];

        Township::insert($township);

    }
}
