<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\Route\Entities\Route;

class RouteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routes = [
            ['id' => '1','to_city_id' => '1','from_city_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['id' => '2','to_city_id' => '1','from_city_id' => '2', 'is_active' => '1', 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['id' => '3','to_city_id' => '1','from_city_id' => '3', 'is_active' => '1', 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['id' => '4','to_city_id' => '1','from_city_id' => '3', 'is_active' => '1', 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['id' => '5','to_city_id' => '2','from_city_id' => '2', 'is_active' => '1', 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['id' => '6','to_city_id' => '2','from_city_id' => '1', 'is_active' => '1', 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],
            ['id' => '7','to_city_id' => '1','from_city_id' => '4', 'is_active' => '1', 'created_at' => Carbon::now(),'updated_at' => Carbon::now()],

        ];
        Route::insert($routes);
    }
}
