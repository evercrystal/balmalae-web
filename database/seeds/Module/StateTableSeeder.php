<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\State\Entities\State;
class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = [
            ['id' => '1', 'name' => 'Yangon Region', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '2', 'name' => 'Mandalay Region', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '3', 'name' => 'Magway Region', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '4', 'name' => 'Sagaing Region', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '5', 'name' => 'Bago Region', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '6', 'name' => 'Ayeyarwady Region', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '7', 'name' => 'Taninthayi Region', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '8', 'name' => 'Kachin State', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '9', 'name' => 'Kayah State', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '10', 'name' => 'Kayin State', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '11', 'name' => 'Chin State', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '12', 'name' => 'Mon State', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '13', 'name' => 'Rakhine State', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '14', 'name' => 'Shan State', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ];
        State::insert($state);
    }
}
