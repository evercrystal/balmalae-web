<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\City\Entities\City;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = [
            ['id' => '1', 'name' => 'Yangon', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '2', 'name' => 'Mandalay', 'state_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '3', 'name' => 'Nay Pyi Taw', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '4', 'name' => 'Mawlamyine', 'state_id' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '5', 'name' => 'Kyain Seikgyi Township', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '6', 'name' => 'Bago', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '7', 'name' => 'Pathein', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '8', 'name' => 'Monywa', 'state_id' => '4', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '9', 'name' => 'Sittwe', 'state_id' => '13', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '10', 'name' => 'Meiktila', 'state_id' => '14', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '11', 'name' => 'Myeik', 'state_id' => '7', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '12', 'name' => 'Taunggyi', 'state_id' => '14', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '13', 'name' => 'Myingyan', 'state_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '14', 'name' => 'Dawei', 'state_id' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '15', 'name' => 'Pyay', 'state_id' => '5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '16', 'name' => 'Hinthada', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '17', 'name' => 'Lashio', 'state_id' => '14', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '18', 'name' => 'Pakokku', 'state_id' => '3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '19', 'name' => 'Thaton', 'state_id' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '20', 'name' => 'Pyin Oo Lwin', 'state_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '21', 'name' => 'Yenangyaung', 'state_id' => '3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '22', 'name' => 'Taungoo', 'state_id' => '5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '23', 'name' => 'Thayetmyo', 'state_id' => '3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '24', 'name' => 'Pyinmana', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '25', 'name' => 'Magway', 'state_id' => '3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '26', 'name' => 'Myitkyina', 'state_id' => '8', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '27', 'name' => 'Chauk', 'state_id' => '3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '28', 'name' => 'Mogok', 'state_id' => '14', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '29', 'name' => 'Nyaunglebin', 'state_id' => '5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '30', 'name' => 'Mudon', 'state_id' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '31', 'name' => 'Shwebo', 'state_id' => '4', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '32', 'name' => 'Sagaing', 'state_id' => '4', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '33', 'name' => 'Taungdwingyi', 'state_id' => '3', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '34', 'name' => 'Syriam', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '35', 'name' => 'Bogale', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '36', 'name' => 'Pyapon', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '37', 'name' => 'Yamethin', 'state_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '38', 'name' => 'Kanbe', 'state_id' => '14', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '39', 'name' => 'Kawthoung', 'state_id' => '7', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '40', 'name' => 'Myaydo', 'state_id' => '14', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '41', 'name' => 'Minbu', 'state_id' => '11', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '42', 'name' => 'Tharyarwady', 'state_id' => '13', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '43', 'name' => 'Thongwa', 'state_id' => '10', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '44', 'name' => 'Kyaiklat', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '45', 'name' => 'Tachilek', 'state_id' => '14', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '46', 'name' => 'Maubin', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '47', 'name' => 'Kyaukse', 'state_id' => '2', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '48', 'name' => 'Hpa-An', 'state_id' => '10', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '49', 'name' => 'Kyaikto', 'state_id' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '50', 'name' => 'Martaban', 'state_id' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '51', 'name' => 'Kyaikkami', 'state_id' => '12', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '52', 'name' => 'Bhamo', 'state_id' => '8', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '53', 'name' => 'Twante', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '54', 'name' => 'Myawadi', 'state_id' => '10', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '55', 'name' => 'Mawlaik', 'state_id' => '4', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '56', 'name' => 'Wakema', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '57', 'name' => 'Myanaung', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '58', 'name' => 'Pyu', 'state_id' => '1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '59', 'name' => 'Kayah', 'state_id' => '9', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '60', 'name' => 'Nyaungdon', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '61', 'name' => 'Mawlamyinegyunn', 'state_id' => '6', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '62', 'name' => 'Letpandan', 'state_id' => '8', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '63', 'name' => 'Thanatpin', 'state_id' => '5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '64', 'name' => 'Paungde', 'state_id' => '5', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '65', 'name' => 'Hakha', 'state_id' => '11', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '66', 'name' => 'Loikaw', 'state_id' => '9', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['id' => '67', 'name' => 'Falam', 'state_id' => '11', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],





        ];

        City::insert($city);
    }
}
