<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\HostCategory\Entities\HostCategory;

class HostCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hostcategories = [
          ['host_category_id' => '1','host_category_name' => 'Hospital','host_category_mmname' => 'ဆေးရုံ','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '2','host_category_name' => 'Shopping Center','host_category_mmname' => 'စျေးဝယ်စင်တာ','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '3','host_category_name' => 'Pagoda','host_category_mmname' => 'ဘုရား','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '4','host_category_name' => 'Bank','host_category_mmname' => 'ဘဏ်','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '5','host_category_name' => 'Air Port','host_category_mmname' => 'လေဆိပ်','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '6','host_category_name' => 'Train Station','host_category_mmname' => 'မီးရထားဘူတာ','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '7','host_category_name' => 'Cinema','host_category_mmname' => 'ရုပ်ရှင်ရုံ','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '8','host_category_name' => 'Hotel','host_category_mmname' => 'ဟိုတယ်','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['host_category_id' => '9','host_category_name' => 'Church','host_category_mmname' => 'ခရစ်ယာန်ဘုရားကျောင်း','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
        ];

        HostCategory::insert($hostcategories);
    }
}
