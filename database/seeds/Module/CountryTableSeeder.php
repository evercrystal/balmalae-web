<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Modules\Country\Entities\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
          ['id' => '1','name' => 'Myanmar','country_code' => '959','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '2','name' => 'Singapore','country_code' => '5','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '3','name' => 'Thailand','country_code' => '66','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '4','name' => 'China','country_code' => '86','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '5','name' => 'India','country_code' => '91','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '6','name' => 'Japan','country_code' => '81','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '7','name' => 'United States','country_code' => '1','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '8','name' => 'Brunei','country_code' => '673','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '9','name' => 'Cambodia','country_code' => ' 855','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '10','name' => 'Hong Kong','country_code' => '852','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '11','name' => 'Laos','country_code' => '856','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '12','name' => 'Malaysia','country_code' => '60','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '13','name' => 'Philippines','country_code' => '63','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '14','name' => 'South Korea','country_code' => '82','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '15','name' => 'United Kingdom','country_code' => '44','created_at' => Carbon::now(),'updated_at' => Carbon::now()],
          ['id' => '16','name' => 'Vietnam','country_code' => '84','created_at' => Carbon::now(),'updated_at' => Carbon::now()]
        ];

        Country::insert($countries);
    }
}
