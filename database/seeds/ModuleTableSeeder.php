<?php

use Illuminate\Database\Seeder;
use App\Enums\Table;

class ModuleTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

         $this->truncateMultiple([
            Table::CITY,
            Table::HOST_CATEGORY,
            Table::TOWNSHIP,
            Table::STATE,
        ]);

        $this->call([
            CityTableSeeder::class,
            TownshipTableSeeder::class,
            StateTableSeeder::class,
            HostCategoryTableSeeder::class,

	    ]);

        $this->enableForeignKeys();
    }
}
