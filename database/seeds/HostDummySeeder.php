<?php

use Illuminate\Database\Seeder;
use Modules\Host\Entities\Host;
use Modules\HostCategory\Entities\HostCategory;

class HostDummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(HostCategory::class, 100)->create()->each(function ($host_category) {
    
            $host = factory(Host::class, 500)->make();
            $host_category->hosts()->saveMany($host);
            
        });
    }
}
