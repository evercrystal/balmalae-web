<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\SubRoute\Entities\SubRoute;
use Modules\Township\Entities\Township;
use Modules\SubRoute\Enum\DeliveryType;

$factory->define(SubRoute::class, function (Faker $faker) {
    $fromtownship = factory(Township::class)->create();
    $totownship = factory(Township::class)->create();

    return [
        'name' => $faker->name(),
        'type' => DeliveryType::ID_CAR,
        'from_township_id' => $fromtownship->id,
        'to_township_id' => $totownship->id,
        'max_height' => $faker->randomNumber(),
        'max_width' => $faker->randomNumber(),
        'max_weight' => $faker->randomNumber(),
        'usd_price' => $faker->randomNumber(),
        'mmk_price' => $faker->randomNumber()
    ];
});
