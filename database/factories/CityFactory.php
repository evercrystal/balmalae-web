<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Modules\City\Entities\City;
use Modules\State\Entities\State;
use Faker\Generator as Faker;

$factory->define(City::class, function (Faker $faker) {
    $state = factory(State::class)->create();
    return [
        'name' => $faker->name(),
        'state_id' => $state->id
    ];
});
