<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Modules\Host\Entities\Host;
use Modules\HostCategory\Entities\HostCategory;

$factory->define(HostCategory::class, function (Faker $faker) {
    return [
        'host_category_name' => $faker->name,
        'host_category_mmname' => $faker->name,
    ];
});


$factory->define(Host::class, function (Faker $faker) {
    return [
        'host_name' => $faker->name,
        'host_mmname' => $faker->name,

        'description' => $faker->sentence($nbWords = 6, $variableNbWords = true) ,
        'mm_description' => $faker->sentence($nbWords = 6, $variableNbWords = true) ,
        'host_qrcode' => $faker->name,
        'host_mobileno' => $faker->name,
        'host_logo' => $faker->name,
        'latitude' => 33.3213,
        'longitude' => 44.321323,
        'address_detail' => $faker->sentence($nbWords = 6, $variableNbWords = true) ,
        'state_id' => 1,

        'city_id' => 1,

        'township_id' => 23,

        'sick_status' => 1,
        'trip_status' => 1,
        'is_active' => 1
    ];
});
