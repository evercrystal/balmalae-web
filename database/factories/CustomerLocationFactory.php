<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Entities\CustomerLocation;
use Modules\Country\Entities\Country;
use Modules\State\Entities\State;

$factory->define(CustomerLocation::class, function (Faker $faker) {
    $customer = factory(Customer::class)->create();
    $country = factory(Country::class)->create();
    $state = factory(State::class)->create();

    return [
        'customer_id' => $customer->id,
        'country_id' => $country->id,
        'state_id' => $state->id,
        'city_id' => 1,
        'township_id' => 1
        
    ];
});
