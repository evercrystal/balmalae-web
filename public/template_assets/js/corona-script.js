//Decalring the Different Variable and Objects
let total_cases = document.querySelector("#totalCases");
let total_death = document.querySelector("#totalDeaths");
let total_recovered = document.querySelector("#totalRecovered");
let new_cases = document.querySelector("#newCases");


const checkRecordUpdate = (data) => {
	let result = (data) ? data : 0;
	return result;
}
// Fetching the Data from the server
//Fetching the World Data
fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/latest_stat_by_country.php?country=myanmar", {
		"method": "GET",
		"headers": {
			"x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
			"x-rapidapi-key": "53009286a0mshdc8ec356f7aa205p1e0e80jsn5858f548ed53"
		}
	})
	.then(response => response.json().then(response => {

		let country_name = response.country;
		let data = response.latest_stat_by_country[0];

		if ($(total_cases).length != 0) {
			total_cases.innerHTML = data.total_cases;
		}
		if ($(new_cases).length != 0) {
			
			new_cases.innerHTML = checkRecordUpdate(data.new_cases); 
		}
		if ($(total_death).length != 0) {
			total_death.innerHTML = data.total_deaths;
		}
		if ($(total_recovered).length != 0) {
			total_recovered.innerHTML = data.total_recovered;
		}

	})).catch(err => {
		console.log(err);
	});