@extends ('backend.layouts.app')

@section ('title', __('visitor::labels.backend.visitor.management') . ' | ' . __('visitor::labels.backend.visitor.create'))

@section('breadcrumb-links')
    @include('visitor::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
{{ html()->form('POST', route('admin.visitor.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('visitor::labels.backend.visitor.management') }}
                        <small class="text-muted">{{ __('visitor::labels.backend.visitor.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                    {{ html()->label(__('visitor::labels.backend.visitor.table.user_id').'<span class="text-danger"> (option)</span>')->class('col-md-2 form-control-label')->for('user_id') }}

                        <div class="col-md-10">
                            <select name="user_id" class="form-control select2">
                                <option value =""> Please Select User </option>
                                @foreach($users as $user)
                                    <option value="{{$user->user_id}}"> {{$user->user_name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->


                    <div class="form-group row">
                    {{ html()->label(__('visitor::labels.backend.visitor.table.name'))->class('col-md-2 form-control-label')->for('visitor_name') }}

                        <div class="col-md-10">
                            {{ html()->text('visitor_name')
                                ->class('form-control')
                                ->placeholder(__('visitor::labels.backend.visitor.table.name'))
                                ->attribute('maxlength', 100)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('visitor::labels.backend.visitor.table.ph'))->class('col-md-2 form-control-label')->for('mobile_no') }}

                        <div class="col-md-10">
                            {{ html()->number('mobile_no')
                                ->class('form-control')
                                ->placeholder(__('visitor::labels.backend.visitor.table.ph'))
                                ->attribute('maxlength', 50)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('visitor::labels.backend.visitor.table.address'))->class('col-md-2 form-control-label')->for('address') }}

                        <div class="col-md-10">
                            {{ html()->textarea('address')
                                ->class('form-control')
                                ->placeholder(__('visitor::labels.backend.visitor.table.address')) }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.visitor.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')

<script>


</script>
@endpush