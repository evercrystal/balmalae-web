@extends ('backend.layouts.app')

@section ('title', __('visitor::labels.backend.visitor.management'))

@section('breadcrumb-links')
    @include('visitor::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('visitor::labels.backend.visitor.management') }}
                    <small class="text-muted">{{ __('visitor::labels.backend.visitor.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('visitor::labels.backend.visitor.table.user_name') }}</th>
                                <td>
                                    <?php 
                                        if (isset($visitor->user->user_name)) {
                                            echo $visitor->user->user_name; 
                                        }else{
                                            echo '-';
                                        }
                                     ?>
                                </td>
                            </tr>
                            <tr>
                                <th>{{ __('visitor::labels.backend.visitor.table.visitor_name') }}</th>
                                <td>{{ $visitor->visitor_name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('visitor::labels.backend.visitor.table.ph') }}</th>
                                <td>{{ $visitor->mobile_no }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('visitor::labels.backend.visitor.table.address') }}</th>
                                <td>{{ $visitor->address }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('visitor::labels.backend.visitor.table.created') }}:</strong> {{ $visitor->updated_at->timezone(get_user_timezone()) }} ({{ $visitor->created_at->diffForHumans() }}),
                    <strong>{{ __('visitor::labels.backend.visitor.table.last_updated') }}:</strong> {{ $visitor->created_at->timezone(get_user_timezone()) }} ({{ $visitor->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush