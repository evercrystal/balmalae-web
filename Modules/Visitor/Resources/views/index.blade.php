@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('visitor::labels.backend.visitor.management'))

@section('breadcrumb-links')
    @include('visitor::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('visitor::labels.backend.visitor.management') }} <small class="text-muted">{{ __('visitor::labels.backend.visitor.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('visitor::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="visitor-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('visitor::labels.backend.visitor.table.id') }}</th>
                            <th>{{ __('visitor::labels.backend.visitor.table.user_id') }}</th>
                            <th>{{ __('visitor::labels.backend.visitor.table.name') }}</th>
                            <th>{{ __('visitor::labels.backend.visitor.table.ph') }}</th>
                            <th>{{ __('visitor::labels.backend.visitor.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#visitor-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.visitor.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'visitor_id', name: 'visitor_id'},
                    {data: 'user_id', name: 'user_id'},
                    {data: 'visitor_name', name: 'visitor_name'},
                    {data: 'mobile_no', name: 'mobile_no'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush