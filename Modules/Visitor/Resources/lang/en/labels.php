<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'visitor' => [
                    'create'     => 'Create Visitor',
                    'edit'       => 'Edit Visitor',
                    'management' => 'Visitor Management',
                    'list'       => 'Visitor List',
                    'show'       => 'Visitor Detail',

                    'table' => [
                        'number_of_users' => 'Number of Visitors',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'ph'               => 'Mobile Number',
                        'user_id'          => 'User ID',
                        'user_name'        => 'User Name',
                        'visitor_name'     => 'Visitor Name',
                        'description'      => 'Description',
                        'address'          => 'Address',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'visitor total|visitor total',
                    ]
                ]
            ]

];