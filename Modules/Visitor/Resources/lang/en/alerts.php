<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'visitor' => [
            'created' => 'The visitor was successfully created.',
            'deleted' => 'The visitor was successfully deleted.',
            'not_deleted' => 'The visitor was not deleted. It is used in checkin table.',
            'updated' => 'The visitor was successfully updated.',
        ]
    ]
];