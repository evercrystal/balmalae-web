<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'visitor' => 'Visitor',
                'visitor_bin' => 'Visitor Bin',
            ],
            'visitor' => [
                'all'        => 'All Visitor',
                'create'     => 'Create Visitor',
                'edit'       => 'Edit Visitor',
                'show'       => 'Show Visitor',
                'management' => 'Visitor Management',
                'main'       => 'Visitor',
            ]
        ]
];