<?php

namespace Modules\Visitor\Repositories;

use Modules\Visitor\Entities\Visitor;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VisitorRepository.
 */
class VisitorRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function __construct(Visitor $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }

    public function saveVisitorinfo($input)
    {
        return DB::transaction(function () use ($input) {

            $visitor = new Visitor;
            $visitor->user_id = null;
            $visitor->visitor_name = $input['visitor_name'];
            $visitor->mobile_no = $input['mobile_no'];
            $visitor->address = $input['visitor_address'];


            if ($visitor->save()) 
            {
                return $visitor->visitor_id;
            }  
                    
            throw new GeneralException(trans('visitor::exceptions.backend.visitor.create_error'));
        });


    }

    public function createCheckinVisitor($input)
    {
        $visitor = Visitor::where('mobile_no', $input['mobile'])->first();
        
        if (isset($visitor)) {
            $visitor->address = $input['address'];
            $visitor->update();
            return $visitor->visitor_id;
        }

        return DB::transaction(function () use ($input) {

            $visitor = new Visitor;
            $visitor->user_id = null;
            $visitor->visitor_name = $input['name'];
            $visitor->mobile_no = $input['mobile'];
            $visitor->address = $input['address'];


            if ($visitor->save()) 
            {
                return $visitor->visitor_id;
            }  
                    
            throw new GeneralException(trans('visitor::exceptions.backend.visitor.create_error'));
        });


    }
}
