<?php

namespace Modules\Visitor\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Visitor\Repositories\VisitorRepository;
use Modules\Visitor\Http\Requests\ManageVisitorRequest;

class VisitorTableController extends Controller
{
    /**
     * @var VisitorRepository
     */
    protected $visitor;

    /**
     * @param VisitorRepository $visitor
     */
    public function __construct(VisitorRepository $visitor)
    {
        $this->visitor = $visitor;
    }

    /**
     * @param ManageVisitorRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageVisitorRequest $request)
    {
        return DataTables::of($this->visitor->getForDataTable())
            ->addColumn('actions', function ($visitor) {
                return $visitor->action_buttons;
            })
            ->editColumn('user_id', function ($visitor) {
                if ($visitor->user_id == null) {
                    return '-';
                }else{
                    return $visitor->user_id;
                }
                
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
