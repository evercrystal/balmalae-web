<?php

namespace Modules\Visitor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Auth\User;
use Modules\Visitor\Entities\Visitor;
use Modules\Checkin\Entities\Checkin;
use Modules\Visitor\Http\Requests\ManageVisitorRequest;
use Modules\Visitor\Http\Requests\CreateVisitorRequest;
use Modules\Visitor\Http\Requests\UpdateVisitorRequest;
use Modules\Visitor\Http\Requests\ShowVisitorRequest;
use Modules\Visitor\Repositories\VisitorRepository;
use Modules\Checkin\Repositories\CheckinRepository;

class VisitorController extends Controller
{
 /**
     * @var VisitorRepository
     * @var CategoryRepository
     */
    protected $visitor;

    /**
     * @param VisitorRepository $visitor
     */
    public function __construct(VisitorRepository $visitor)
    {
        $this->visitor = $visitor;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('visitor::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $users = User::select('user_id','user_name')->get();
        return view('visitor::create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateVisitorRequest $request)
    {
        $this->visitor->create($request->except('_token','_method'));
        return redirect()->route('admin.visitor.index')->withFlashSuccess(trans('visitor::alerts.backend.visitor.created'));
    }

    /**
     * @param Visitor              $visitor
     * @param ManageVisitorRequest $request
     *
     * @return mixed
     */
    public function edit(Visitor $visitor, ManageVisitorRequest $request)
    {
        $users = User::select('user_id','user_name')->get();
        return view('visitor::edit', compact('users'))
            ->withVisitor($visitor);
    }

    /**
     * @param Visitor              $visitor
     * @param UpdateVisitorRequest $request
     *
     * @return mixed
     */
    public function update(Visitor $visitor,UpdateVisitorRequest $request)
    {
        $request->validate([
            'mobile_no' => 'valid_phone_number|unique:visitor,mobile_no,'.$visitor->visitor_id.',visitor_id',
        ]);
        $this->visitor->updateById($visitor->visitor_id,$request->except('_token','_method'));

        return redirect()->route('admin.visitor.index')->withFlashSuccess(trans('visitor::alerts.backend.visitor.updated'));
    }

    /**
     * @param Visitor              $visitor
     * @param ManageVisitorRequest $request
     *
     * @return mixed
     */
    public function show(Visitor $visitor, ShowVisitorRequest $request)
    {
        return view('visitor::show')->withVisitor($visitor);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Visitor $visitor)
    {
        $checkin_list = Checkin::where('visitor_id', $visitor->visitor_id)->count();
        if ($checkin_list > 0) {
            return redirect()->route('admin.visitor.index')->withFlashDanger(__('visitor::alerts.backend.visitor.not_deleted'));
        }
        $this->visitor->deleteById($visitor->visitor_id);

        return redirect()->route('admin.visitor.index')->withFlashSuccess(trans('visitor::alerts.backend.visitor.deleted'));
    }
}
