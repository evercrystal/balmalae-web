<?php

Breadcrumbs::for('admin.visitor.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('visitor::labels.backend.visitor.management'), route('admin.visitor.index'));
});

Breadcrumbs::for('admin.visitor.create', function ($trail) {
    $trail->parent('admin.visitor.index');
    $trail->push(__('visitor::labels.backend.visitor.create'), route('admin.visitor.create'));
});

Breadcrumbs::for('admin.visitor.show', function ($trail, $id) {
    $trail->parent('admin.visitor.index');
    $trail->push(__('visitor::labels.backend.visitor.show'), route('admin.visitor.show', $id));
});

Breadcrumbs::for('admin.visitor.edit', function ($trail, $id) {
    $trail->parent('admin.visitor.index');
    $trail->push(__('visitor::labels.backend.visitor.edit'), route('admin.visitor.edit', $id));
});
