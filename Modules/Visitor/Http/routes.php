<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Visitor\Http\Controllers'], function()
{
    		/*
             * For DataTables
             */
            Route::post('visitor/get', 'VisitorTableController')->name('visitor.get');
            /*
             * User CRUD
             */
            Route::resource('visitor', 'VisitorController');
});