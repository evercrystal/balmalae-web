<?php

namespace Modules\Visitor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Table;

class CreateVisitorRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visitor_name' => 'required|max:100|',
            'mobile_no' => 'required|valid_phone_number|unique:'.Table::VISITOR,
        ];
    }

    public function messages()
    {
        return [
            'valid_phone_number' => 'Invalid Mobile No. or Not Support Mobile No.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create visitor');
    }
}
