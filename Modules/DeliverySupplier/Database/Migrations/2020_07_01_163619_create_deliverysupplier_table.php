<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\Table;

class CreateDeliverysupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Table::DELIVERY_SUPPLIER, function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->string('ref_id')->unique();
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')->references('id')->on(Table::COUNTRY)->onDelete('cascade');
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on(Table::STATE)->onDelete('cascade');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on(Table::CITY)->onDelete('cascade');
            $table->integer('township_id')->unsigned();
            $table->foreign('township_id')->references('id')->on(Table::TOWNSHIP)->onDelete('cascade');
            $table->string('name');
            $table->integer('type');
            $table->string('mobile');
            $table->integer('total_delivery_amount')->default(0);
            $table->integer('paid_amount')->default(0);
            $table->integer('unpaid_amount')->default(0);
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('address_detail')->nullable();

            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
            ;
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Table::DELIVERY_SUPPLIER);
    }
}
