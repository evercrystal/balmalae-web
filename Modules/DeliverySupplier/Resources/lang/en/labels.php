<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'deliverysupplier' => [
                    'create'     => 'Create DeliverySupplier',
                    'edit'       => 'Edit DeliverySupplier',
                    'management' => 'DeliverySupplier Management',
                    'list'       => 'DeliverySupplier List',
                    'show'       => 'DeliverySupplier Detail',

                    'table' => [
                        'number_of_users' => 'Number of DeliverySuppliers',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'description'      => 'Description',
                        'email'            => 'Email',
                        'mobile'           => 'Mobile No',
                        'customer_type'    => 'Customer Type',
                        'choose_location'  => 'Choose Location',
                        'location'         => 'Location',
                        'country'          => 'Country',
                        'state'            => 'State',
                        'city'             => 'City',
                        'township'         => 'Township',
                        'address_detail'   => 'Address Detail',
                        'latitude'         => 'Latitude',
                        'longitude'        => 'Longitude',
                        'type'             => 'Customer Type',
                        'total_delivery_amount'=> 'Total Delivery Amount',
                        'paid_amount'      => 'Paid Amount',
                        'unpaid_amount'    => 'Unpaid Amount',
                        'active'           => 'Active',
                        'status'           => 'Status',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'deliverysupplier total|deliverysupplier total',
                    ]
                ]
            ]

];