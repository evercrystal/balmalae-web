<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'deliverysupplier' => [
            'created' => 'The deliverysupplier was successfully created.',
            'deleted' => 'The deliverysupplier was successfully deleted.',
            'updated' => 'The deliverysupplier was successfully updated.'
        ]
    ]
];