<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'deliverysupplier' => 'Delivery Supplier',
                'deliverysupplier_bin' => 'DeliverySupplier Bin',
            ],
            'deliverysupplier' => [
                'all'        => 'All DeliverySupplier',
                'create'     => 'Create DeliverySupplier',
                'edit'       => 'Edit DeliverySupplier',
                'show'       => 'Show DeliverySupplier',
                'management' => 'DeliverySupplier Management',
                'main'       => 'DeliverySupplier',
            ]
        ]
];