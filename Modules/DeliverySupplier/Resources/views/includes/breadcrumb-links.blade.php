<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('deliverysupplier::menus.backend.deliverysupplier.main') }}</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.deliverysupplier.index') }}">{{ __('deliverysupplier::menus.backend.deliverysupplier.all') }}</a>
                @can('create deliverysupplier')
                <a class="dropdown-item" href="{{ route('admin.deliverysupplier.create') }}">{{ __('deliverysupplier::menus.backend.deliverysupplier.create') }}</a>
                @endcan
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>