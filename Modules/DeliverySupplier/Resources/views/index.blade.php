@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('deliverysupplier::labels.backend.deliverysupplier.management'))

@section('breadcrumb-links')
    @include('deliverysupplier::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('deliverysupplier::labels.backend.deliverysupplier.management') }} <small class="text-muted">{{ __('deliverysupplier::labels.backend.deliverysupplier.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('deliverysupplier::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="deliverysupplier-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.id') }}</th>
                            <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.name') }}</th>
                            <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.mobile') }}</th>
                            <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.total_delivery_amount') }}</th>
                            <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.paid_amount') }}</th>
                            <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.unpaid_amount') }}</th>
                            <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#deliverysupplier-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.deliverysupplier.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'total_delivery_amount', name: 'total_delivery_amount'},
                    {data: 'paid_amount', name: 'paid_amount'},
                    {data: 'unpaid_amount', name: 'unpaid_amount'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush