@extends ('backend.layouts.app')

@section ('title', __('deliverysupplier::labels.backend.deliverysupplier.management'))

@section('breadcrumb-links')
    @include('deliverysupplier::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('deliverysupplier::labels.backend.deliverysupplier.management') }}
                    <small class="text-muted">{{ __('deliverysupplier::labels.backend.deliverysupplier.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.name') }}</th>
                                <td>{{ $deliverysupplier->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('labels.backend.access.users.table.email') }}</th>
                                <td>{{ $deliverysupplier->user->email }}</td>                                
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.mobile') }}</th>
                                <td>{{ $deliverysupplier->mobile }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.type') }}</th>
                                <td>{{ $supplierType }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.country') }}</th>
                                <td>{{ $deliverysupplier->country->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.state') }}</th>
                                <td>{{ $deliverysupplier->state->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.city') }}</th>
                                <td>{{ $deliverysupplier->city->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.township') }}</th>
                                <td>{{ $deliverysupplier->township->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.latitude') }}</th>
                                <td>{{ $deliverysupplier->latitude }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.longitude') }}</th>
                                <td>{{ $deliverysupplier->longitude }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.address_detail') }}</th>
                                <td>{{ $deliverysupplier->address_detail }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.status') }}</th>
                                <td>{!! $deliverysupplier->status_label !!}</td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.created') }}:</strong> {{ $deliverysupplier->updated_at->timezone(get_user_timezone()) }} ({{ $deliverysupplier->created_at->diffForHumans() }}),
                    <strong>{{ __('deliverysupplier::labels.backend.deliverysupplier.table.last_updated') }}:</strong> {{ $deliverysupplier->created_at->timezone(get_user_timezone()) }} ({{ $deliverysupplier->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush