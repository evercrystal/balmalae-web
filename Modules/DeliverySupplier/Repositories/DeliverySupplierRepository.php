<?php

namespace Modules\DeliverySupplier\Repositories;

use Modules\DeliverySupplier\Entities\DeliverySupplier;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\Auth\UserRepository;
use GMBF\PhoneNumber;

/**
 * Class DeliverySupplierRepository.
 */
class DeliverySupplierRepository extends BaseRepository
{
    protected $userRepository;
    /**
     * @return string
     */
    public function __construct(DeliverySupplier $model, UserRepository $userRepository)
    {
        $this->model = $model;
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }

    public function create(array $input): bool
    {
        $customerName = explode(' ', $input['name']);
        $firstname = $customerName[0];
        unset($customerName[0]);
        $lastname  = implode(' ', $customerName);

        $phoneNumber = new PhoneNumber();
        $input['mobile'] = $phoneNumber->add_prefix($input['mobile']);
        $input['ref_id'] = $this->generateReferenceId();

        \DB::beginTransaction();
        try {
            $user = $this->userRepository->create([
                'first_name' => $firstname,
                'last_name' => $lastname,
                'email' => $input['email'],
                'password' => $input['password'],
                'active' => $input['active'],
                'confirmed' => $input['confirmed'],
                'roles' => [0 => 'user']

            ]);
            $input['user_id'] = $user->id;

            DeliverySupplier::create($input);

        } catch (\Exception $e) {
            DB::rollback();
            throw new GeneralException(trans('deliverysupplier::exceptions.backend.deliverysupplier.create_error'));
        }

        \DB::commit();
        return true;
    }

    public static function generateReferenceId(): string
    {
        $caracters = 'ABCDEFGHIJKLMOPQRSTUVXWYZ';
        $quantidadeCaracters = strlen($caracters);
        $quantidadeCaracters--;
        $hash = null;
        for ($x = 1; $x <= 6; $x++) {
            $Posicao = rand(0, $quantidadeCaracters);
            $hash .= substr($caracters, $Posicao, 1);
        }
        $data = 'BNFS-'.$hash;
        $supplier = DeliverySupplier::where('ref_id', $data)->count();
        if (preg_match('/^(.)\1*$/', $data) == 1 || $supplier != 0) {
            $data = DeliverySupplier::generateReferenceId();
        }
        return $data;
    }

    public function update(DeliverySupplier $supplier, array $input)
    {
        $user = $supplier->user;

        $supplierName = explode(' ', $input['name']);
        $firstname = $supplierName[0];
        unset($supplierName[0]);
        $lastname  = implode(' ', $supplierName);

        \DB::beginTransaction();
        try {
            $this->userRepository->update($user, [
                'first_name' => $firstname,
                'last_name' => $lastname,
                'email' => $input['email']
            ]);
            $this->updateById($supplier->id, $input);

        } catch (\Exception $e) {
            DB::rollback();
            throw new GeneralException(trans('deliverysupplier::exceptions.backend.deliverysupplier.update_error'));
        }

        \DB::commit();
    }
}
