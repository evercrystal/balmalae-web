<?php

namespace Modules\DeliverySupplier\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\Country\Entities\Country;
use Modules\City\Entities\City;
use Modules\State\Entities\State;
use Modules\Township\Entities\Township;
use App\Models\Auth\User;
use App\Enums\Table;

class DeliverySupplier extends Model
{
    use SoftDeletes;
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = Table::DELIVERY_SUPPLIER;

    protected $fillable = ["id","user_id","ref_id","country_id","state_id","city_id","township_id","name","type","mobile","total_delivery_amount","paid_amount","unpaid_amount","latitude","longitude","address_detail"];

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return mixed
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * @return mixed
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
    /**
     * @return mixed
     */
    public function township()
    {
        return $this->belongsTo(Township::class, 'township_id');
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        if (auth()->user()->can('view deliverysupplier')) {
            return '<a href="'.route('admin.deliverysupplier.show', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
        return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (auth()->user()->can('edit deliverysupplier')) {
            return '<a href="'.route('admin.deliverysupplier.edit', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
        return '';
    }

    /**
    * @return string
    */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete deliverysupplier')) {
            return '<a href="'.route('admin.deliverysupplier.destroy', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    public function getStatusLabelAttribute()
    {
        $user = User::find($this->user_id);
        if ($user->active) {
            return '<span class="badge badge-success">active</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getShowButtonAttribute().$this->getEditButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
