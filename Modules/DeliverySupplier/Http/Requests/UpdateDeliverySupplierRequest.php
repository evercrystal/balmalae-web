<?php

namespace Modules\DeliverySupplier\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Enums\Table;

class UpdateDeliverySupplierRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'mobile' => 'required|valid_phone_number',
            'type' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'township_id' => 'required',
            'email'    => ['required', 'email', 'max:191', Rule::unique('users')->ignore($this->user_id)]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit deliverysupplier');
    }
}
