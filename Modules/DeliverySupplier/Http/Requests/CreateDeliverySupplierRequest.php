<?php

namespace Modules\DeliverySupplier\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateDeliverySupplierRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'mobile' => 'required|valid_phone_number',
            'type' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'township_id' => 'required',
            'email'    => ['required', 'email', 'max:191', Rule::unique('users')],
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'active' => 'required',
            'confirmed' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'valid_phone_number' => 'Invalid Mobile No. or Not Support Mobile No.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create deliverysupplier');
    }
}
