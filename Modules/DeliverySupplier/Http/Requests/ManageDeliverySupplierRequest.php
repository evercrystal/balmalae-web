<?php

namespace Modules\DeliverySupplier\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ManageDeliverySupplierRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('manage deliverysupplier');
    }
}
