<?php

namespace Modules\DeliverySupplier\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Modules\DeliverySupplier\Http\Requests\ManageDeliverySupplierRequest;
use Modules\DeliverySupplier\Http\Requests\CreateDeliverySupplierRequest;
use Modules\DeliverySupplier\Http\Requests\UpdateDeliverySupplierRequest;
use Modules\DeliverySupplier\Http\Requests\ShowDeliverySupplierRequest;
use Modules\DeliverySupplier\Repositories\DeliverySupplierRepository;
use Modules\Country\Repositories\CountryRepository;
use Modules\City\Repositories\CityRepository;
use Modules\State\Repositories\StateRepository;
use Modules\Township\Repositories\TownshipRepository;
use Modules\DeliverySupplier\Enum\DeliverySupplierType;

class DeliverySupplierController extends Controller
{
    /**
        * @var DeliverySupplierRepository
        * @var CategoryRepository
        */
    protected $deliverysupplier;
    protected $country;
    protected $city;
    protected $township;
    protected $state;

    /**
     * @param DeliverySupplierRepository $deliverysupplier
     */
    public function __construct(DeliverySupplierRepository $deliverysupplier, CountryRepository $country, CityRepository $city, StateRepository $state, TownshipRepository $township)
    {
        $this->deliverysupplier = $deliverysupplier;
        $this->country = $country;
        $this->city = $city;
        $this->township = $township;
        $this->state = $state;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('deliverysupplier::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $countries = $this->country->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id', 'asc');
        $townships = $this->township->getAll('id', 'asc');
        $types = DeliverySupplierType::AVAILABLES;
        return view('deliverysupplier::create', compact('countries', 'cities', 'townships', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateDeliverySupplierRequest $request)
    {
        $input = $request->except('_token', '_method');
        $this->deliverysupplier->create($input);
        
        return redirect()->route('admin.deliverysupplier.index')->withFlashSuccess(trans('deliverysupplier::alerts.backend.deliverysupplier.created'));
    }

    /**
     * @param DeliverySupplier              $deliverysupplier
     * @param ManageDeliverySupplierRequest $request
     *
     * @return mixed
     */
    public function edit(DeliverySupplier $deliverysupplier, ManageDeliverySupplierRequest $request)
    {
        $countries = $this->country->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id', 'asc');
        $townships = $this->township->getAll('id', 'asc');
        $states = $this->state->getAll('id', 'asc');
        $types = DeliverySupplierType::AVAILABLES;
        return view('deliverysupplier::edit', compact('countries', 'cities', 'townships','states', 'types'))
            ->withDeliverysupplier($deliverysupplier);
    }

    /**
     * @param DeliverySupplier              $deliverysupplier
     * @param UpdateDeliverySupplierRequest $request
     *
     * @return mixed
     */
    public function update(DeliverySupplier $deliverysupplier, UpdateDeliverySupplierRequest $request)
    {
        $input = $request->except('_token', '_method');

        $this->deliverysupplier->update($deliverysupplier, $input);

        return redirect()->route('admin.deliverysupplier.index')->withFlashSuccess(trans('deliverysupplier::alerts.backend.deliverysupplier.updated'));
    }

    /**
     * @param DeliverySupplier              $deliverysupplier
     * @param ManageDeliverySupplierRequest $request
     *
     * @return mixed
     */
    public function show(DeliverySupplier $deliverysupplier, ShowDeliverySupplierRequest $request)
    {
        $types = DeliverySupplierType::AVAILABLES;
        $supplierType = $types[$deliverysupplier->type];
        return view('deliverysupplier::show', compact('supplierType'))->withDeliverysupplier($deliverysupplier);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(DeliverySupplier $deliverysupplier)
    {
        $this->deliverysupplier->deleteById($deliverysupplier->id);

        return redirect()->route('admin.deliverysupplier.index')->withFlashSuccess(trans('deliverysupplier::alerts.backend.deliverysupplier.deleted'));
    }
}
