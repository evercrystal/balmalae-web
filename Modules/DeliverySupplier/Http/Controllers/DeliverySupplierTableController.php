<?php

namespace Modules\DeliverySupplier\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\DeliverySupplier\Repositories\DeliverySupplierRepository;
use Modules\DeliverySupplier\Http\Requests\ManageDeliverySupplierRequest;

class DeliverySupplierTableController extends Controller
{
    /**
     * @var DeliverySupplierRepository
     */
    protected $deliverySupplier;

    /**
     * @param DeliverySupplierRepository $deliverySupplier
     */
    public function __construct(DeliverySupplierRepository $deliverySupplier)
    {
        $this->deliverySupplier = $deliverySupplier;
    }

    /**
     * @param ManageDeliverySupplierRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageDeliverySupplierRequest $request)
    {
        return DataTables::of($this->deliverySupplier->getForDataTable())
            ->addColumn('actions', function ($deliverySupplier) {
                return $deliverySupplier->action_buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
