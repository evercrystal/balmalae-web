<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\DeliverySupplier\Http\Controllers'], function () {
    /*
     * For DataTables
     */
    Route::post('deliverysupplier/get', 'DeliverySupplierTableController')->name('deliverysupplier.get');
    /*
     * User CRUD
     */
    Route::resource('deliverysupplier', 'DeliverySupplierController');
});
