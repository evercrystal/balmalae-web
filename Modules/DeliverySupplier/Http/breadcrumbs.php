<?php

Breadcrumbs::for('admin.deliverysupplier.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('deliverysupplier::labels.backend.deliverysupplier.management'), route('admin.deliverysupplier.index'));
});

Breadcrumbs::for('admin.deliverysupplier.create', function ($trail) {
    $trail->parent('admin.deliverysupplier.index');
    $trail->push(__('deliverysupplier::labels.backend.deliverysupplier.create'), route('admin.deliverysupplier.create'));
});

Breadcrumbs::for('admin.deliverysupplier.show', function ($trail, $id) {
    $trail->parent('admin.deliverysupplier.index');
    $trail->push(__('deliverysupplier::labels.backend.deliverysupplier.show'), route('admin.deliverysupplier.show', $id));
});

Breadcrumbs::for('admin.deliverysupplier.edit', function ($trail, $id) {
    $trail->parent('admin.deliverysupplier.index');
    $trail->push(__('deliverysupplier::labels.backend.deliverysupplier.edit'), route('admin.deliverysupplier.edit', $id));
});
