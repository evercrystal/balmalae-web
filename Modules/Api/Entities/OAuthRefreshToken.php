<?php

namespace Modules\Api\Entities;

use Illuminate\Database\Eloquent\Model;

class OAuthRefreshToken extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_refresh_tokens';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
