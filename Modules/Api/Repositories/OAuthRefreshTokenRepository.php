<?php 

namespace Modules\Api\Repositories;

use App\Repositories\BaseRepository;
use Modules\Api\Entities\OAuthRefreshToken;

class OAuthRefreshTokenRepository extends BaseRepository
{
    /**
     * OAuthRefreshTokenRepository constructor.
     *
     * @param  OAuthRefreshToken  $model
     */
    public function __construct(OAuthRefreshToken $model)
    {
        $this->model = $model;
    }

    public function revokeRefreshTokenByAccessTokenId($accessTokenId)
    {
        return $this->model::where('access_token_id', $accessTokenId)->update(
            ['revoked' => true]
        );
    }
}
