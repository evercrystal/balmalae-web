<?php

namespace Modules\Api\Transformers;

use Modules\State\Entities\State;
use League\Fractal\TransformerAbstract;

class StateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * Transform State model
     * @param  State   $state
     * @return array
     */
    public function transform(State $state)
    {
        return [
            "name" => $state->name,
            "country_name" => $state->country->name
        ];
    }

}