<?php

namespace Modules\Api\Transformers;

use Modules\City\Entities\City;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * Transform City model
     * @param  City   $city
     * @return array
     */
    public function transform(City $city)
    {
        return [
            "name" => $city->name,
            "state_name" => $city->state->name
        ];
    }

}