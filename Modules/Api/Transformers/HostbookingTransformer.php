<?php

namespace Modules\Api\Transformers;

use Modules\Host\Entities\Host;
use League\Fractal\TransformerAbstract;

class HostbookingTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * Transform Hostbooking model
     * @param  Hostbooking   $hostbooking
     * @return array
     */
    public function transform(Host $hostbooking)
    {
        return [
            "name" => $hostbooking->host_name,
            "host_category" => $hostbooking->host_category->host_category_name,
            "description" => $hostbooking->description,
            "host_mobile" => $hostbooking->host_mobileno,
            "host_logo" => (string) url('uploads/' . $hostbooking->host_logo),
            "latitude" => $hostbooking->latitude,
            "longitude" => $hostbooking->longitude,
            "address_detail" => $hostbooking->address_detail,
            "state_name" => $hostbooking->state->name,
            "city_name" => $hostbooking->city->name,
            "township_name" => $hostbooking->township->name,
            "is_sick" => $hostbooking->sick_status,
            "is_trip" => $hostbooking->trip_status,
            "created_at" => (string) $hostbooking->created_at,
            "updated_at" => (string) $hostbooking->updated_at,
        ];
    }

}