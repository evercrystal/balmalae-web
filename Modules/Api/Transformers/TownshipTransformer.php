<?php

namespace Modules\Api\Transformers;

use Modules\Township\Entities\Township;
use League\Fractal\TransformerAbstract;

class TownshipTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * Transform Township model
     * @param  Township   $township
     * @return array
     */
    public function transform(Township $township)
    {
        return [
            "name" => $township->name,
            "city_name" => $township->city->name
        ];
    }

}