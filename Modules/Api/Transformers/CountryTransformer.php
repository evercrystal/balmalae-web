<?php

namespace Modules\Api\Transformers;

use Modules\Country\Entities\Country;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];

    /**
     * Transform Coutry model
     * @param  Coutry   $country
     * @return array
     */
    public function transform(Country $country)
    {
        return [
            "name" => $country->name,
            "country_code" => $country->country_code
        ];
    }

}