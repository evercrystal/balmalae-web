<?php

namespace Modules\Api\Transformers;

use App\Models\Auth\User;
use League\Fractal\TransformerAbstract;


/**
 * @SWG\Definition(
 *   definition="UserWithIncludes",
 *   type="object",
 *   allOf={
 *      @SWG\Schema(ref="#/definitions/User"),
 *      @SWG\Schema(ref="#/definitions/UserIncludes"),
 *   }
 * )
*/
/**
 * @SWG\Definition(
 *   definition="UserIncludes",
 *   type="object",
 *   @SWG\Property(
 *       property="supplier",
 *       description="Supplier of this user. This is only returned when specified in the API or if included  in `include` request.",
 *       type="object"
 *   )
 * )
 */

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
    ];


    public function transform(User $user)
    {
        return [
            "id" => $user->user_id,
            "uuid" => $user->uuid,
            "user_name" => $user->user_name,
            "email" => $user->email,
            "mobile" => $user->mobile,
            "visitor_id" => $user->visitor ? $user->visitor->visitor_id : null,
            "visitor_name" => $user->visitor ? $user->visitor->visitor_name : null,
            "visitor_mobile" => $user->visitor ? $user->visitor->mobile_no : null,
            "visitor_address" => $user->visitor ? $user->visitor->address : null,
            "password_changed_at" => $user->password_changed_at,
            "active" => $user->active,
            "created_at" => (string) $user->created_at,
            "updated_at" => (string) $user->updated_at
        ];
    }

    public function includeSupplier(User $user)
    {
        $supplier = $user->deliverySupplier;
        return $supplier ? $this->primitive($supplier): $this->null();
    }

}