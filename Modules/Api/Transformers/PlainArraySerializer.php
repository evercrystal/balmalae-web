<?php

namespace Modules\Api\Transformers;

use League\Fractal\Serializer\ArraySerializer;


class PlainArraySerializer extends ArraySerializer
{
    public function collection($resourceKey, array $data)
    {
        if ($resourceKey === false) {
            return $data;
        }
        return array($resourceKey ?: 'data' => $data);
    }

    public function item($resourceKey, array $data)
    {
        if ($resourceKey === false) {
            return $data;
        }
        return array($resourceKey ?: 'data' => $data);
    }

    public function null()
    {
        return null;
    }

    public function empty()
    {
        return [];
    }
}