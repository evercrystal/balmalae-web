<?php

namespace Modules\Api\Http\Middleware;

use App;
use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $clientLocales = $request->headers->get('Accept-Language', null) ?
            explode(',', $request->headers->get('Accept-Language')) :
            [];
        $availableLocales = config('locale.languages');

        //set user default language as English
        $userLocale = $availableLocales['en'];

        // if it is auth user, get user locale from setting
        // if not found, use default locale
        if ($request->user()) {
            /* set user local if user have custom local */
            // $userLocale =  'en';
        }

        // Accept-Language in request header will override user setting, if any
        // check the request locale is available
        foreach ($clientLocales as $clientLocale) {
            if (in_array($clientLocale, array_keys($availableLocales))) {
                $userLocale = $clientLocale;
                break;
            }
        }
        App::setLocale($userLocale[0]);

        $contentLanguage = App::getLocale();
        $request->headers->set('Accept-Language', $contentLanguage);

        $response = $next($request);
        $response->headers->set('Content-Language', $contentLanguage);

        return $response;
    }
}
