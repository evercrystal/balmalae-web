<?php

namespace Modules\Api\Http\Requests\V1;

use Modules\Api\Traits\SanitizesRequest;
use Modules\Api\Http\Requests\ApiRequest;
use Illuminate\Validation\Rule;
use Validator;

class UserRequest extends ApiRequest
{
    use SanitizesRequest;

    public function getValidatorsForSave_register()
    {
        $inputValidator = Validator::make(
            $this->input(),
            [
                'username' =>  'required',
                // 'email' => ['required', 'email', 'max:191', Rule::unique('users')],
                'email' => ['nullable','max:191', Rule::unique('users')],
                'mobile' => ['required', 'valid_phone_number', Rule::unique('users')],
                'password' => 'required|min:6',
            ],
            [
                'valid_phone_number' => 'Invalid Mobile No. or Not Support Mobile No.'
            ]
        );

        return [
            ['validator' => $inputValidator, 'status_code' => 400, 'message_key' => 'bad_request']
        ];
    }

    public function getValidatorsForForgotPassword()
    {
        $inputValidator = Validator::make(
            $this->input(),
            [
                'email' =>  'required|email'
            ]
        );

        return [
            ['validator' => $inputValidator, 'status_code' => 400, 'message_key' => 'bad_request']
        ];
    }

    public function getValidatorsForUpdateProfile()
    {
        $inputValidator = Validator::make(
            $this->input(),
            [
                'user_name' =>  'required|max:191',
                // 'email'     =>  ['nullable','max:191', Rule::unique('users')],
                'email' => ['sometimes', 'required', 'email'],

            ]
        );

        return [
            ['validator' => $inputValidator, 'status_code' => 400, 'message_key' => 'bad_request']
        ];
    }

}