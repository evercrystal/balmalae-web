<?php

namespace Modules\Api\Http\Requests\V1;

use Modules\Api\Traits\SanitizesRequest;
use Modules\Api\Http\Requests\ApiRequest;
use Illuminate\Validation\Rule;
use Validator;

class CustomerRequest extends ApiRequest
{
    use SanitizesRequest;

    public function getValidatorsForVerifyCustomer()
    {
        $inputValidator = Validator::make(
            $this->input(),
            [
                'mobile' => 'required',
                'customerRefNo' => 'required',
                'hashValue' => 'required',
            ]
        );

        return [
            ['validator' => $inputValidator, 'status_code' => 400, 'message_key' => 'bad_request']
        ];
    }

    public function getValidatorsForGetLocationData()
    {
        $inputValidator = Validator::make(
            $this->input(),
            [
                'customerRefNo' => 'required',
                'hashValue' => 'required',
            ]
        );

        return [
            ['validator' => $inputValidator, 'status_code' => 400, 'message_key' => 'bad_request']
        ];
    }
}