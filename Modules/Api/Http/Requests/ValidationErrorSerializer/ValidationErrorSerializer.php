<?php

namespace Modules\Api\Http\Requests\ValidationErrorSerializer;

use Illuminate\Validation\Validator;

class ValidationErrorSerializer
{
    public function serialize(Validator $validator)
    {
        return $validator->errors();
    }
}