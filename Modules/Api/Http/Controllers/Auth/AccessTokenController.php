<?php

namespace Modules\Api\Http\Controllers\Auth;

use App\Models\Auth\User;
use Auth;
use Illuminate\Http\Response;
use Laravel\Passport\Http\Controllers\AccessTokenController as BaseAccessTokenController;
use Modules\Api\Repositories\OAuthRefreshTokenRepository;
use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Exceptions\OAuthServerException;
use Str;

class AccessTokenController extends BaseAccessTokenController
{
    public function revokeAccessToken(OAuthRefreshTokenRepository $oauthRefreshTokenRepository)
    {
        $userAccessToken = Auth::user()->token();

        $oauthRefreshTokenRepository->revokeRefreshTokenByAccessTokenId($userAccessToken->id);

        $userAccessToken->revoke();

        return new Response('', 204);
    }

    public function issueToken(ServerRequestInterface $request)
    {
        try {
            $response = parent::issueToken($request);
        } catch (OAuthServerException $e) {
            return new Response([
                'error' => trans('errors.invalid_credentials'),
                'message' => trans('errors.invalid_credentials_message')
            ], 400);
        }

        if ($response->getStatusCode() === 200) {
            $grantType = $this->getRequestParameter('grant_type', $request);

            // check user status if we're issuing the access token for the first time
            if ($grantType !== 'refresh_token') {
                $mobile = $this->getRequestParameter('username', $request);
                $user = (new User())->findForPassport($mobile);

                // if user is inactive, return 403 (unauthorized)
                if (!$user->isActive()) {
                    return new Response([
                        'error' => trans('errors.unverified_user'),
                        'message' => trans('errors.user_not_active')
                    ], 403);
                }
            }
        } elseif ($response->getStatusCode() === 400) {
            return new Response([
                'error' => trans('errors.invalid_credentials'),
                'message' => trans('errors.invalid_credentials_message')
            ], 401);
        }

        return $response;
    }

    protected function getRequestParameter($parameter, ServerRequestInterface $request, $default = null)
    {
        $requestParameters = (array) $request->getParsedBody();

        return isset($requestParameters[$parameter]) ? $requestParameters[$parameter] : $default;
    }
}
