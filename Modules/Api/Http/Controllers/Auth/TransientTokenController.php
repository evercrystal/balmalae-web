<?php

namespace Modules\Api\Http\Controllers\Auth;

use Laravel\Passport\Http\Controllers\TransientTokenController as PassportTransientTokenController;

class TransientTokenController extends PassportTransientTokenController
{
}
