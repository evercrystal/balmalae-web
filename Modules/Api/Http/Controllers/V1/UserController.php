<?php

namespace Modules\Api\Http\Controllers\V1;

use App\Models\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use League\Fractal\Manager;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Api\Http\Requests\V1\UserRequest;
use Modules\Api\Transformers\UserTransformer;
use Illuminate\Support\Facades\DB;
use App\Repositories\Frontend\Auth\UserRepository;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\GeneralException;

/**
 * @SWG\Definition(
 *   definition="ShowUser",
 *   type="object",
 *   required={"data"},
 *   @SWG\Property(
 *       property="data",
 *       type="object",
 *       ref="#/definitions/UserWithIncludes"
 *   ),
 * )
 */

class UserController extends ApiController
{

    protected $userRepository;
    /**
     * @param FractalManager $manager
     */
    public function __construct(Manager $manager, UserRepository $userRepository,User $usermodel)
    {
        parent::__construct($manager);
        $this->userRepository = $userRepository;
        $this->usermodel      = $usermodel;
    }

    public function index()
    {
        $user = Auth::user();
        if (!$user->active) {
            return $this->respondWithFieldError('user_not_active', 'username', 401);
        }

        return $this->respondWithItem($user, new UserTransformer());
    }

    public function register(UserRequest $request)
    {
        $credentials  = $request->only('username','email','mobile', 'password');
        $credentials['user_name'] = $credentials['username'];
        $credentials['email']     = $credentials['email'];
        $credentials['mobile']    = $credentials['mobile'];
        $credentials['password']  = $credentials['password'];
        $user = $this->userRepository->create($credentials);
        return $this->respondWithItem($user, new UserTransformer(), null, [], 201);
    }

    public function forgotPassword(UserRequest $request)
    {
        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );
        if ($response == Password::RESET_LINK_SENT) {
            return $this->respondWithFieldError('user_reset_password', 'message', 200);
        }
        return $this->respondWithFieldError('user_not_found', 'message', 404);
    }

    public function UpdateProfile(UserRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        if ($user->email !== $input['email'] && $this->usermodel->where('email', '=', $input['email'])->first()) 
        {
            return $this->respondWithError(400, trans('exceptions.backend.access.users.email_error'));
        }

        \DB::beginTransaction();
        try 
        {
            $user->update([
                'user_name' => $input['user_name'],
                'email' => $input['email'],
            ]);

            $user->visitor->update([
                'visitor_name' => $input['user_name'],
                'address' => $input['address'],
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondWithUnexpectedError('profile_update_error');
        }

        \DB::commit();

        return $this->respondWithArray(['http_code'=>200,'message'=>trans('alerts.successfully_update_profile')]);
    }

    public function changePassword(UserRequest $request)
    {
        $user = Auth::user();
        $input = $request->all();

        if (Hash::check($input['old_password'], $user->password)) {
            $user->update(['password' => $input['password']]);
            return $this->respondWithArray(['http_code'=>200,'message'=>trans('alerts.successfully_changed_password')]);

        }
        return $this->respondWithError(400, trans('exceptions.frontend.auth.password.change_mismatch'));
    }

}
