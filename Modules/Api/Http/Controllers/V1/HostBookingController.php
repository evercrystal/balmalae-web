<?php

namespace Modules\Api\Http\Controllers\V1;

use League\Fractal\Manager;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Host\Entities\Host;
use Modules\Host\Repositories\HostRepository;
use Modules\Api\Transformers\HostBookingTransformer;

class HostBookingController extends ApiController
{

    protected $hostbooking;

    /**
     * @param FractalManager $manager
     */
    public function __construct(Manager $manager, HostRepository $hostbooking)
    {
        parent::__construct($manager);
        $this->hostbooking = $hostbooking;
    }   

    /**
     * @return JSON
     */
    public function getHostBookingData($filterText)
    {
        if($filterText == "all")
        {
            $hostbookings = Host::where('is_active',1)
                ->join('host_category', 'host.host_category_id', '=', 'host_category.host_category_id')
                ->select('host.host_id as host_id','host.host_name as host_name','host_category.host_category_name as host_category_name','host.host_mobileno as host_mobile','host.address_detail as host_address','host.trip_status','host.sick_status','host.host_logo')
                ->get();
        }else
        {
            $hostbookings = Host::where('is_active',1)
                ->join('host_category', 'host.host_category_id', '=', 'host_category.host_category_id')
                ->where(function ($query) use ($filterText) {
                        $query->where('host.host_name', 'LIKE', "%$filterText%")
                            ->orWhere('host_category.host_category_name', 'LIKE', "%$filterText%");
                    })
                ->select('host.host_id as host_id','host.host_name as host_name','host_category.host_category_name as host_category_name','host.host_mobileno as host_mobile','host.address_detail as host_address','host.trip_status','host.sick_status','host.host_logo')
                ->get();
        }
        

        return response()->json($hostbookings, 200);
        
        
    }

    public function ScanandGetHostData($slug)
    {
        $hostbooking = Host::where('is_active',1)
                    ->where('slug', $slug)
                    ->join('host_category', 'host.host_category_id', '=', 'host_category.host_category_id')
                    ->select('host.host_id as host_id','host.host_name as host_name','host_category.host_category_name as host_category_name','host.host_mobileno as host_mobile','host.address_detail as host_address','host.trip_status','host.sick_status','host.is_active','host.host_logo')
                    ->first();

        return response()->json($hostbooking, 200);
    }

}
