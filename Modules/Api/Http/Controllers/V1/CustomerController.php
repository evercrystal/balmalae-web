<?php

namespace Modules\Api\Http\Controllers\V1;

use League\Fractal\Manager;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Api\Http\Requests\V1\CustomerRequest;
use Modules\Customer\Entities\Customer;
use Modules\Country\Repositories\CountryRepository;
use Modules\State\Repositories\StateRepository;
use Modules\City\Repositories\CityRepository;
use Modules\Township\Repositories\TownshipRepository;
use Modules\Api\Transformers\CountryTransformer;
use Modules\Api\Transformers\StateTransformer;
use Modules\Api\Transformers\CityTransformer;
use Modules\Api\Transformers\TownshipTransformer;
use App\Services\HashService;

class CustomerController extends ApiController
{

    protected $hashService;
    protected $country;
    protected $state;
    protected $city;
    protected $township;

    /**
     * @param FractalManager $manager
     */
    public function __construct(Manager $manager, HashService $hashService, CountryRepository $country, StateRepository $state, CityRepository $city, TownshipRepository $township)
    {
        parent::__construct($manager);
        $this->hashService = $hashService;
        $this->country = $country;
        $this->state = $state;
        $this->city = $city;
        $this->township = $township;
    }

    /**
     * @return JSON
     */
    public function verifyCustomer(CustomerRequest $request)
    {
        $input = $request->all();
        $customer = Customer::where('mobile',$input['mobile'])->where('ref_id',$input['customerRefNo'])->first();

        if($customer && $customer->user->active) {
            $toString = 'mobile='.$input['mobile'].'&customerRefNo='.$input['customerRefNo'];
            $hashEqual = $this->hashService->hashEqual($toString,$customer->hash_key,$input['hashValue']);
            if($hashEqual) {
                return $this->respondWithArray(['http_code'=>200,'message'=>'success']);
            }else {
                return $this->respondWithUnexpectedError('invalid_hash_value');
            }
        }
        return $this->respondWithUnexpectedError('customer_does_not_exist');
        
    }

    /**
     * @return JSON
     */
    public function getLocationData(CustomerRequest $request)
    {
        $input = $request->all();
        $customer = Customer::where('ref_id',$input['customerRefNo'])->first();
        
        if($customer && $customer->user->active) {
            $toString = 'customerRefNo='.$input['customerRefNo'];
            $hashEqual = $this->hashService->hashEqual($toString,$customer->hash_key,$input['hashValue']);
            if($hashEqual) {
                $countries = $this->transformCollection($this->country->getAll(), new CountryTransformer());
                $states = $this->transformCollection($this->state->getAll(), new StateTransformer());
                $cities = $this->transformCollection($this->city->getAll(), new CityTransformer());
                $townships = $this->transformCollection($this->township->getAll(), new TownshipTransformer());
                return $this->respondWithArray([
                    'http_code'=>200,
                    'message'=>'success',
                    'countries'=> $countries,
                    'states'=> $states,
                    'cities'=> $cities,
                    'townships'=> $townships
                ]);
            } else {
                return $this->respondWithUnexpectedError('invalid_hash_value');
            }
        }
        return $this->respondWithUnexpectedError('customer_does_not_exist');
        
    }

}
