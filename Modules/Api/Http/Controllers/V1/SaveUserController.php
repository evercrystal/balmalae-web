<?php

namespace Modules\Api\Http\Controllers\V1;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use League\Fractal\Manager;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Api\Http\Requests\V1\UserRequest;
use Modules\Api\Transformers\UserTransformer;
use Illuminate\Support\Facades\DB;
use App\Repositories\Frontend\Auth\UserRepository;
use Illuminate\Support\Facades\Hash;

/**
 * @SWG\Definition(
 *   definition="ShowUser",
 *   type="object",
 *   required={"data"},
 *   @SWG\Property(
 *       property="data",
 *       type="object",
 *       ref="#/definitions/UserWithIncludes"
 *   ),
 * )
 */

class SaveUserController extends ApiController
{

    protected $userRepository;
    /**
     * @param FractalManager $manager
     */
    public function __construct(Manager $manager, UserRepository $userRepository)
    {
        parent::__construct($manager);
        $this->userRepository = $userRepository;
    }

    public function save_register(UserRequest $request)
    {
        $credentials  = $request->only('username','email','mobile', 'password');
        $credentials['user_name'] = $credentials['username'];
        $credentials['email']     = $credentials['email'];
        $credentials['mobile']    = $credentials['mobile'];
        $credentials['password']  = $credentials['password'];
        $user = $this->userRepository->create($credentials);
        return $this->respondWithItem($user, new UserTransformer(), null, [], 201);
    }

}
