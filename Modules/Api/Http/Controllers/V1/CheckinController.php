<?php

namespace Modules\Api\Http\Controllers\V1;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Visitor\Entities\Visitor;
use Modules\Visitor\Repositories\VisitorRepository;
use Modules\Checkin\Entities\Checkin;
use Modules\Checkin\Repositories\CheckinRepository;
use Modules\Api\Transformers\HostBookingTransformer;
use DB;

class CheckinController extends ApiController
{

    protected $visitor;
    protected $checkin;

    /**
     * @param FractalManager $manager
     */
    public function __construct(Manager $manager, VisitorRepository $visitor, CheckinRepository $checkin)
    {
        parent::__construct($manager);
        $this->visitor = $visitor;
        $this->checkin = $checkin;
    }   

    /**
     * @return JSON
     */
    public function saveCheckinData(Request $request)
    {
        
        $input = $request->all();
        $exited_visitor = Visitor::where('mobile_no',$input['mobile_no'])->first();
        $current_date       = date('Y-m-d H:i:s');

        if($exited_visitor)
        {

            $exited_visitor->visitor_name = $input['visitor_name'];
            $exited_visitor->address = $input['visitor_address'];
            $exited_visitor->save();
            
            DB::table('checkin')->insert([
                'host_id'    => $input['host_id'],
                'visitor_id' => $exited_visitor->visitor_id,
                'is_sick'    => $input['is_sick'],
                'is_trip'    => $input['is_trip'],
                'created_at' => $current_date,
                'updated_at' => $current_date,
                
            ]);

            return response()->json(["message" => "Check In ၀င်ရောက်ခြင်းအောင်မြင်ပါသည်။ ", "status" => true],200);

        }else
        {
            $input['visitor_id'] = $this->visitor->saveVisitorinfo($input);

            DB::table('checkin')->insert([
                'host_id'    => $input['host_id'],
                'visitor_id' => $input['visitor_id'],
                'is_sick'    => $input['is_sick'],
                'is_trip'    => $input['is_trip'],
                'created_at' => $current_date,
                'updated_at' => $current_date,
                
            ]);

            return response()->json(["message" => "Check In ၀င်ရောက်ခြင်းအောင်မြင်ပါသည်။ ", "status" => true],200);

        }
    }

    public function getMyCheckInData($visitor_id)
    {
        $my_checkin_data = Checkin::where('visitor_id',$visitor_id)
                            ->join('host', 'host.host_id', '=', 'checkin.host_id')
                            ->select('host.host_name as host_name',
                                    'host.host_mobileno as host_mobile',
                                    'host.address_detail as host_address',
                                    'host.host_logo as host_logo',
                                    'checkin.created_at as checkin_date')
                            ->orderBy("checkin_date", "desc")
                            ->get();
        return response()->json($my_checkin_data, 200);

    }

}
