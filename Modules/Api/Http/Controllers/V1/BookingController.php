<?php

namespace Modules\Api\Http\Controllers\V1;

use League\Fractal\Manager;
use Modules\Api\Http\Controllers\ApiController;
use Modules\Host\Entities\Host;
use Modules\Host\Repositories\HostRepository;
use Modules\Api\Transformers\BoxTransformer;

class BoxController extends ApiController
{

    protected $box;

    /**
     * @param FractalManager $manager
     */
    public function __construct(Manager $manager, BoxRepository $box)
    {
        parent::__construct($manager);
        $this->box = $box;
    }   

    /**
     * @return JSON
     */
    public function getBoxData()
    {

        $boxes = $this->box->getActiveAll();

        return $this->respondWithCollection($boxes, new BoxTransformer());
        
    }

}
