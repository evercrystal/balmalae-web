<?php

Route::group(['middleware' => ['api'], 'as' => 'api.', 'prefix' => 'api', 'namespace' => 'Modules\Api\Http\Controllers'], function () {

	/** Login Authentication API */
    Route::group(['prefix' => 'oauth'], function () {
        Route::post('token', 'Auth\AccessTokenController@issueToken');
    });

    //**************************************  Protected API  ***********/
    Route::group(["middleware" => ['auth.default:api', 'locale', 'scope:full-access']], function () {
        //**************************************  OAuth API  ***********/
        Route::group(['prefix' => 'oauth'], function () {
            Route::post('token/refresh', 'Auth\TransientTokenController@refresh');

            Route::post('token/revoke', 'Auth\AccessTokenController@revokeAccessToken');
        });
        Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {

            Route::get('user', 'UserController@index');
            Route::post('user/update-profile', 'UserController@UpdateProfile');

        });
    });

	Route::group(['prefix' => 'v1', 'namespace' => 'V1'], function () {

        Route::post('user/register', 'UserController@register');

        Route::post('save-register-user', 'SaveUserController@save_register');

        Route::get('booking-data/{filterText}', 'HostBookingController@getHostBookingData');

        Route::get('scan-slug-and-get-data/{slug}', 'HostBookingController@ScanandGetHostData');

        Route::post('save-visitor-info', 'CheckinController@saveCheckinData');

        Route::get('get-my-checkin-data/{visitor_id}', 'CheckinController@getMyCheckInData');
        
        Route::post('user/forgot-password', 'UserController@forgotPassword');



        
    });

});