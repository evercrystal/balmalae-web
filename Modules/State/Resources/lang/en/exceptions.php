<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'state' => [
                'create_error'      => 'There was a problem creating this state. Please try again.',
                'delete_error'      => 'There was a problem deleting this state. Please try again.',
                'not_found'         => 'That state does not exist.',
                'update_error'      => 'There was a problem updating this state. Please try again.',
            ]
        ]
];
