<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'state' => [
                    'create'     => 'Create State',
                    'edit'       => 'Edit State',
                    'management' => 'State Management',
                    'list'       => 'State List',
                    'show'       => 'State Detail',

                    'table' => [
                        'number_of_users' => 'Number of States',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'country'          => 'Country Name',
                        'country_code'     => 'Country Code',

                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'state total|state total',
                    ]
                ]
            ]

];