<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'state' => [
            'not_deleted' => 'This state was used in other table. Cannot deleted yet.',
            'created' => 'The state was successfully created.',
            'deleted' => 'The state was successfully deleted.',
            'updated' => 'The state was successfully updated.'
        ]
    ]
];