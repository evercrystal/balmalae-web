<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'state' => 'State',
                'state_bin' => 'State Bin',
            ],
            'state' => [
                'all'        => 'All State',
                'create'     => 'Create State',
                'edit'       => 'Edit State',
                'show'       => 'Show State',
                'management' => 'State Management',
                'main'       => 'State',
            ]
        ]
];