@extends ('backend.layouts.app')

@section ('title', __('state::labels.backend.state.management') . ' | ' . __('state::labels.backend.state.edit'))

@section('breadcrumb-links')
    @include('state::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}

@endpush

@section('content')
{{ html()->modelForm($state, 'PATCH', route('admin.state.update', $state->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('state::labels.backend.state.management') }}
                        <small class="text-muted">{{ __('state::labels.backend.state.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                    {{ html()->label(__('state::labels.backend.state.table.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('state::labels.backend.state.table.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.state.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')

{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script>
$(function(){

    $(".select").select2({
        placeholder: 'Choose city',
        width: '100%'
    });
});
</script>
@endpush