@extends ('backend.layouts.app')

@section ('title', __('state::labels.backend.state.management'))

@section('breadcrumb-links')
    @include('state::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('state::labels.backend.state.management') }}
                    <small class="text-muted">{{ __('state::labels.backend.state.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ __('state::labels.backend.state.table.name') }}</th>
                            <td>{{ $state->name }}</td>
                        </tr>
                    </tbody>
                </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('state::labels.backend.state.table.created') }}:</strong> {{ $state->updated_at->timezone(get_user_timezone()) }} ({{ $state->created_at->diffForHumans() }}),
                    <strong>{{ __('state::labels.backend.state.table.last_updated') }}:</strong> {{ $state->created_at->timezone(get_user_timezone()) }} ({{ $state->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush