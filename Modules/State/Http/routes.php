<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\State\Http\Controllers'], function()
{
    /*
        * For DataTables
        */
    Route::post('state/get', 'StateTableController')->name('state.get');
    /*
        * User CRUD
        */
    Route::resource('state', 'StateController');
});