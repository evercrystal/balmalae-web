<?php

namespace Modules\State\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use app\Enums\Table;
class UpdateStateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'name' => 'required|max:191|unique:'.Table::STATE.',name,'.$id,
            'country_id' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit state');
    }
}
