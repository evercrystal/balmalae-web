<?php

Breadcrumbs::for('admin.state.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('state::labels.backend.state.management'), route('admin.state.index'));
});

Breadcrumbs::for('admin.state.create', function ($trail) {
    $trail->parent('admin.state.index');
    $trail->push(__('state::labels.backend.state.create'), route('admin.state.create'));
});

Breadcrumbs::for('admin.state.show', function ($trail, $id) {
    $trail->parent('admin.state.index');
    $trail->push(__('state::labels.backend.state.show'), route('admin.state.show', $id));
});

Breadcrumbs::for('admin.state.edit', function ($trail, $id) {
    $trail->parent('admin.state.index');
    $trail->push(__('state::labels.backend.state.edit'), route('admin.state.edit', $id));
});
