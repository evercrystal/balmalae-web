<?php

namespace Modules\State\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\State\Entities\State;
use Modules\State\Http\Requests\ManageStateRequest;
use Modules\State\Http\Requests\CreateStateRequest;
use Modules\State\Http\Requests\UpdateStateRequest;
use Modules\State\Http\Requests\ShowStateRequest;
use Modules\State\Repositories\StateRepository;
use Modules\City\Repositories\CityRepository;


class StateController extends Controller
{
 /**
     * @var StateRepository
     * @var CategoryRepository
     */
    protected $state;
    protected $city;
    /**
     * @param StateRepository $state
     */
    public function __construct(StateRepository $state, CityRepository $city)
    {
        $this->state = $state;
        $this->city = $city;

    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('state::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('state::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateStateRequest $request)
    {
        $this->state->create($request->except('_token','_method'));
        return redirect()->route('admin.state.index')->withFlashSuccess(trans('state::alerts.backend.state.created'));
    }

    /**
     * @param State              $state
     * @param ManageStateRequest $request
     *
     * @return mixed
     */
    public function edit(State $state, ManageStateRequest $request)
    {        
        return view('state::edit')
            ->withState($state);
    }

    /**
     * @param State              $state
     * @param UpdateStateRequest $request
     *
     * @return mixed
     */
    public function update(State $state, UpdateStateRequest $request)
    {
        $this->state->updateById($state->id,$request->except('_token','_method'));

        return redirect()->route('admin.state.index')->withFlashSuccess(trans('state::alerts.backend.state.updated'));
    }

    /**
     * @param State              $state
     * @param ManageStateRequest $request
     *
     * @return mixed
     */
    public function show(State $state, ShowStateRequest $request)
    {
        return view('state::show')->withState($state);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(State $state)
    {
        if(optional($state->city)->count()) 
        {
            return redirect()->route('admin.state.index')->withFlashDanger(trans('state::alerts.backend.state.not_deleted'));
        }
        $this->state->deleteById($state->id);

        return redirect()->route('admin.state.index')->withFlashSuccess(trans('state::alerts.backend.state.deleted'));
    }
}
