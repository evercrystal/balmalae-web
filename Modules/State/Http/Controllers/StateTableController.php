<?php

namespace Modules\State\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\State\Repositories\StateRepository;
use Modules\State\Http\Requests\ManageStateRequest;

class StateTableController extends Controller
{
    /**
     * @var StateRepository
     */
    protected $state;

    /**
     * @param StateRepository $state
     */
    public function __construct(StateRepository $state)
    {
        $this->state = $state;
    }

    /**
     * @param ManageStateRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageStateRequest $request)
    {
        return DataTables::of($this->state->getForDataTable())
            ->addColumn('actions', function ($state) {
                return $state->action_buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
