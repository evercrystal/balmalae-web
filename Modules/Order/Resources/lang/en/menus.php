<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'order' => 'Orders',
                'order_list' => 'Order List',
                'progress_list' => 'On Progress List',
                'deleted_list' => 'Deleted Order List',
                'order_bin' => 'Order Bin',
            ],
            'order' => [
                'all'        => 'All Order',
                'create'     => 'Create Order',
                'edit'       => 'Edit Order',
                'show'       => 'Show Order',
                'management' => 'Order Management',
                'main'       => 'Order',
            ]
        ]
];