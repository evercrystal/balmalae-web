<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'order' => [
                    'create'     => 'Create Order',
                    'edit'       => 'Edit Order',
                    'management' => 'Order Management',
                    'onprogress_management' => 'On Progress Order Management',
                    'deleted_management'    => 'Deleted Order Management',
                    'onprogress_list'       => 'On Progress Order List',
                    'deleted_list'          => 'Deleted Order List',
                    'list'       => 'Order List',
                    'show'       => 'Order Detail',

                    'table' => [
                        'number_of_users' => 'Number of Orders',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'ref_no'           => 'Ref No',
                        'name'             => 'Name',
                        'customer'         => 'Customer',
                        'from_city'        => 'From City',
                        'to_city'          => 'To City',
                        'from_township'    => 'From Township',
                        'to_township'      => 'To Township',
                        'supplier'         => 'Supplier',
                        'status'           => 'Status',
                        'total_price'      => 'Price',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'customer_info'    => 'Customer Information',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'order total|order total',
                    ]
                ]
            ]

];