@extends ('backend.layouts.app')

@section ('title', __('order::labels.backend.order.management'))

@section('breadcrumb-links')
    @include('order::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('order::labels.backend.order.management') }}
                    <small class="text-muted">{{ __('order::labels.backend.order.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.ref_no') }}</th>
                                <td>{{ $order->ref_no }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.created') }}</th>
                                <td>{{ $order->created_at }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.customer_info') }}</th>
                                <td>
                                    <p>{{$order->customer->name}}  ({{$order->customer->mobile}} )</p>
                                </td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.from_city') }}</th>
                                <td>{{ $order->fromCity->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.to_city') }}</th>
                                <td>{{ $order->toCity->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.from_township') }}</th>
                                <td>{{ $order->fromTownship->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.to_township') }}</th>
                                <td>{{ $order->toTownship->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.total_price') }}</th>
                                <td>{{ $order->total_price }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('order::labels.backend.order.table.status') }}</th>
                                <td>{!! $order->status_label !!}</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('order::labels.backend.order.table.created') }}:</strong> {{ $order->updated_at->timezone(get_user_timezone()) }} ({{ $order->created_at->diffForHumans() }}),
                    <strong>{{ __('order::labels.backend.order.table.last_updated') }}:</strong> {{ $order->created_at->timezone(get_user_timezone()) }} ({{ $order->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush