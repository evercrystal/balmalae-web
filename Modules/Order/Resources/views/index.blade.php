@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('order::labels.backend.order.management'))

@section('breadcrumb-links')
    @include('order::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                     @if($type == $listTypes[1])
                        {{ __('order::labels.backend.order.management') }} <small class="text-muted">{{ __('order::labels.backend.order.list') }}</small>
                    @elseif($type == $listTypes[2])
                        {{ __('order::labels.backend.order.onprogress_management') }} <small class="text-muted">{{ __('order::labels.backend.order.onprogress_list') }}</small>
                    @else
                        {{ __('order::labels.backend.order.deleted_management') }} <small class="text-muted">{{ __('order::labels.backend.order.deleted_list') }}</small>
                    @endif
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('order::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="order-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('order::labels.backend.order.table.id') }}</th>
                            <th>{{ __('order::labels.backend.order.table.ref_no') }}</th>
                            <th>{{ __('order::labels.backend.order.table.customer') }}</th>
                            <th>{{ __('order::labels.backend.order.table.from_city') }}</th>
                            <th>{{ __('order::labels.backend.order.table.to_city') }}</th>
                            <th>{{ __('order::labels.backend.order.table.from_township') }}</th>
                            <th>{{ __('order::labels.backend.order.table.to_township') }}</th>
                            <th>{{ __('order::labels.backend.order.table.supplier') }}</th>
                            <th>{{ __('order::labels.backend.order.table.total_price') }}</th>
                            <th>{{ __('order::labels.backend.order.table.status') }}</th>
                            <th>{{ __('order::labels.backend.order.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#order-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.order.get") !!}',
                    type: 'post',
                    data: function (d) {
                        d.data = {
                                list_type : "{{$type}}"
                            };
                        return d;
                    },
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'ref_no', name: 'ref_no'},
                    {data: 'customer_id', name: 'customer_id'},
                    {data: 'from_city_id', name: 'from_city_id'},
                    {data: 'to_city_id', name: 'to_city_id'},
                    {data: 'from_township_id', name: 'from_township_id'},
                    {data: 'to_township_id', name: 'to_township_id'},
                    {data: 'delivery_supplier_id', name: 'delivery_supplier_id'},
                    {data: 'total_price', name: 'total_price'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush