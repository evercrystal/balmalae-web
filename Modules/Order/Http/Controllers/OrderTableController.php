<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Order\Repositories\OrderRepository;
use Modules\Order\Http\Requests\ManageOrderRequest;

class OrderTableController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $order;

    /**
     * @param OrderRepository $order
     */
    public function __construct(OrderRepository $order)
    {
        $this->order = $order;
    }

    /**
     * @param ManageOrderRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageOrderRequest $request)
    {
        $input = $request->all();
        return DataTables::eloquent($this->order->getForDataTable($input))
            ->addColumn('actions', function ($order) {
                return $order->action_buttons;
            })
            ->editColumn('ref_no', function ($order) {
                return $order->reference_number;
            })
            ->editColumn('customer_id', function ($order) {
                return $order->customer->name;
            })
            ->editColumn('from_city_id', function ($order) {
                return $order->fromCity->name;
            })
            ->editColumn('to_city_id', function ($order) {
                return $order->toCity->name;
            })
            ->editColumn('from_township_id', function ($order) {
                return $order->fromTownship->name;
            })
            ->editColumn('to_township_id', function ($order) {
                return $order->toTownship->name;
            })
            ->editColumn('status', function ($order) {
                return $order->status_label;
            })
            ->rawColumns(['actions','ref_no','customer_id','from_city_id','to_city_id','from_township_id','to_township_id','status'])
            ->make(true);
    }
}
