<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Order\Http\Controllers'], function () {
    /*
     * For DataTables
     */
    Route::post('order/get', 'OrderTableController')->name('order.get');
    /*
     * User CRUD
     */
    Route::resource('order', 'OrderController');
    Route::get('progress_order', 'OrderController@onProgressOrderList')->name('order.progress_order');
    Route::get('deleted_order', 'OrderController@deletedOrderList')->name('order.deleted_order');
});
