<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\Customer\Entities\Customer;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\Order\Enum\OrderStatusType;
use App\Enums\Table;

class Order extends Model
{
    use SoftDeletes;
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = Table::ORDER;

    protected $fillable = ["id","from_city_id","to_city_id","from_township_id","to_township_id","delivery_supplier_id","ref_no","status","payment_complete","total_price"];

    /**
     * @return mixed
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

     /**
     * @return mixed
     */
    public function fromCity()
    {
        return $this->belongsTo(City::class, 'from_city_id');
    }

     /**
     * @return mixed
     */
    public function toCity()
    {
        return $this->belongsTo(City::class, 'to_city_id');
    }

    /**
     * @return mixed
     */
    public function fromTownship()
    {
        return $this->belongsTo(Township::class, 'from_township_id');
    }

    /**
     * @return mixed
     */
    public function toTownship()
    {
        return $this->belongsTo(Township::class, 'to_township_id');
    }

    /**
    * @return string
    */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete order')) {
            return '<a href="'.route('admin.order.destroy', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    public function getStatusLabelAttribute()
    {
        if ($this->status == OrderStatusType::ID_ON_PROGRESS) {
            return '<a href="#" style="color:red;">'.OrderStatusType::NAME_ON_PROGRESS.'</a>';
        } elseif ($this->status == OrderStatusType::ID_COMPLETE) {
            return '<a href="#" style="color:green;">'.OrderStatusType::NAME_COMPLETE.'</a>';
        } else {
             return '<a href="#" style="color:blue;">'.OrderStatusType::NAME_CANCELLED.'</a>';
        }
    }

    public function getReferenceNumberAttribute()
    {
        if ($this->status == OrderStatusType::ID_ON_PROGRESS) {
            return '<a href="'. route('admin.order.show', $this->id) .'" target="_blank" class="list-group-item " style="text-align:center;color:white;background: red;">' . $this->ref_no . '</a>';
        } elseif ($this->status == OrderStatusType::ID_COMPLETE) {
            return '<a href="'. route('admin.order.show', $this->id) .'" target="_blank" class="list-group-item " style="text-align:center;color:white;background: green;">' . $this->ref_no . '</a>';
        }else {
           return '<a href="'. route('admin.order.show', $this->id) .'" target="_blank" class="list-group-item " style="text-align:center;color:white;background: blue;">' . $this->ref_no . '</a>';
        }
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getDeleteButtonAttribute();
    }
}
