<?php

namespace Modules\Order\Repositories;

use Modules\Order\Entities\Order;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use Modules\Order\Enum\OrderStatusType;
use Modules\Order\Enum\OrderListType;

/**
 * Class OrderRepository.
 */
class OrderRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function __construct(Order $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable(array $input)
    {
        $order = $this->model->with([
            'customer' => function ($query) {
                return $query->select('id', 'name');
            }
        ]);
        if ($input['data']['list_type'] == OrderListType::NAME_DELETED) {
            return $order->onlyTrashed()->select('*')->orderBy('deleted_at', 'desc');
        }

        if ($input['data']['list_type'] == OrderListType::NAME_ON_PROGRESS) {
            return $order->where('status',OrderStatusType::ID_ON_PROGRESS)->select('*')->orderBy('deleted_at', 'desc');
        }
        return $order->select('*')->orderBy('created_at','desc');
    }
}
