<?php
namespace Modules\Order\Enum;

class OrderListType
{
    const ID_ALL =1;
    const ID_ON_PROGRESS =2 ;
    const ID_DELETED = 3;

    const NAME_ALL = "all";
    const NAME_ON_PROGRESS = "onprogress";
    const NAME_DELETED = "deleted";

    const AVAILABLES = [
        self::ID_ALL => self::NAME_ALL,
        self::ID_ON_PROGRESS => self::NAME_ON_PROGRESS,
        self::ID_DELETED => self::NAME_DELETED
    ];
}
