<?php
namespace Modules\Order\Enum;

class OrderStatusType
{
    const ID_ON_PROGRESS = 1;
    const ID_COMPLETE = 2;
    const ID_CANCELLED = 3;

    const NAME_ON_PROGRESS = "On Progress";
    const NAME_COMPLETE = "Complete";
    const NAME_CANCELLED = "Cancel";

    const AVAILABLES = [
        self::ID_ON_PROGRESS => self::NAME_ON_PROGRESS,
        self::ID_COMPLETE => self::NAME_COMPLETE,
        self::ID_CANCELLED => self::NAME_CANCELLED
    ];
}
