<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\Table;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Table::ORDER, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on(Table::CUSTOMER)->onDelete('cascade');
            $table->integer('from_city_id')->unsigned();
            $table->foreign('from_city_id')->references('id')->on(Table::CITY)->onDelete('cascade');
            $table->integer('to_city_id')->unsigned();
            $table->foreign('to_city_id')->references('id')->on(Table::CITY)->onDelete('cascade');
            $table->integer('from_township_id')->unsigned();
            $table->foreign('from_township_id')->references('id')->on(Table::TOWNSHIP)->onDelete('cascade');
            $table->integer('to_township_id')->unsigned();
            $table->foreign('to_township_id')->references('id')->on(Table::TOWNSHIP)->onDelete('cascade');
            $table->integer('delivery_supplier_id')->default(0);
            $table->string('ref_no')->unique();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('payment_complete')->default(0);
            $table->integer('total_price')->default(0);
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Table::ORDER);
    }
}
