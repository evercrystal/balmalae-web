<?php

return [
    'name' => 'AppSetting',
    'icon' => 'nav-icon fas fa-cog',
    'basic' => [
        'facebook' => env('FACEBOOK', ''),
        'name' => env('APP_NAME', 'Balmalae'),
        'email' => env('APP_EMAIL', 'bnfdelivery@gmail.com'),
        'qr_prefix' => env('QR_PREFIX', 'BNF'),
        'address' => env('APP_ADDRESS', 'No. 391 Waizayantar Road, Za/South Quarter, Thingangyun Township, Yangon, Myanmar.'),
        'phone' => env('APP_PHONE', '+959420077655,+959969910109'),
        'main_logo' => env('MAIN_LOGO', ''),
        'favicon' => env('APP_FAVICON', ''),
        'meta_keywords' => env('META_KEYWORDS'),
        'meta_description' => env('META_DESCRIPTION'),
        'date_format' => env('DATE_FORMAT', 'Y-m-d'),
        'time_format' => env('TIME_FORMAT'),
        'datetime_format' => env('DATETIME_FORMAT'),
        'youtubedemo' => env('YOUTUBEDEMO'),
        'map_key' => env('GOOGLE_MAP'),
        'appstore' => env('APPSTORE'),
        'playstore' => env('PLAYSTORE'),
        'app_dollar_rate' => env('APP_DOLLAR_RATE', ''),
    ],

    'email' => [
        'mail_driver' => env('MAIL_DRIVER', 'Smtp'),
        'mail_host' => env('MAIL_HOST', 'smtp.mailtrap.io'),
        'mail_port' => env('MAIL_PORT', '587'),
        'mail_username' => env('MAIL_USERNAME', '7d41168bba76b9'),
        'mail_password' => env('MAIL_PASSWORD', '338a6d5b5f94cd'),
        'mail_encryption' => env('MAIL_ENCRYPTION', 'tls'),
    ],

    'sms' => [
        'sms_noti_phone'     => env('SMS_NOTI_PHONE', '09969910132'),
        'telerivet_sms_enable' => env('TELERIVET_SMS_ENABLE', true),
        'telerivet_sms_token'  => env('TELERIVET_SMS_TOKEN', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg3Nzk3N2ZlNTVjNTE1MjYyZDVhNThjMjRmOWIzNjBiZjY2NDcxNDE4MDMwMWFhYTk2M2Q2ZGVlZjMwN2VlNDFjMjhkMzU2YjFlZDE2YWZhIn0.eyJhdWQiOiIxIiwianRpIjoiODc3OTc3ZmU1NWM1MTUyNjJkNWE1OGMyNGY5YjM2MGJmNjY0NzE0MTgwMzAxYWFhOTYzZDZkZWVmMzA3ZWU0MWMyOGQzNTZiMWVkMTZhZmEiLCJpYXQiOjE1MDU3OTA1MTUsIm5iZiI6MTUwNTc5MDUxNSwiZXhwIjoxODIxMzIzMzE1LCJzdWIiOiIxMSIsInNjb3BlcyI6WyJzbXMtcmVnaXN0ZXIiLCJzbXMtdHJhY2tpbmciLCJzbXMtc3Vic2NyaXB0aW9uIl19.e57zLlbes8BIFdM_xcBjlPLmcygqCIUgc1hYQMc7VqZuq6Fn1lICJISETWVmqE5hsv14gxlhV2bKKgex29K16RTnXHMIK877BOH9ImR4A0XhF6Dj4WquDursVqIG8Ckj-0ighO7tCx1D6fir99FTmByOTmnxpwGRq7RD1H_6u6OcxXFqHzU0qBIKifda4_ArStyjqjq2LlzTVswx65ZO9MKaWpkcLVL6zAnum-WLD991yM7o1bBkyUFw_WSyYkHwNDRiX1OL_uAQtlMlgpU4xBqQYY3FPYbJ1PYxsGfsAUxRIo7MESAlTDqvUt5-vgxrIVsoy8IvMi99HqqCCT64kXszx2DldN_mJA0lJH4_bdboMmZWhEwtbivsPV8taTBMEe6own0H4XidzDPl0RvoFhPDbp4LemmMmffk-387mUPFzRaOF1YCO7oJ5yrCB9r6qKaT_Ssn4XBdN8RAarPaU308t1TvROi_ovb-FVd5vnzwK445Kpz-ZSKlFGqwueVI3BV16a4zR8xk93PQ4pXMYuYrsYFDdPxub1-NDJMcTy3EISL3bhJP5FrNo3ZwGgBQd-8cOW0z1kfxhfjEIFajEuVeqzqzii9zOlqZ_Q-LVczxkrX2lI2v1Nmeu2z32LJPWprGFVUOg1HIu-ahYQ2449VYprpFoi7I_Z7Rcw04mQ8'),
        'telerivet_sms_server' => env('TELERIVET_SMS_SERVER', 'http://139.59.112.35/api/sms/send_sms'),
        'telerivet_sms_api_key' => env('TELERIVET_SMS_API_KEY', 'YEih4diBElwcRxXcC33YXIBbq85mgcKE'),
        'telerivet_sms_project_id'  => env('TELERIVET_SMS_PROJECT_ID', 'PJ622f4f10bfdcd52e')
    ],
    /**
     * Date Format
     */
    'date_format_list' => [
        'd-m-Y'  => date('d-m-Y'),
        'm-d-Y'  => date('m-d-Y'),
        'd/m/Y'  => date('d/m/Y'),
        'm/d/Y'  => date('m/d/Y'),
        'F d, Y' => date('F d, Y'),
        'M d, Y' => date('M d, Y'),
    ],

    
     /**
     * Time Format
     */
    'time_format_list' => [
        'h:i A' => '09:15 AM/PM',
        'H:i' => '21:15',
    ],

    
    /**
     * Date Time Format
     */
    'datetime_format_list' => [
        'd-m-Y h:i A' => date('d-m-Y').' 09:15 AM/PM',
        'd-m-Y H:i' => date('d-m-Y').' 21:15',
        'd/m/Y h:i A' => date('d/m/Y').' 09:15 AM/PM',
        'd/m/Y H:i' => date('m/d/Y').' 21:15',
        'F d, Y h:i A' => date('F d, Y').' 09:15 AM/PM',
        'F d, Y H:i' => date('F d, Y').' 21:15',
        'M d, Y h:i A' => date('M d, Y').' 09:15 AM/PM',
        'M d, Y H:i' => date('M d, Y').' 21:15',
    ],
];
