<?php

namespace Modules\RecycleBin\Repositories;

use Modules\RecycleBin\Entities\RecycleBin;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * Class RecycleBinRepository.
 */
class RecycleBinRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function __construct(RecycleBin $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable($submodule=null)
    {
        if($submodule){
            return $this->model->where('module',$submodule)
            ->select('*');
        }
        return $this->model
            ->select('*');
    }

    public function bin_check_deleted($module,$date)
    {
        if($module && $module = \Module::find($module)){
            $table = $module->getLowerName();
            $module_deleted = DB::table($table)->select('id')->whereNotNull('deleted_at');

            if($date){
                    $start_date = Carbon::parse($date);
                    $end_date   = Carbon::now();
                $module_deleted = $module_deleted->between('deleted_at',[$start_date,$end_date]);
            }

            $module_deleted = $module_deleted->get();

            foreach ($module_deleted as $key => $deleted) {
                if($this->model->where('related_row_id',$deleted->id)->where('module',$module->getName())->count() == 0){
                    $recyclebin = new RecycleBin;
                    $recyclebin->related_row_id = $deleted->id;
                    $recyclebin->module = $module->getName();
                    $recyclebin->eloquent = '\\Modules\\'.$module->getName().'\\Entities\\'.$module->getName();
                    $recyclebin->save();
                }
            }
        }
        else{
            foreach(\Module::getOrdered() as $module)
            {
                if($module->enabled() && !in_array($module->getLowerName(),config('backend.ignored_bin'))){
                    $table = $module->getLowerName();
                    $module_deleted = DB::table($table)->select('id')->whereNotNull('deleted_at');

                    if($date){
                            $start_date = Carbon::parse($date);
                            $end_date   = Carbon::now();
                        $module_deleted = $module_deleted->between('deleted_at',[$start_date,$end_date]);
                    }

                    $module_deleted = $module_deleted->get();

                    foreach ($module_deleted as $key => $deleted) {
                        if($this->model->where('related_row_id',$deleted->id)->where('module',$module->getName())->count() == 0){
                            $recyclebin = new RecycleBin;
                            $recyclebin->related_row_id = $deleted->id;
                            $recyclebin->module = $module->getName();
                            $recyclebin->eloquent = '\\Modules\\'.$module->getName().'\\Entities\\'.$module->getName();
                            $recyclebin->save();
                        }
                    }
                }
            }
        }

        return "Successfully Sync Recycle Bin.";
    }
}
