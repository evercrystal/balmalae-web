<?php

namespace Modules\RecycleBin\Entities;

use Illuminate\Database\Eloquent\Model;

class RecycleBin extends Model
{
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'recyclebin';

    protected $fillable = ["id"];

       /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
    	if(auth()->user()->can('view recyclebin')){
        	return '<a href="'.route('admin.recyclebin.show', $this).'" class="btn btn-sm btn-info"><i class="fas fa-eye" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'"></i></a>';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
    	if(auth()->user()->can('edit recyclebin')){
            return '<a href="'.route('admin.recyclebin.destroy', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.restore').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" class="btn btn-sm btn-primary"><i class="icon icon-refresh" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.restore').'"></i></a> ';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group" role="group" aria-label="User Actions">'.$this->show_button.''.$this->edit_button.'<div>';
    }
}
