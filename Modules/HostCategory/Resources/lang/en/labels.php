<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'hostcategory' => [
                    'create'     => 'Create HostCategory',
                    'edit'       => 'Edit HostCategory',
                    'management' => 'HostCategory Management',
                    'list'       => 'HostCategory List',
                    'show'       => 'HostCategory Detail',

                    'table' => [
                        'number_of_users' => 'Number of HostCategorys',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'mm_name'          => 'MM Name',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'hostcategory total|hostcategory total',
                    ]
                ]
            ]

];