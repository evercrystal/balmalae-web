<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'hostcategory' => [
            'created' => 'The hostcategory was successfully created.',
            'deleted' => 'The hostcategory was successfully deleted.',
            'updated' => 'The hostcategory was successfully updated.'
        ]
    ]
];