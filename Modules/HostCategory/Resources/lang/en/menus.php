<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'hostcategory' => 'HostCategory',
                'hostcategory_bin' => 'HostCategory Bin',
            ],
            'hostcategory' => [
                'all'        => 'All HostCategory',
                'create'     => 'Create HostCategory',
                'edit'       => 'Edit HostCategory',
                'show'       => 'Show HostCategory',
                'management' => 'HostCategory Management',
                'main'       => 'HostCategory',
            ]
        ]
];