@extends ('backend.layouts.app')

@section ('title', __('hostcategory::labels.backend.hostcategory.management'))

@section('breadcrumb-links')
    @include('hostcategory::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('hostcategory::labels.backend.hostcategory.management') }}
                    <small class="text-muted">{{ __('hostcategory::labels.backend.hostcategory.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('hostcategory::labels.backend.hostcategory.table.name') }}</th>
                                <td>{{ $hostcategory->host_category_name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('hostcategory::labels.backend.hostcategory.table.mm_name') }}</th>
                                <td>{{ $hostcategory->host_category_mmname }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('hostcategory::labels.backend.hostcategory.table.created') }}:</strong> {{ $hostcategory->updated_at->timezone(get_user_timezone()) }} ({{ $hostcategory->created_at->diffForHumans() }}),
                    <strong>{{ __('hostcategory::labels.backend.hostcategory.table.last_updated') }}:</strong> {{ $hostcategory->created_at->timezone(get_user_timezone()) }} ({{ $hostcategory->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush