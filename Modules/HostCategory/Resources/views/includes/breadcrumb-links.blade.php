<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('hostcategory::menus.backend.hostcategory.main') }}</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.hostcategory.index') }}">{{ __('hostcategory::menus.backend.hostcategory.all') }}</a>
                @can('create hostcategory')
                <a class="dropdown-item" href="{{ route('admin.hostcategory.create') }}">{{ __('hostcategory::menus.backend.hostcategory.create') }}</a>
                @endcan
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>