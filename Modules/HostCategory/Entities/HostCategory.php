<?php

namespace Modules\HostCategory\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Host\Entities\Host;
use Illuminate\Database\Eloquent\Model;

class HostCategory extends Model
{
    use SoftDeletes;
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'host_category';
    protected $primaryKey = 'host_category_id';

    protected $fillable = ["host_category_id", "host_category_name", "host_category_mmname"];
    
    public function hosts()
    {
        return $this->hasMany(Host::class);
    }
       /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
    	if(auth()->user()->can('view hostcategory')){
        	return '<a href="'.route('admin.hostcategory.show', $this->host_category_id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
    	if(auth()->user()->can('edit hostcategory')){
        	return '<a href="'.route('admin.hostcategory.edit', $this->host_category_id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
       	return '';
    }

     /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete hostcategory')) {
            return '<a href="'.route('admin.hostcategory.destroy', $this->host_category_id).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
            return $this->getShowButtonAttribute().$this->getEditButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
