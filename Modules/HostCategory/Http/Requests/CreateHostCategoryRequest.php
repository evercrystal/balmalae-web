<?php

namespace Modules\HostCategory\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Table;

class CreateHostCategoryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'host_category_name' => 'required|max:100|unique:'.Table::HOST_CATEGORY,
            'host_category_mmname' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create hostcategory');
    }
}
