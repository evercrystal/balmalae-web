<?php

namespace Modules\HostCategory\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\HostCategory\Entities\HostCategory;
use Modules\HostCategory\Http\Requests\ManageHostCategoryRequest;
use Modules\HostCategory\Http\Requests\CreateHostCategoryRequest;
use Modules\HostCategory\Http\Requests\UpdateHostCategoryRequest;
use Modules\HostCategory\Http\Requests\ShowHostCategoryRequest;
use Modules\HostCategory\Repositories\HostCategoryRepository;

class HostCategoryController extends Controller
{
 /**
     * @var HostCategoryRepository
     * @var CategoryRepository
     */
    protected $hostcategory;

    /**
     * @param HostCategoryRepository $hostcategory
     */
    public function __construct(HostCategoryRepository $hostcategory)
    {
        $this->hostcategory = $hostcategory;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('hostcategory::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hostcategory::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateHostCategoryRequest $request)
    {
        $this->hostcategory->create($request->except('_token','_method'));
        return redirect()->route('admin.hostcategory.index')->withFlashSuccess(trans('hostcategory::alerts.backend.hostcategory.created'));
    }

    /**
     * @param HostCategory              $hostcategory
     * @param ManageHostCategoryRequest $request
     *
     * @return mixed
     */
    public function edit(HostCategory $hostcategory, ManageHostCategoryRequest $request)
    {
        return view('hostcategory::edit', compact('hostcategory'))
            ->withHostCategory($hostcategory);
    }

    /**
     * @param HostCategory              $hostcategory
     * @param UpdateHostCategoryRequest $request
     *
     * @return mixed
     */
    public function update(HostCategory $hostcategory, UpdateHostCategoryRequest $request)
    {
        $request->validate([
            'host_category_name' => 'required|max:100|unique:host_category,host_category_name,'.$hostcategory->host_category_id.',host_category_id',
        ]);
        $this->hostcategory->updateById($hostcategory->host_category_id,$request->except('_token','_method'));

        return redirect()->route('admin.hostcategory.index')->withFlashSuccess(trans('hostcategory::alerts.backend.hostcategory.updated'));
    }

    /**
     * @param HostCategory              $hostcategory
     * @param ManageHostCategoryRequest $request
     *
     * @return mixed
     */
    public function show(HostCategory $hostcategory, ShowHostCategoryRequest $request)
    {
        return view('hostcategory::show', compact('hostcategory'))->withHostCategory($hostcategory);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(HostCategory $hostcategory)
    {
        $this->hostcategory->deleteById($hostcategory->host_category_id);

        return redirect()->route('admin.hostcategory.index')->withFlashSuccess(trans('hostcategory::alerts.backend.hostcategory.deleted'));
    }
}
