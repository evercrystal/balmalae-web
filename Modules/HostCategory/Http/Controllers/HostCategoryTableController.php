<?php

namespace Modules\HostCategory\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\HostCategory\Repositories\HostCategoryRepository;
use Modules\HostCategory\Http\Requests\ManageHostCategoryRequest;

class HostCategoryTableController extends Controller
{
    /**
     * @var HostCategoryRepository
     */
    protected $hostcategory;

    /**
     * @param HostCategoryRepository $hostcategory
     */
    public function __construct(HostCategoryRepository $hostcategory)
    {
        $this->hostcategory = $hostcategory;
    }

    /**
     * @param ManageHostCategoryRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageHostCategoryRequest $request)
    {
        return DataTables::of($this->hostcategory->getForDataTable())
            ->addColumn('actions', function ($hostcategory) {
                return $hostcategory->action_buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
