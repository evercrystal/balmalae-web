<?php

Breadcrumbs::for('admin.hostcategory.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('hostcategory::labels.backend.hostcategory.management'), route('admin.hostcategory.index'));
});

Breadcrumbs::for('admin.hostcategory.create', function ($trail) {
    $trail->parent('admin.hostcategory.index');
    $trail->push(__('hostcategory::labels.backend.hostcategory.create'), route('admin.hostcategory.create'));
});

Breadcrumbs::for('admin.hostcategory.show', function ($trail, $id) {
    $trail->parent('admin.hostcategory.index');
    $trail->push(__('hostcategory::labels.backend.hostcategory.show'), route('admin.hostcategory.show', $id));
});

Breadcrumbs::for('admin.hostcategory.edit', function ($trail, $id) {
    $trail->parent('admin.hostcategory.index');
    $trail->push(__('hostcategory::labels.backend.hostcategory.edit'), route('admin.hostcategory.edit', $id));
});
