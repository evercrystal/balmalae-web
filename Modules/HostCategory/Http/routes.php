<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\HostCategory\Http\Controllers'], function()
{
    		/*
             * For DataTables
             */
            Route::post('hostcategory/get', 'HostCategoryTableController')->name('hostcategory.get');
            /*
             * User CRUD
             */
            Route::resource('hostcategory', 'HostCategoryController');
});