<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\Table;
class CreateRouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Table::ROUTE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('to_city_id')->unsigned();
            $table->foreign('to_city_id')->references('id')->on(Table::CITY)->onDelete('cascade');
            $table->integer('from_city_id')->unsigned();
            $table->foreign('from_city_id')->references('id')->on(Table::CITY)->onDelete('cascade');
            $table->tinyInteger('is_active')->default(0);
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Table::ROUTE);
    

    }
}
