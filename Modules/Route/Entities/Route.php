<?php

namespace Modules\Route\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Enums\Table;
use Modules\City\Entities\City;
use Modules\SubRoute\Entities\SubRoute;

class Route extends Model
{
    use SoftDeletes;
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = Table::ROUTE;

    protected $fillable = ["id", "from_city_id", "to_city_id", "is_active"];

    public function subroute()
    {
        return $this->belongsToMany(SubRoute::class, Table::PIVOT_ROUTE, 'route_id', 'sub_route_id');
    }
    
    public function toCity()
    {
        return $this->belongsTo(City::class, 'to_city_id');
    }
    
    public function fromCity()
    {
        return $this->belongsTo(City::class, 'from_city_id');
    }
       /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
    	if(auth()->user()->can('view route')){
        	return '<a href="'.route('admin.route.show', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
    	if(auth()->user()->can('edit route')){
        	return '<a href="'.route('admin.route.edit', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
       	return '';
    }

     /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete route')) {
            return '<a href="'.route('admin.route.destroy', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }
    public function getStatusLabelAttribute()
    {
        if ($this->is_active == 1) {
            return '<span class="badge badge-success">active</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }
    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
            return $this->getShowButtonAttribute().$this->getEditButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
