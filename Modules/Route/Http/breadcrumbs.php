<?php

Breadcrumbs::for('admin.route.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('route::labels.backend.route.management'), route('admin.route.index'));
});

Breadcrumbs::for('admin.route.create', function ($trail) {
    $trail->parent('admin.route.index');
    $trail->push(__('route::labels.backend.route.create'), route('admin.route.create'));
});

Breadcrumbs::for('admin.route.show', function ($trail, $id) {
    $trail->parent('admin.route.index');
    $trail->push(__('route::labels.backend.route.show'), route('admin.route.show', $id));
});

Breadcrumbs::for('admin.route.edit', function ($trail, $id) {
    $trail->parent('admin.route.index');
    $trail->push(__('route::labels.backend.route.edit'), route('admin.route.edit', $id));
});
