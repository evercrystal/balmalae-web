<?php

namespace Modules\Route\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Route\Repositories\RouteRepository;
use Modules\Route\Http\Requests\ManageRouteRequest;

class RouteTableController extends Controller
{
    /**
     * @var RouteRepository
     */
    protected $route;

    /**
     * @param RouteRepository $route
     */
    public function __construct(RouteRepository $route)
    {
        $this->route = $route;
    }

    /**
     * @param ManageRouteRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageRouteRequest $request)
    {
        return DataTables::of($this->route->getForDataTable())
            ->addColumn('actions', function ($route) {
                return $route->action_buttons;
            }) 
            ->addColumn('to_city_id', function ($route) {
                return $route->toCity->name;
            })
            ->addColumn('from_city_id', function ($route) {
                return $route->fromCity->name;
            })
            ->addColumn('status', function ($route) {
                return $route->status_label;
            })
        ->rawColumns(['actions','to_city_id', 'from_city_id','status'])
            ->make(true);
    }
}
