<?php

namespace Modules\Route\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Route\Entities\Route;
use Modules\Route\Http\Requests\ManageRouteRequest;
use Modules\Route\Http\Requests\CreateRouteRequest;
use Modules\Route\Http\Requests\UpdateRouteRequest;
use Modules\Route\Http\Requests\ShowRouteRequest;
use Modules\Route\Repositories\RouteRepository;
use Modules\City\Repositories\CityRepository;

class RouteController extends Controller
{
 /**
     * @var RouteRepository
     * @var CategoryRepository
     */
    protected $route;
    protected $city;
    /**
     * @param RouteRepository $route
     */
    public function __construct(RouteRepository $route, CityRepository $city)
    {
        $this->route = $route;
        $this->city = $city;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('route::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $cities = $this->city->getAll('id','asc');
        return view('route::create',compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateRouteRequest $request)
    {
        $this->route->create($request->except('_token','_method'));
        return redirect()->route('admin.route.index')->withFlashSuccess(trans('route::alerts.backend.route.created'));
    }

    /**
     * @param Route              $route
     * @param ManageRouteRequest $request
     *
     * @return mixed
     */
    public function edit(Route $route, ManageRouteRequest $request)
    {
        $cities = $this->city->getAll('id','asc');
        return view('route::edit',compact('cities'))
            ->withRoute($route);
    }

    /**
     * @param Route              $route
     * @param UpdateRouteRequest $request
     *
     * @return mixed
     */
    public function update(Route $route, UpdateRouteRequest $request)
    {
        $request['is_active'] = $request->is_active ? $request->is_active : 0;
        $this->route->updateById($route->id,$request->except('_token','_method'));

        return redirect()->route('admin.route.index')->withFlashSuccess(trans('route::alerts.backend.route.updated'));
    }

    /**
     * @param Route              $route
     * @param ManageRouteRequest $request
     *
     * @return mixed
     */
    public function show(Route $route, ShowRouteRequest $request)
    {
        return view('route::show')->withRoute($route);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Route $route)
    {
        if(optional($route->subroute)->count())
        {
            return redirect()->route('admin.route.index')->withFlashDanger(trans('route::alerts.backend.route.not_deleted'));
        }
        $this->route->deleteById($route->id);

        return redirect()->route('admin.route.index')->withFlashSuccess(trans('route::alerts.backend.route.deleted'));
    }
}
