<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Route\Http\Controllers'], function()
{
    /*
        * For DataTables
        */
    Route::post('route/get', 'RouteTableController')->name('route.get');
    /*
        * User CRUD
        */
    Route::resource('route', 'RouteController');
});