@extends ('backend.layouts.app')

@section ('title', __('route::labels.backend.route.management'))

@section('breadcrumb-links')
    @include('route::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('route::labels.backend.route.management') }}
                    <small class="text-muted">{{ __('route::labels.backend.route.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ __('route::labels.backend.route.table.to_city') }}</th>
                            <td>{{ $route->toCity->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('route::labels.backend.route.table.from_city') }}</th>
                            <td>{{ $route->fromCity->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('route::labels.backend.route.table.is_active') }}</th>
                            <td>
                                @if($route->is_active == 1)
                                On
                                @else
                                off
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('route::labels.backend.route.table.created') }}:</strong> {{ $route->updated_at->timezone(get_user_timezone()) }} ({{ $route->created_at->diffForHumans() }}),
                    <strong>{{ __('route::labels.backend.route.table.last_updated') }}:</strong> {{ $route->created_at->timezone(get_user_timezone()) }} ({{ $route->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush