@extends ('backend.layouts.app')

@section ('title', __('route::labels.backend.route.management') . ' | ' . __('route::labels.backend.route.create'))

@section('breadcrumb-links')
    @include('route::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
@endpush

@section('content')
{{ html()->form('POST', route('admin.route.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('route::labels.backend.route.management') }}
                        <small class="text-muted">{{ __('route::labels.backend.route.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">

                    
                    <div class="form-group row">
                    {{ html()->label(__('route::labels.backend.route.table.from_city').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('type') }}

                        <div class="col-md-10">
                            <select name="from_city_id" id="" class="form-control select2">
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{ $city->name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('route::labels.backend.route.table.to_city').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('type') }}

                        <div class="col-md-10">
                            <select name="to_city_id" id="" class="form-control select2">
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{ $city->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('route::labels.backend.route.table.active'))->class('col-md-2 form-control-label')->for('active') }}

                        <div class="col-md-10">
                            <label class="switch switch-label switch-pill switch-primary">
                                {{ html()->checkbox('is_active', true)->class('switch-input') }}
                                <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                            </label>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.route.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')

{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script>
    $(function(){

        $(".select").select2({
            placeholder: 'Choose city',
            width: '100%'
        });
    });
</script>

@endpush