@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('route::labels.backend.route.management'))

@section('breadcrumb-links')
    @include('route::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('route::labels.backend.route.management') }} <small class="text-muted">{{ __('route::labels.backend.route.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('route::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="route-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('route::labels.backend.route.table.id') }}</th>
                            <th>{{ __('route::labels.backend.route.table.from_city') }}</th>
                            <th>{{ __('route::labels.backend.route.table.to_city') }}</th>
                            <th>{{ __('route::labels.backend.route.table.status') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#route-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.route.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'from_city_id', name: 'from_city_id'},
                    {data: 'to_city_id', name: 'to_city_id'},
                    {data: 'status', name: 'status'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush