<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'route' => [
                    'create'     => 'Create Route',
                    'edit'       => 'Edit Route',
                    'management' => 'Route Management',
                    'list'       => 'Route List',
                    'show'       => 'Route Detail',

                    'table' => [
                        'number_of_users' => 'Number of Routes',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'from_city'        => 'From City',
                        'to_city'          => 'To City',
                        'is_active'        => 'Is Active',
                        'active'           => 'Active',
                        'status'           => 'Status',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'route total|route total',
                    ]
                ]
            ]

];