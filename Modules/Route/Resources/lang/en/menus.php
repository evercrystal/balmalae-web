<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'route' => 'Route',
                'subroute' => 'Sub Route',
                'route_bin' => 'Route Bin',
            ],
            'route' => [
                'all'        => 'All Route',
                'create'     => 'Create Route',
                'edit'       => 'Edit Route',
                'show'       => 'Show Route',
                'management' => 'Route Management',
                'main'       => 'Route',
            ],
            'subroute' => [
                'all'        => 'All Sub Route',
                'create'     => 'Create Sub Route',
                'edit'       => 'Edit Sub Route',
                'show'       => 'Show Sub Route',
                'management' => 'Sub Route Management',
                'main'       => 'Sub Route',
            ]
        ]
];