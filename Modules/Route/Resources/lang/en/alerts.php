<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'route' => [
            'created' => 'The route was successfully created.',
            'deleted' => 'The route was successfully deleted.',
            'not_deleted' => 'The route was not successfully deleted.It is used in Sub Route!',
            'updated' => 'The route was successfully updated.'
        ]
    ]
];