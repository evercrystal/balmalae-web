<?php

namespace Modules\Email\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Email\Repositories\EmailRepository;
use Modules\Email\Http\Requests\ManageEmailRequest;

class EmailTableController extends Controller
{
    /**
     * @var EmailRepository
     */
    protected $email;

    /**
     * @param EmailRepository $email
     */
    public function __construct(EmailRepository $email)
    {
        $this->email = $email;
    }

    /**
     * @param ManageEmailRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageEmailRequest $request)
    {
        return DataTables::of($this->email->getForDataTable())
            ->editColumn('slug', function ($email) {
                return $email->slug;
            })
            ->editColumn('content', function ($email) {
                return $email->content;
            })
            ->addColumn('actions', function ($email) {
                return $email->action_buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
