@extends ('backend.layouts.app')

@section ('title', __('email::labels.backend.email.management') . ' | ' . __('email::labels.backend.email.edit'))

@section('breadcrumb-links')
    @include('email::includes.breadcrumb-links')
@endsection

@push('after-styles')
    <link rel="stylesheet" type="text/css" href="/css/plugin/bootstrap-summernote/summernote.css">
@endpush

@section('content')
{{ html()->modelForm($email, 'PATCH', route('admin.email.update', $email->email_id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('email::labels.backend.email.management') }}
                        <small class="text-muted">{{ __('email::labels.backend.email.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                    {{ html()->label(__('email::labels.backend.email.table.slug'))->class('col-md-2 form-control-label')->for('slug') }}

                        <div class="col-md-10">
                            {{ html()->text('slug')
                                ->class('form-control')
                                ->placeholder(__('email::labels.backend.email.table.slug'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('email::labels.backend.email.table.subject'))->class('col-md-2 form-control-label')->for('subject') }}

                        <div class="col-md-10">
                            {{ html()->text('subject')
                                ->class('form-control')
                                ->placeholder(__('email::labels.backend.email.table.subject'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('email::labels.backend.email.table.content'))->class('col-md-2 form-control-label')->for('content') }}

                        <div class="col-md-10">
                            {{ html()->textarea('content')
                                ->class('form-control editor')
                                ->placeholder(__('email::labels.backend.email.table.content'))
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('email::labels.backend.email.table.mm_subject'))->class('col-md-2 form-control-label')->for('mm_subject') }}

                        <div class="col-md-10">
                            {{ html()->text('mm_subject')
                                ->class('form-control')
                                ->placeholder(__('email::labels.backend.email.table.mm_subject'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('email::labels.backend.email.table.mm_content'))->class('col-md-2 form-control-label')->for('mm_content') }}

                        <div class="col-md-10">
                            {{ html()->textarea('mm_content')
                                ->class('form-control editor')
                                ->placeholder(__('email::labels.backend.email.table.mm_content'))
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.email.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')
<script src="/js/plugin/bootstrap-summernote/summernote.min.js"></script>
<script type="text/javascript">
        
       $(document).ready(function(){

              $('.editor').summernote({
              height: 400
            });

             
        });


</script>
@endpush