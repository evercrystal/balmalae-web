<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'email' => [
                    'create'     => 'Create Email',
                    'edit'       => 'Edit Email',
                    'management' => 'Email Management',
                    'list'       => 'Email List',
                    'show'       => 'Email Detail',

                    'table' => [
                        'number_of_users' => 'Number of Emails',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'slug'             => 'Slug',
                        'slug-text'        => 'Eg.Member_forgot_password',
                        'content'          => 'Content',
                        'mm_content'       => 'Myanmar Content',
                        'subject'          => 'Subject',
                        'mm_subject'       => 'Myanmar Subject',
                        'total'            => 'email total|email total',
                    ]
                ]
            ]

];