<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Customer\Http\Controllers'], function () {
    /*
     * For DataTables
     */
    Route::post('customer/get', 'CustomerTableController')->name('customer.get');
    Route::post('customer/get-location', 'CustomerLocationTableController')->name('customer.get_location');
    
    Route::get('customer/{customerId}/location-create', 'CustomerController@createLocation')->name('customer.create_location');
    Route::post('customer/{customerId}/location-create', 'CustomerController@storeLocation')->name('customer.store_location');
    Route::delete('customer-location/{locationId}', 'CustomerController@destroyLocation')->name('customer.delete_location');
    Route::get('customer-location/{id}/edit', 'CustomerController@editLocation')->name('customer.edit_location');
    Route::patch('customer-location/{id}', 'CustomerController@updateLocation')->name('customer.update_location');
    
    Route::get('customer/get-state/{id}', 'CustomerController@getState')->name('customer.get_state');
    Route::get('customer/get-city/{id}', 'CustomerController@getCity')->name('customer.get_city');
    Route::get('customer/get-township/{id}', 'CustomerController@getTownship')->name('customer.get_township');

    /*
    * User CRUD
    */
    Route::resource('customer', 'CustomerController');
});
