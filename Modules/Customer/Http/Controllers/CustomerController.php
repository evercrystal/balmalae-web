<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Customer\Entities\Customer;
use Modules\State\Entities\State;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\Customer\Entities\CustomerLocation;
use Modules\Customer\Http\Requests\ManageCustomerRequest;
use Modules\Customer\Http\Requests\CreateCustomerRequest;
use Modules\Customer\Http\Requests\UpdateCustomerRequest;
use Modules\Customer\Http\Requests\CreateCustomerLocationRequest;
use Modules\Customer\Http\Requests\UpdateCustomerLocationRequest;
use Modules\Customer\Http\Requests\ShowCustomerRequest;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Country\Repositories\CountryRepository;
use Modules\City\Repositories\CityRepository;
use Modules\Township\Repositories\TownshipRepository;
use Modules\State\Repositories\StateRepository;
use Modules\Customer\Enum\CustomerType;

class CustomerController extends Controller
{
    /**
        * @var CustomerRepository
        * @var CategoryRepository
        */
    protected $customer;
    protected $country;
    protected $city;
    protected $township;
    protected $state;

    /**
     * @param CustomerRepository $customer
     */
    public function __construct(CustomerRepository $customer, CountryRepository $country, CityRepository $city, StateRepository $state, TownshipRepository $township)
    {
        $this->state = $state;
        $this->customer = $customer;
        $this->country = $country;
        $this->city = $city;
        $this->township = $township;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('customer::index');
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $countries = $this->country->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id','asc');
        $townships = $this->township->getAll('id','asc');
        $types = CustomerType::AVAILABLES;
        return view('customer::create', compact('countries', 'cities', 'townships', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->except('_token', '_method');

        $this->customer->create($input);

        return redirect()->route('admin.customer.index')->withFlashSuccess(trans('customer::alerts.backend.customer.created'));
    }

    /**
     * @param Customer              $customer
     * @param ManageCustomerRequest $request
     *
     * @return mixed
     */
    public function edit(Customer $customer, ManageCustomerRequest $request)
    {
        $types = CustomerType::AVAILABLES;
        return view('customer::edit', compact('types'))
            ->withCustomer($customer);
    }

    /**
     * @param Customer              $customer
     * @param UpdateCustomerRequest $request
     *
     * @return mixed
     */
    public function update(Customer $customer, UpdateCustomerRequest $request)
    {
        $input = $request->except('_token', '_method');

        $this->customer->update($customer, $input);

        return redirect()->route('admin.customer.index')->withFlashSuccess(trans('customer::alerts.backend.customer.updated'));
    }

    /**
     * @param Customer              $customer
     * @param ManageCustomerRequest $request
     *
     * @return mixed
     */
    public function show(Customer $customer, ShowCustomerRequest $request)
    {
        $types = CustomerType::AVAILABLES;
        $customerType = $types[$customer->type];
        return view('customer::show',compact('customerType'))->withCustomer($customer);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Customer $customer)
    {
        $this->customer->destroy($customer->id);
        \Log::info('Customer '.$customer->ref_id.' was deleted by ' . auth()->user()->name);
        return redirect()->route('admin.customer.index')->withFlashSuccess(trans('customer::alerts.backend.customer.deleted'));
    }

    public function createLocation(int $id)
    {
        $customer = Customer::find($id);
        $countries = $this->country->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id','asc');
        $townships = $this->township->getAll('id','asc');
        return view('customer::location.create', compact('countries', 'cities', 'townships', 'customer'));
    }

    public function storeLocation(int $id, CreateCustomerLocationRequest $request)
    {
        $input = $request->except('_token', '_method');
        $input['customer_id'] = $id;
        
        $location = $this->customer->createLocation($input);
        $customer = $location->customer;

        return redirect()->route('admin.customer.show', $customer->id)->withCustomer($customer)->withFlashSuccess(trans('customer::alerts.backend.customer.location_created'));
    }

    public function editLocation(int $id)
    {
        $location = CustomerLocation::find($id);
        $countries = $this->country->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id','asc');
        $states = $this->state->getAll('id','asc');
        $townships = $this->township->getAll('id','asc');
        return view('customer::location.edit', compact('countries', 'cities', 'states', 'townships'))
            ->withLocation($location);
    }

    public function updateLocation(int $id, UpdateCustomerLocationRequest $request)
    {
        $input = $request->except('_token', '_method');
        $input['location_id'] = $id;

        $location = $this->customer->updateLocation($input);
        $customer = $location->customer;

        return redirect()->route('admin.customer.show', $customer->id)->withCustomer($customer)->withFlashSuccess(trans('customer::alerts.backend.customer.location_updated'));
    }

    public function destroyLocation(int $id)
    {
        $location = CustomerLocation::find($id);
        $customer = $location->customer;
        $location->delete();
        return redirect()->route('admin.customer.show', $customer->id)->withCustomer($customer)->withFlashSuccess(trans('customer::alerts.backend.customer.location_deleted'));
    }

    public function getState(int $id)
    {
        $states = State::Where('country_id',$id)->get();
        return $states;
    }

    public function getCity(int $id)
    {
        $cities = City::Where('state_id',$id)->get();
        return $cities;
    }

    public function getTownship(int $id)
    {
        $township = Township::Where('city_id', $id)->get();
        return $township;
    }

}
