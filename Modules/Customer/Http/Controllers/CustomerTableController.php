<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Customer\Http\Requests\ManageCustomerRequest;

class CustomerTableController extends Controller
{
    /**
     * @var CustomerRepository
     */
    protected $customer;

    /**
     * @param CustomerRepository $customer
     */
    public function __construct(CustomerRepository $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param ManageCustomerRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCustomerRequest $request)
    {
        return DataTables::of($this->customer->getForDataTable())
            ->addColumn('actions', function ($customer) {
                return $customer->action_buttons;
            })
            ->addColumn('status', function ($customer) {
                return $customer->status_label;
            })
            ->rawColumns(['actions', 'status'])
            ->make(true);
    }
}
