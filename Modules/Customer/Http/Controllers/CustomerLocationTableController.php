<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Customer\Repositories\CustomerRepository;
use Modules\Customer\Http\Requests\ManageCustomerRequest;

class CustomerLocationTableController extends Controller
{
    /**
     * @var CustomerRepository
     */
    protected $customer;

    /**
     * @param CustomerRepository $customer
     */
    public function __construct(CustomerRepository $customer)
    {
        $this->customer = $customer;
    }

    /**
     * @param ManageCustomerRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCustomerRequest $request)
    {
        $input = $request->all();
        return DataTables::of($this->customer->getLocationForDataTable($input))
            ->addColumn('actions', function ($location) {
                return $location->action_buttons;
            })
            ->editColumn('country_id', function ($location) {
                return $location->country->name;
            })
            ->editColumn('state_id', function ($location) {
                return $location->state->name;
            })
            ->editColumn('city_id', function ($location) {
                return $location->city->name;
            })
            ->editColumn('township_id', function ($location) {
                return $location->township->name;
            })
            ->rawColumns(['actions', 'country_id','city_id','township_id'])
            ->make(true);
    }
}
