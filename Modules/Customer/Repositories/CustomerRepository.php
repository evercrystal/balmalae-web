<?php

namespace Modules\Customer\Repositories;

use Modules\Customer\Entities\Customer;
use Modules\Customer\Entities\CustomerLocation;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Backend\Auth\UserRepository;
use GMBF\PhoneNumber;

/**
 * Class CustomerRepository.
 */
class CustomerRepository extends BaseRepository
{
    protected $userRepository;
    /**
     * @return string
     */
    public function __construct(Customer $model, UserRepository $userRepository)
    {
        $this->model = $model;
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }

    public function create(array $input): bool
    {
        $customerName = explode(' ', $input['name']);
        $firstname = $customerName[0];
        unset($customerName[0]);
        $lastname  = implode(' ', $customerName);

        $phoneNumber = new PhoneNumber();
        $input['mobile'] = $phoneNumber->add_prefix($input['mobile']);
        $input['ref_id'] = $this->generateReferenceId();
        $input['hash_key'] = $this->generateHashKey();

        \DB::beginTransaction();
        try {
            $user = $this->userRepository->create([
                'first_name' => $firstname,
                'last_name' => $lastname,
                'email' => $input['email'],
                'password' => $input['password'],
                'active' => $input['active'],
                'confirmed' => $input['confirmed'],
                'roles' => [0 => 'user']

            ]);
            $input['user_id'] = $user->id;

            $customer = Customer::create($input);

            $input['customer_id'] = $customer->id;

            CustomerLocation::create($input);
        } catch (\Exception $e) {
            DB::rollback();
            throw new GeneralException(trans('customer::exceptions.backend.customer.create_error'));
        }

        \DB::commit();
        return true;
    }

    public static function generateReferenceId(): string
    {
        $caracters = 'ABCDEFGHIJKLMOPQRSTUVXWYZ';
        $quantidadeCaracters = strlen($caracters);
        $quantidadeCaracters--;
        $hash = null;
        for ($x = 1; $x <= 6; $x++) {
            $Posicao = rand(0, $quantidadeCaracters);
            $hash .= substr($caracters, $Posicao, 1);
        }
        $data = 'BNF-'.$hash;
        $customer = Customer::where('ref_id', $data)->count();
        if (preg_match('/^(.)\1*$/', $data) == 1 || $customer != 0) {
            $data = Customer::generateReferenceId();
        }
        return $data;
    }

    public static function generateHashKey(): string
    {
        $caracters = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
        $quantidadeCaracters = strlen($caracters);
        $quantidadeCaracters--;
        $hash = null;
        for ($x = 1; $x <= 10; $x++) {
            $Posicao = rand(0, $quantidadeCaracters);
            $hash .= substr($caracters, $Posicao, 1);
        }
        $customer = Customer::where('hash_key', $hash)->count();
        if (preg_match('/^(.)\1*$/', $hash) == 1 || $customer != 0) {
            $hash = Customer::generateHashKey();
        }
        return $hash;
    }

    public function update(Customer $customer, array $input)
    {
        $user = $customer->user;

        $supplierName = explode(' ', $input['name']);
        $firstname = $supplierName[0];
        unset($supplierName[0]);
        $lastname  = implode(' ', $supplierName);

        \DB::beginTransaction();
        try {
            $this->userRepository->update($user, [
                'first_name' => $firstname,
                'last_name' => $lastname,
                'email' => $input['email']
            ]);
            $this->updateById($customer->id, $input);

        } catch (\Exception $e) {
            DB::rollback();
            throw new GeneralException(trans('customer::exceptions.backend.customer.update_error'));
        }

        \DB::commit();
    }

    public function destroy(int $id): bool
    {
        $customer = $this->model->find($id);
        \DB::beginTransaction();
        try {
            $customer->customerLocation()->delete();
            $this->deleteById($id);
        } catch (\Exception $e) {
            DB::rollback();
            throw new GeneralException(trans('customer::exceptions.backend.customer.delete_error'));
        }

        \DB::commit();
        return true;
    }

    public function getLocationForDataTable(array $input)
    {
        return CustomerLocation::where('customer_id', $input['customerId'])->select('*');
    }

    public function createLocation(array $input)
    {
        $location  = new CustomerLocation;
        $location->customer_id = $input['customer_id'];
        $location->country_id = $input['country_id'];
        $location->state_id = $input['state_id'];
        $location->city_id = $input['city_id'];
        $location->township_id = $input['township_id'];
        $location->latitude = isset($input['latitude']) ? $input['latitude'] : null;
        $location->longitude = isset($input['longitude']) ? $input['longitude'] : null;
        $location->address_detail = isset($input['address_detail']) ? $input['address_detail'] : null;
        $location->save();

        return $location;
    }

    public function updateLocation(array $input)
    {
        $location  = CustomerLocation::find($input['location_id']);
        $location->country_id = $input['country_id'];
        $location->city_id = $input['city_id'];
        $location->state_id = $input['state_id'];
        $location->township_id = $input['township_id'];
        $location->latitude = isset($input['latitude']) ? $input['latitude'] : null;
        $location->longitude = isset($input['longitude']) ? $input['longitude'] : null;
        $location->address_detail = isset($input['address_detail']) ? $input['address_detail'] : null;
        $location->save();

        return $location;
    }
}
