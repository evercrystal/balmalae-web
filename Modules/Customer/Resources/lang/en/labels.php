<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'customer' => [
                    'create'     => 'Create Customer',
                    'create_location'=> 'Create New Location',
                    'edit'       => 'Edit Customer',
                    'management' => 'Customer Management',
                    'location_management' => 'Location Management',
                    'list'       => 'Customer List',
                    'location_list'=> 'Location List',
                    'show'       => 'Customer Detail',

                    'table' => [
                        'number_of_users' => 'Number of Customers',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'description'      => 'Description',
                        'email'            => 'Email',
                        'mobile'           => 'Mobile No',
                        'paid_amount'      => 'Paid Amount',
                        'unpaid_amount'    => 'Unpaid Amount',
                        'customer_type'    => 'Customer Type',
                        'hash_key'         => 'Hash Key',
                        'ref_id'           => 'Ref ID',
                        'choose_location'  => 'Choose Location',
                        'location'         => 'Location',
                        'country'          => 'Country',
                        'state'             => 'State',
                        'city'             => 'City',
                        'township'         => 'Township',
                        'address_detail'   => 'Address Detail',
                        'latitude'         => 'Latitude',
                        'longitude'        => 'Longitude',
                        'type'             => 'Customer Type',
                        'used_amount'      => 'Used Amount',
                        'paid_amount'      => 'Paid Amount',
                        'unpaid_amount'    => 'Unpaid Amount',
                        'status'           => 'Status',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'customer total|customer total',
                    ]
                ]
            ]

];