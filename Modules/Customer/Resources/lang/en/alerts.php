<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'customer' => [
            'created' => 'The customer was successfully created.',
            'deleted' => 'The customer was successfully deleted.',
            'updated' => 'The customer was successfully updated.',
            'location_created' => 'The customer location was successfully created.',
            'location_deleted' => 'The customer location was successfully deleted.',
            'location_updated' => 'The customer location was successfully updated.'
        ]
    ]
];