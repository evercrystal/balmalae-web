@extends ('backend.layouts.app')

@section ('title', __('customer::labels.backend.customer.location_management') . ' | ' . __('customer::labels.backend.customer.edit'))

@section('breadcrumb-links')
    @include('customer::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
{{ style('assets/css/google-map.css') }}
<link href="{{ asset('assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" type="text/css"/>

@endpush

@section('content')
{{ html()->form('POST', route('admin.customer.store_location',$customer->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{$customer->name}}'s {{ __('customer::labels.backend.customer.location_management') }}
                        <small class="text-muted">{{ __('customer::labels.backend.customer.create_location') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">

                <div class="form-group row">
                    {{ html()->label(__('customer::labels.backend.customer.table.country').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('country') }}

                        <div class="col-md-10">
                            <select name="country_id" id="country_id" class="form-control select2">
                                <option selected>Choose Country</option>
                                @foreach($countries as $country)
                                    <option value="{{$country->id}}">{{ $country->name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                    {{ html()->label(__('customer::labels.backend.customer.table.state').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('city') }}

                        <div class="col-md-10">

                            <select name="state_id" id="state_id" class="form-control select2" disabled>
                                <option selected>Choose State</option>

                            </select>
                            <span style="color:red;display:none;" id="state_text">State Not found in this country</span>
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                    {{ html()->label(__('customer::labels.backend.customer.table.city').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('city') }}

                        <div class="col-md-10">
                            <select name="city_id" id="city_id" class="form-control select2" disabled>
                                <option value="">Choose City</option>
                              
                            </select>
                            <span style="color:red;display:none;" id="city_text">City Not found in this State</span>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('customer::labels.backend.customer.table.township').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('township') }}

                        <div class="col-md-10">
                            <select name="township_id" id="township_id" class="form-control select2" disabled>
                                <option value="">Chooser Township</option>
                              
                            </select>
                            <span style="color:red;display:none;" id="township_text">Township Not found in this City</span>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('customer::labels.backend.customer.table.choose_location'))->class('col-md-2 form-control-label')->for('choose_location') }}

                        <div class="col-md-10">
                            <input id="pac-input" class="controls" type="text" placeholder="Search Place">
                            <div id="map-canvas"
                                 style="width:97%;height:400px;"></div>
                            <div id="ajax_msg"></div>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('customer::labels.backend.customer.table.location'))->class('col-md-2 form-control-label')->for('location') }}

                        <div class="col-md-10" style="display: flex;">
                            <div class="col-md-6">
                                {{ html()->text('latitude')
                                ->class('form-control')
                                ->id('input-latitude')
                                ->placeholder(__('customer::labels.backend.customer.table.latitude')) }}
                            </div>

                            <div class="col-md-6">
                                {{ html()->text('longitude')
                                ->class('form-control')
                                ->id('input-longitude')
                                ->placeholder(__('customer::labels.backend.customer.table.longitude')) }}
                            </div>
                            
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('customer::labels.backend.customer.table.address_detail'))->class('col-md-2 form-control-label')->for('address_detail') }}

                        <div class="col-md-10">
                            {{ html()->text('address_detail')
                                ->class('form-control')
                                ->placeholder(__('customer::labels.backend.customer.table.address_detail'))
                                ->attribute('maxlength', 191) }}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.customer.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')
{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('appsetting.basic.map_key') }}&libraries=weather,geometry,visualization,places,drawing&callback=initMap"
            async defer></script>
<script src="{{ asset('assets/plugins/waitme/waitMe.js') }}" type="text/javascript"></script>

<script>
     function run_waitMe() {
        $('body').waitMe({
            effect : 'bounce',
            text: 'Please wait',
            bg: 'rgba(255,255,255,0.7)',
            color: '#000',
            sizeW: '',
            sizeH: ''
        });
    }
    function stop_waitMe() {
        $('body').waitMe('hide');
    }
$(document).ready(function(){
    $('#country_id').on('change', function(){
        run_waitMe();
        var countryId = $(this).val();
        if(countryId){
            var state_id= $('#state_id').val();
            $('#state_id').empty();
        
            $('#state_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{ url('admin/customer/get-state/') }}/"+countryId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {
                        $('#state_id').attr('disabled','disabled');
                        $('#state_id').find('option').remove().end();
                        $('#state_id').hide();
                        $('#state_text').show();
                        $('#city_text').show();
                        $('#township_text').show();
                        stop_waitMe();

                    }else {
                        $('#state_id').find('option').remove().end();
                        $('#state_id').show();
                        $('#state_text').hide();
                        $('#city_text').hide();
                        $('#township_text').hide();
                        $('#state_id').append($('<option></option>') .attr('selected',true).text('Seletct state'));

                        $.each(data, function(i, value) {
                            $('#state_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#state_id').trigger('change');  
                        });    
                            $('#state_id').trigger('change');
                            stop_waitMe();
                    }  
                }, 
            });
        }else{
        $('#state_id').attr('disabled','disabled');  
        } 
    });
    $('#state_id').on('select2:select', function(){
        run_waitMe();
        var stateId = $(this).val();
        if(stateId){
            var city_id= $('#city_id').val();
            $('#city_id').empty();
        
            $('#city_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{ url('admin/customer/get-city/') }}/"+stateId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {
                        $('#city_id').attr('disabled','disabled');
                        $('#township_id').attr('disabled','disabled');
                        $('#city_id').remove().end();
                        $('#city_id').hide();
                        $('#city_text').show();
                        $('#township_text').show();
                        stop_waitMe();
                    }else {
                        $('#city_id').find('option').remove().end();
                        $('#city_id').removeAttr('disabled','disabled');
                        $('#city_id').show();
                        $('#city_text').hide();
                        $('#township_text').hide();
                        $('#city_id').append($('<option></option>') .attr('selected',true).text('Seletct City'));
                        $.each(data, function(i, value) {                            
                            $('#city_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#city_id').trigger('change');  
                            stop_waitMe();
                        });    
                            $('#city_id').trigger('change');
                    }  
                }, 
            });
        }else{
        $('#city_id').attr('disabled','disabled');  
        } 
    });
    $('#city_id').on('select2:select', function(){
        run_waitMe();
        var cityId = $(this).val();
        if(cityId){
            var township_id= $('#township_id').val();
            $('#township_id').empty();
        
            $('#township_id').removeAttr('disabled','disabled');
            $.ajax({
                url: "{{ url('admin/customer/get-township/') }}/"+cityId,
                type: 'GET',
                success: function (data){
                    if(data.length == 0) {

                        $('#township_id').attr('disabled','disabled');
                        $('#township_id').find('option').remove().end();
                        $('#township_id').hide();
                        $('#township_text').show();
                        stop_waitMe();

                    }else {
                        $('#township_id').show();
                        $('#township_id').removeAttr('disabled','disabled');
                        $('#township_text').hide();
                        $('#township_id').find('option').remove().end();
                        $('#township_id').append($('<option></option>') .attr('selected',true).text('Seletct Township'));

                        $.each(data, function(i, value) {

                            $('#township_id').append($('<option></option>').attr('value', value.id).text(value.name ));
                            $('#township_id').trigger('change');  
                            stop_waitMe();
                        });    
                        $('#township_id').removeAttr('disabled','disabled');

                            $('#township_id').trigger('change');
                            stop_waitMe();
                    }  
                }, 
            });
        }else{
        $('#township_id').attr('disabled','disabled');  
        } 
    });
});

    function initMap() {
    
        var latitude = 16.798703652839684;
        var longitude = 96.14947007373053;
        var mapOptions = {
            center: new google.maps.LatLng(latitude, longitude),
            zoom: 13
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);

        var marker_position = new google.maps.LatLng(latitude, longitude);
        var input = /** @type {HTMLInputElement} */(
                document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
            position: marker_position,
            draggable: true,
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });


        google.maps.event.addListener(marker, "mouseup", function (event) {
            $('#input-latitude').val(this.position.lat());
            $('#input-longitude').val(this.position.lng());
        });

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }

            marker.setIcon(/** @type {google.maps.Icon} */({
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(35, 35)
            }));

            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            $('#input-latitude').val(place.geometry.location.lat());
            $('#input-longitude').val(place.geometry.location.lng());

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            $('input[name=address]').val(place.formatted_address);

            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);
        });


        google.maps.event.addListener(marker, 'dragend', function () {

            $('#input-latitude').val(place.geometry.location.lat());
            $('#input-longitude').val(place.geometry.location.lng());

        });

    }

    if ($('#map-canvas').length != 0) {
        google.maps.event.addDomListener(window, 'load', initMap);
    }

</script>
@endpush