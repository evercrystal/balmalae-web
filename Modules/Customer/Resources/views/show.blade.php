@extends ('backend.layouts.app')

@section ('title', __('customer::labels.backend.customer.management'))

@section('breadcrumb-links')
    @include('customer::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('customer::labels.backend.customer.management') }}
                    <small class="text-muted">{{ __('customer::labels.backend.customer.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('customer::labels.backend.customer.table.ref_id') }}</th>
                                <td>{{ $customer->ref_id }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('customer::labels.backend.customer.table.name') }}</th>
                                <td>{{ $customer->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('labels.backend.access.users.table.email') }}</th>
                                <td>{{ $customer->user->email }}</td>                                
                            </tr>
                            <tr>
                                <th>{{ __('customer::labels.backend.customer.table.mobile') }}</th>
                                <td>{{ $customer->mobile }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('customer::labels.backend.customer.table.type') }}</th>
                                <td>{!! $customerType !!}</td>
                            </tr>
                            <tr>
                                <th>{{ __('customer::labels.backend.customer.table.status') }}</th>
                                <td>{!! $customer->status_label !!}</td>
                            </tr>
                            <tr>
                                <th>{{ __('customer::labels.backend.customer.table.hash_key') }}</th>
                                <td>{{ $customer->hash_key }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>

                <!-- Start Booking Table -->
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-5">
                                    <h4 class="card-title mb-0">
                                        {{ __('customer::labels.backend.customer.location_management') }} <small class="text-muted">{{ __('customer::labels.backend.customer.location_list') }}</small>
                                    </h4>
                                </div>
                                <div class="col-sm-7">
                                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                                        <a href="{{ route('admin.customer.create_location',$customer->id) }}" class="btn btn-success ml-1" data-toggle="tooltip" title="{{ __('customer::menus.backend.customer.create') }}"><i class="fas fa-plus-circle"></i></a>
                                    </div>
                                </div><!--col-->
                            </div>

                            <div class="row mt-4">
                                <div class="col">
                                    <div class="table-responsive">
                                        <table id="location-table" class="table table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th>{{ __('customer::labels.backend.customer.table.country') }}</th>
                                                <th>{{ __('customer::labels.backend.customer.table.state') }}</th>
                                                <th>{{ __('customer::labels.backend.customer.table.city') }}</th>
                                                <th>{{ __('customer::labels.backend.customer.table.township') }}</th>
                                                <th>{{ __('customer::labels.backend.customer.table.latitude') }}</th>
                                                <th>{{ __('customer::labels.backend.customer.table.longitude') }}</th>
                                                <th>{{ __('customer::labels.backend.customer.table.address_detail') }}</th>
                                                <th>{{ __('labels.general.actions') }}</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Booking Table -->

            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('customer::labels.backend.customer.table.created') }}:</strong> {{ $customer->updated_at->timezone(get_user_timezone()) }} ({{ $customer->created_at->diffForHumans() }}),
                    <strong>{{ __('customer::labels.backend.customer.table.last_updated') }}:</strong> {{ $customer->created_at->timezone(get_user_timezone()) }} ({{ $customer->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')
{{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
{{ script("js/plugin/datatables/dataTables-extend.js") }}
<script>
$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#location-table').DataTable({
        serverSide: true,
        ajax: {
            url: '{!! route("admin.customer.get_location") !!}',
            type: 'post',
            data: { 'customerId': '{{$customer->id}}'},
            error: function (xhr, err) {
                if (err === 'parsererror')
                    location.reload();
                else swal(xhr.responseJSON.message);
            }
        },
        columns: [
            {data: 'country_id', name: 'country_id'},
            {data: 'state_id', name: 'state_id'},
            {data: 'city_id', name: 'city_id'},
            {data: 'township_id', name: 'township_id'},
            {data: 'latitude', name: 'latitude'},
            {data: 'longitude', name: 'longitude'},
            {data: 'address_detail', name: 'address_detail'},
            {data: 'actions', name: 'actions', searchable: false, sortable: false}
        ],
        order: [[0, "asc"]],
        searchDelay: 500,
        fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            load_plugins();
        }
    });
});

</script>
@endpush