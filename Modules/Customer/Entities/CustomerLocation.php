<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\Customer\Entities\Customer;
use Modules\Country\Entities\Country;
use Modules\State\Entities\State;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use App\Enums\Table;

class CustomerLocation extends Model
{
    use SoftDeletes;
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = Table::CUSTOMER_LOCATION;

    protected $fillable = ["id","customer_id","state_id","country_id","city_id","township_id","latitude","longitude","address_detail"];

    /**
     * @return mixed
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    /**
     * @return mixed
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
    /**
     * @return mixed
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    /**
     * @return mixed
     */
    public function township()
    {
        return $this->belongsTo(Township::class, 'township_id');
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (auth()->user()->can('edit customer')) {
            return '<a href="'.route('admin.customer.edit_location', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
        return '';
    }

    /**
    * @return string
    */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete customer')) {
            return '<a href="'.route('admin.customer.delete_location', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getEditButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
