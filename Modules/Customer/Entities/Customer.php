<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\Customer\Entities\CustomerLocation;
use App\Models\Auth\User;
use App\Enums\Table;

class Customer extends Model
{
    use SoftDeletes;
    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = Table::CUSTOMER;

    protected $fillable = ["id","user_id","ref_id","name","mobile","used_amount","paid_amount","unpaid_amount","type","hash_key"];

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @return mixed
     */
    public function customerLocation()
    {
        return $this->hasMany(CustomerLocation::class, 'customer_id');
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        if (auth()->user()->can('view customer')) {
            return '<a href="'.route('admin.customer.show', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
        return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (auth()->user()->can('edit customer')) {
            return '<a href="'.route('admin.customer.edit', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
        return '';
    }

    /**
    * @return string
    */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete customer')) {
            return '<a href="'.route('admin.customer.destroy', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    public function getStatusLabelAttribute()
    {
        $user = User::find($this->user_id);
        if ($user->active) {
            return '<span class="badge badge-success">active</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getShowButtonAttribute().$this->getEditButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
