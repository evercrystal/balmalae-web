<?php
namespace Modules\Customer\Enum;

class CustomerType
{
    const ID_GENERAL = 1;
    const ID_ONLINE_SHOP = 2;

    const NAME_GENERAL = "General";
    const NAME_ONLINE_SHOP = "Online Shop";

    const AVAILABLES = [
        self::ID_GENERAL => self::NAME_GENERAL,
        self::ID_ONLINE_SHOP => self::NAME_ONLINE_SHOP
    ];
}
