<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'cms' => [
                    'create'     => 'Create CMS',
                    'edit'       => 'Edit CMS',
                    'management' => 'CMS Management',
                    'list'       => 'CMS List',
                    'show'       => 'CMS Detail',

                    'table' => [
                        'number_of_users' => 'Number of CMSs',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'description'      => 'Description',
                        'meta_tag'         => 'Meta Tag',
                        'meta_keyword'     => 'Meta Keyword',
                        'title'            => 'Title',
                        'page'             => 'Page',
                        'cms_text'         => 'Text',
                        'mm_title'         => 'MM Title',
                        'mm_cms_text'      => 'MM Text',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'cms total|cms total',
                    ]
                ]
            ]

];