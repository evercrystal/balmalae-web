<?php

namespace Modules\CMS\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCMSRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'meta_tags' => 'required',
            'meta_keywords' => 'required',
            'page' => 'required',
            'title' => 'required',
            'content' => 'required',
            'mm_title' => 'required',
            'mm_content' => 'required',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit cms');
    }
}
