<?php

namespace Modules\CMS\Repositories;

use Modules\CMS\Entities\CMS;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CMSRepository.
 */
class CMSRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function __construct(CMS $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }

    public function getFaqPage()
    {
        $page = CMS::where('page','faq')->first();
        // dd($page);
        // $page = changeLanguage($page,['title','content']);
        $content = $page ? $page->content : null;

        if (!is_null($page)) {
            $mystring = trim(strip_tags($content));

            $all_category = trim(strip_tags($content));

            $categories = explode('{/category}{category}',$all_category);
            foreach ($categories as $key=>$cate_info) {
                $cate_info = str_replace('{category}','',$cate_info);
                $cate_info = str_replace('{/category}','',$cate_info);
                $cate_info = explode('{/title}{question}',$cate_info);
                $all_result[$key]['title'] = str_replace('{title}','',$cate_info[0]);
                $strings = explode('{/answer}{question}',$cate_info[1]);
                foreach ($strings as $k => $s) {
                    $changed_string = str_replace('{question}','',$s);
                    $changed_string = str_replace('{newline}','<br>',$changed_string);
                    $changed_string = str_replace('{bold}','<b>',$changed_string);
                    $changed_string = str_replace('{/bold}','</b>',$changed_string);
                    $changed_string = str_replace('{/answer}','',$changed_string);
                    $all_result[$key]['result'][] = explode('{/question}{answer}',$changed_string);
                }
            }
            $page->my_content = $all_result;
        }
        return $page;
    }

    public function getCmsPage($name)
    {
        $name = \Cache::remember($name, 60, function () use ($name) {
                return CMS::where('page',$name)->first();
        });
         return $name;
    }
}
