<?php

namespace Modules\City\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\State\Entities\State;
use Modules\Township\Entities\Township;
use App\Enums\Table;

class City extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = Table::CITY;
    protected $fillable = ["id", "name", "state_id"];
    public function township()
    {
        return $this->hasMany(Township::class);
    }
    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        if (auth()->user()->can('view city')) {
            return '<a href="' . route('admin.city.show', $this) . '" 
            data-toggle="tooltip" data-placement="top" title="' . __('buttons.general.crud.view') . '" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
        return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (auth()->user()->can('edit city')) {
            return '<a href="' . route('admin.city.edit', $this) . '" data-toggle="tooltip" data-placement="top" title="' . __('buttons.general.crud.edit') . '" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
        return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete city')) {
            return '<a href="' . route('admin.city.destroy', $this) . '" data-method="delete"
                 data-trans-button-cancel="' . __('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . __('buttons.general.crud.delete') . '"
                 data-trans-title="' . __('strings.backend.general.are_you_sure') . '" data-toggle="tooltip" data-placement="top" title="' . __('buttons.general.crud.delete') . '" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return $this->getShowButtonAttribute() . $this->getEditButtonAttribute() . $this->getDeleteButtonAttribute();
    }
}
