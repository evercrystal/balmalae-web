<?php

namespace Modules\City\Repositories;

use Modules\City\Entities\City;
use Modules\State\Entities\State;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CityRepository.
 */
class CityRepository extends BaseRepository
{
    /**
     * @return string
     */
    private $state;
    public function __construct(City $model , State $state)
    {
        $this->model = $model;
        $this->state = $state;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model->with([
            'State' => function ($query) {
                return $query->select('id', 'name');
            }
        ])->select('*')->orderBy('created_at','desc');
    }
}
