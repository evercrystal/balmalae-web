<?php

namespace Modules\City\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\City\Entities\City;
use Modules\City\Http\Requests\ManageCityRequest;
use Modules\City\Http\Requests\CreateCityRequest;
use Modules\City\Http\Requests\UpdateCityRequest;
use Modules\City\Http\Requests\ShowCityRequest;
use Modules\City\Repositories\CityRepository;
use Modules\State\Repositories\StateRepository;
use Modules\Country\Entities\Country;
use Modules\Township\Repositories\TownshipRepository;

class CityController extends Controller
{
 /**
     * @var CityRepository
     * @var CategoryRepository
     */
    protected $city;
    protected $state;
    protected $township;
    /**
     * @param CityRepository $city
     */
    public function __construct(CityRepository $city, StateRepository $state, TownshipRepository $township)
    {
        $this->city = $city;
        $this->state = $state;
        $this->township = $township;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('city::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {   
        $states = $this->state->getAll();
        return view('city::create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateCityRequest $request)
    {
        $this->city->create($request->except('_token','_method'));
        return redirect()->route('admin.city.index')->withFlashSuccess(trans('city::alerts.backend.city.created'));
    }

    /**
     * @param City              $city
     * @param ManageCityRequest $request
     *
     * @return mixed
     */
    public function edit(City $city, ManageCityRequest $request)
    {
        $states = $this->state->getAll();
        return view('city::edit',compact('states'))
            ->withCity($city);
    }

    /**
     * @param City              $city
     * @param UpdateCityRequest $request
     *
     * @return mixed
     */
    public function update(City $city, UpdateCityRequest $request)
    {
        $this->city->updateById($city->id,$request->except('_token','_method'));

        return redirect()->route('admin.city.index')->withFlashSuccess(trans('city::alerts.backend.city.updated'));
    }

    /**
     * @param City              $city
     * @param ManageCityRequest $request
     *
     * @return mixed
     */
    public function show(City $city, ShowCityRequest $request)
    {
        return view('city::show')->withCity($city);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(City $city)
    {
        if(optional($city->township)->count()) 
        {
            return redirect()->route('admin.city.index')->withFlashDanger(trans('city::alerts.backend.city.not_deleted'));
        }
        $this->city->deleteById($city->id);

        return redirect()->route('admin.city.index')->withFlashSuccess(trans('city::alerts.backend.city.deleted'));
    }
}
