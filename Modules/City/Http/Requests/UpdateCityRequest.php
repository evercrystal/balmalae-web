<?php

namespace Modules\City\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\Table;

class UpdateCityRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->segment(3);
        return [
            'name' => 'required|max:191|unique:'.Table::CITY.',name,'.$id,
            'state_id' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit city');
    }
}
