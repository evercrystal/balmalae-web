@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('city::labels.backend.city.management'))

@section('breadcrumb-links')
    @include('city::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('city::labels.backend.city.management') }} <small class="text-muted">{{ __('city::labels.backend.city.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('city::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="city-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('city::labels.backend.city.table.id') }}</th>
                            <th>{{ __('city::labels.backend.city.table.name') }}</th>
                            <th>{{ __('city::labels.backend.city.table.state') }}</th>
                            <th>{{ __('city::labels.backend.city.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#city-table').DataTable({
                processing: true,
                language:{processing: "<div class='overlay custom-loader-background' style='font-size:2em;'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"},
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.city.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'state_id', name: 'state_id'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush