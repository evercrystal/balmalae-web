@extends ('backend.layouts.app')

@section ('title', __('city::labels.backend.city.management') . ' | ' . __('city::labels.backend.city.edit'))

@section('breadcrumb-links')
    @include('city::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}


@endpush

@section('content')
{{ html()->modelForm($city, 'PATCH', route('admin.city.update', $city->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('city::labels.backend.city.management') }}
                        <small class="text-muted">{{ __('city::labels.backend.city.edit') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">
                <div class="form-group row">
                    {{ html()->label(__('city::labels.backend.city.table.state'))->class('col-md-2 form-control-label')->for('state') }}

                        <div class="col-md-10">
                        <select class="form-control select2"  name="state_id">
                                @foreach($states as $state)
                                    <option value="{{ $state->id }}" @if($state->id == $city->state_id) selected @endif>{{ $state->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                    {{ html()->label(__('city::labels.backend.city.table.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('city::labels.backend.city.table.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                   
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.city.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')


{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script>
$(function(){

    $(".select").select2({
        placeholder: 'Choose city',
        width: '100%'
    });
});

</script>
@endpush