<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'city' => [
            'created' => 'The city was successfully created.',
            'deleted' => 'The city was successfully deleted.',
            'not_deleted' => 'This city was used in other table. Cannot delete yet.',
            'updated' => 'The city was successfully updated.'
        ]
    ]
];