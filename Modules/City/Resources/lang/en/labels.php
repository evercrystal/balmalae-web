<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'city' => [
                    'create'     => 'Create City',
                    'edit'       => 'Edit City',
                    'management' => 'City Management',
                    'list'       => 'City List',
                    'show'       => 'City Detail',

                    'table' => [
                        'number_of_users' => 'Number of Citys',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'state'             => 'State Name',
                        'country_code'     => 'Country Code',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'city total|city total',
                    ]
                ]
            ]

];