<?php

namespace Modules\Township\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Township\Repositories\TownshipRepository;
use Modules\Township\Http\Requests\ManageTownshipRequest;

class TownshipTableController extends Controller
{
    /**
     * @var TownshipRepository
     */
    protected $township;

    /**
     * @param TownshipRepository $township
     */
    public function __construct(TownshipRepository $township)
    {
        $this->township = $township;
    }

    /**
     * @param ManageTownshipRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageTownshipRequest $request)
    {
        return DataTables::of($this->township->getForDataTable())
            ->editColumn('city_id', function ($township) {
                return $township->city->name;
            })
            ->addColumn('actions', function ($township) {
                return $township->action_buttons;
            })
            ->rawColumns(['actions','city_id'])
            ->make(true);
    }
}
