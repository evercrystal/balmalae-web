<?php

namespace Modules\Township\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Township\Entities\Township;
use Modules\Township\Http\Requests\ManageTownshipRequest;
use Modules\Township\Http\Requests\CreateTownshipRequest;
use Modules\Township\Http\Requests\UpdateTownshipRequest;
use Modules\Township\Http\Requests\ShowTownshipRequest;
use Modules\Township\Repositories\TownshipRepository;
use Modules\City\Repositories\CityRepository;

class TownshipController extends Controller
{
 /**
     * @var TownshipRepository
     * @var CategoryRepository
     */
    protected $township;

    /**
     * @param TownshipRepository $township
     */
    private $city;
    public function __construct(TownshipRepository $township, CityRepository $city)
    {
        $this->township = $township;
        $this->city = $city;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('township::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $cities = $this->city->getAll();
        return view('township::create',compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateTownshipRequest $request)
    {
        $this->township->create($request->except('_token','_method'));
        return redirect()->route('admin.township.index')->withFlashSuccess(trans('township::alerts.backend.township.created'));
    }

    /**
     * @param Township              $township
     * @param ManageTownshipRequest $request
     *
     * @return mixed
     */
    public function edit(Township $township, ManageTownshipRequest $request)
    {
        $cities = $this->city->getAll();
        return view('township::edit', compact('cities'))
            ->withTownship($township);
    }

    /**
     * @param Township              $township
     * @param UpdateTownshipRequest $request
     *
     * @return mixed
     */
    public function update(Township $township, UpdateTownshipRequest $request)
    {
        $this->township->updateById($township->id,$request->except('_token','_method'));

        return redirect()->route('admin.township.index')->withFlashSuccess(trans('township::alerts.backend.township.updated'));
    }

    /**
     * @param Township              $township
     * @param ManageTownshipRequest $request
     *
     * @return mixed
     */
    public function show(Township $township, ShowTownshipRequest $request)
    {
        return view('township::show')->withTownship($township);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Township $township)
    {
        $this->township->deleteById($township->id);

        return redirect()->route('admin.township.index')->withFlashSuccess(trans('township::alerts.backend.township.deleted'));
    }
}
