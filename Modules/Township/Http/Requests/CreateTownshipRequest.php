<?php

namespace Modules\Township\Http\Requests;
use App\Enums\Table;
use Illuminate\Foundation\Http\FormRequest;

class CreateTownshipRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191|unique:'.Table::TOWNSHIP,
            'city_id' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create township');
    }
}
