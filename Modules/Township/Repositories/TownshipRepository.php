<?php

namespace Modules\Township\Repositories;

use Modules\Township\Entities\Township;
use Modules\City\Entities\City;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TownshipRepository.
 */
class TownshipRepository extends BaseRepository
{
    /**
     * @return string
     */
    private $city;
    public function __construct(Township $model, City $city)
    {
        $this->model = $model;
        $this->city = $city;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model->with([
            'City' => function ($query) {
                return $query->select('id', 'name');
            }
        ])->select('*')->orderBy('created_at','desc');
    }
}
