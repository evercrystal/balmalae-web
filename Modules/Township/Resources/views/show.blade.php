@extends ('backend.layouts.app')

@section ('title', __('township::labels.backend.township.management'))

@section('breadcrumb-links')
    @include('township::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('township::labels.backend.township.management') }}
                    <small class="text-muted">{{ __('township::labels.backend.township.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('township::labels.backend.township.table.name') }}</th>
                                <td>{{ $township->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('township::labels.backend.township.table.city_name') }}</th>
                                <td>{{ $township->city->name }}</td>
                            </tr>

                        </tbody>
                    </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('township::labels.backend.township.table.created') }}:</strong> {{ $township->updated_at->timezone(get_user_timezone()) }} ({{ $township->created_at->diffForHumans() }}),
                    <strong>{{ __('township::labels.backend.township.table.last_updated') }}:</strong> {{ $township->created_at->timezone(get_user_timezone()) }} ({{ $township->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush