<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('host', function (Blueprint $table) {
            $table->increments('host_id');
            $table->string('host_name',100);
            $table->string('host_mmname');
            $table->integer('host_category_id')->unsigned();
            $table->foreign('host_category_id')->references('host_category_id')->on('host_category')->onDelete('cascade');
            $table->longText('description');
            $table->longText('mm_description');
            $table->text('host_qrcode')->nullable();
            $table->string('host_mobileno');
            $table->string('host_logo');
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('address_detail');
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('state')->onDelete('cascade');
            $table->integer('city_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('city')->onDelete('cascade');
            $table->integer('township_id')->unsigned();
            $table->foreign('township_id')->references('id')->on('township')->onDelete('cascade');
            $table->tinyInteger('sick_status')->default(1);
            $table->tinyInteger('trip_status')->default(1);
            $table->tinyInteger('is_active')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('host');
    }
}
