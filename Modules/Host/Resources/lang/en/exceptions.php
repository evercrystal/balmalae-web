<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'host' => [
                'create_error'      => 'There was a problem creating this host. Please try again.',
                'delete_error'      => 'There was a problem deleting this host. Please try again.',
                'not_found'         => 'That host does not exist.',
                'update_error'      => 'There was a problem updating this host. Please try again.',
            ]
        ]
];
