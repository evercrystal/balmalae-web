<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'host' => [
                    'create'     => 'Create Host',
                    'edit'       => 'Edit Host',
                    'management' => 'Host Management',
                    'list'       => 'Host List',
                    'show'       => 'Host Detail',

                    'table' => [
                        'number_of_users' => 'Number of Hosts',
                        'sort'                  => 'Sort',
                        'id'                    => 'ID',
                        'name'                  => 'Name',
                        'slug'                  => 'Slug',
                        'host_name'             => 'Host Name',
                        'user_name'             => 'User Name',
                        'mm_name'               => 'MM Name',
                        'host_category'         => 'Host Category',
                        'mobile'                => 'Mobile Number',
                        'status'                => 'Status',
                        'township'              => 'Township',
                        'latitude'              => 'Latitude',
                        'longitude'             => 'Longitude',
                        'address'               => 'Address',
                        'logo'                  => 'Logo',
                        'state'                 => 'State',
                        'sick_status'           => 'Sick Status',
                        'trip_status'           => 'Trip Status',
                        'qr'                    => 'QR Code',
                        'mm_description'        => 'MM Description',
                        'city'                  => 'City',
                        'choose_location'       => 'Choose Location',
                        'location'              => 'Location',
                        'address_detail'        => 'Address Detail',
                        'host_category'         => 'Host Category',
                        'description'           => 'Description',
                        'created'               => 'Created',
                        'last_updated'          => 'Last Updated',
                        'total'                 => 'host total|host total',
                    ]
                ]
            ]

];