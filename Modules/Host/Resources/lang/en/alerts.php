<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'host' => [
            'created' => 'The host was successfully created.',
            'active' => 'The host was successfully activated.',
            'inactive' => 'The host was successfully Inactivated.',
            'deleted' => 'The host was successfully deleted.',
            'updated' => 'The host was successfully updated.'
        ]
    ]
];