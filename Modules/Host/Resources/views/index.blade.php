@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('host::labels.backend.host.management'))

@section('breadcrumb-links')
    @include('host::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('host::labels.backend.host.management') }} <small class="text-muted">{{ __('host::labels.backend.host.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('host::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="host-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('host::labels.backend.host.table.id') }}</th>
                            <th>{{ __('host::labels.backend.host.table.name') }}</th>
                            <th>{{ __('host::labels.backend.host.table.mm_name') }}</th>
                            <th>{{ __('host::labels.backend.host.table.mobile') }}</th>
                            <th>{{ __('host::labels.backend.host.table.host_category') }}</th>
                            <th>{{ __('host::labels.backend.host.table.township') }}</th>
                            <th>{{ __('host::labels.backend.host.table.city') }}</th>
                            <th>{{ __('host::labels.backend.host.table.description') }}</th>
                            <th>{{ __('host::labels.backend.host.table.qr') }}</th>
                            <th>{{ __('host::labels.backend.host.table.status') }}</th>
                            <th>{{ __('host::labels.backend.host.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#host-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.host.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'host_id', name: 'host_id'},
                    {data: 'host_name', name: 'host_name'},
                    {data: 'host_mmname', name: 'host_mmname'},
                    {data: 'host_mobileno', name: 'host_mobileno'},
                    {data: 'host_category_id', name: 'host_category_id'},
                    {data: 'township_id', name: 'township_id'},
                    {data: 'city_id', name: 'city_id'},
                    {data: 'description', name: 'description'},
                    {data: 'host_qrcode', name: 'host_qrcode'},
                    {data: 'status', name: 'status'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush