@extends ('backend.layouts.app')

@section ('title', __('host::labels.backend.host.management'))

@section('breadcrumb-links')
    @include('host::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('host::labels.backend.host.management') }}
                    <small class="text-muted">{{ __('host::labels.backend.host.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.name') }}</th>
                                <td>{{ $host->host_name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.mm_name') }}</th>
                                <td>{{ $host->host_mmname }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.slug') }}</th>
                                <td>{{ $host->slug }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.mobile') }}</th>
                                <td>{{ $host->host_mobileno }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.host_category') }}</th>
                                <td>{{ $host->host_category->host_category_name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.description') }}</th>
                                <td>{{ $host->description }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.mm_description') }}</th>
                                <td>{{ $host->mm_description }}</td>
                            </tr>
                            
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.latitude') }}</th>
                                <td>{{ $host->latitude }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.longitude') }}</th>
                                <td>{{ $host->longitude }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.address') }}</th>
                                <td>{{ $host->address_detail }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.state') }}</th>
                                <td>{{ $host->state->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.city') }}</th>
                                <td>{{ $host->city->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.township') }}</th>
                                <td>{{ $host->township->name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.sick_status') }}</th>
                                <td>
                                    @if($host->sick_status == 1)
                                        Yes
                                    @else
                                        No
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.trip_status') }}</th>
                                <td>
                                    @if($host->trip_status == 1)
                                        Yes
                                    @else
                                        No
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.qr') }}</th>
                                <td><img src="{{ $host->host_qrcode }}" width="100" height="100"></td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.logo') }}</th>
                                <td><img src="{{ url('uploads/'.$host->host_logo) }}" width="100" height="100"></td>
                            </tr>
                            <tr>
                                <th>{{ __('host::labels.backend.host.table.status') }}</th>
                                <td>{!! $host->status_label !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('host::labels.backend.host.table.created') }}:</strong> {{ $host->updated_at->timezone(get_user_timezone()) }} ({{ $host->created_at->diffForHumans() }}),
                    <strong>{{ __('host::labels.backend.host.table.last_updated') }}:</strong> {{ $host->created_at->timezone(get_user_timezone()) }} ({{ $host->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush