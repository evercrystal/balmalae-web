<?php

namespace Modules\Host\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\State\Entities\State;
use Modules\City\Entities\City;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Modules\Host\Entities\HostUser;
use Modules\Township\Entities\Township;
use Modules\HostCategory\Entities\HostCategory;
use App\Models\Auth\User;

class Host extends Model
{
    use SoftDeletes;
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    use Sluggable;
    use SluggableScopeHelpers;

    protected $table = 'host';
    protected $primaryKey = 'host_id';

    protected $fillable = ['host_id','host_name','host_mmname', 'slug','host_category_id','description','mm_description','host_qrcode','host_mobileno','host_logo','latitude','longitude','address_detail','state_id','city_id','township_id','sick_status','trip_status','is_active'];


    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'host_name'
            ]
        ];
    }

    public function township()
    {
        return $this->hasOne(Township::class, 'id', 'township_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function city()
    {
        return $this->hasOne(City::class, 'id', 'city_id');
    }

    public function host_category()
    {
        return $this->hasOne(HostCategory::class, 'host_category_id', 'host_category_id');
    }

    public function host_user()
    {
        return $this->hasMany(HostUser::class, 'host_id');
    }


       /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
    	if(auth()->user()->can('view host')){
        	return '<a href="'.route('admin.host.show', $this->host_id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
    	if(auth()->user()->can('edit host')){
        	return '<a href="'.route('admin.host.edit', $this->host_id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
       	return '';
    }

     /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete host')) {
            return '<a href="'.route('admin.host.destroy', $this->host_id).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    public function getStatusLabelAttribute()
    {
        if ($this->is_active == 1) {
            return '<span class="badge badge-success">active</span>';
        }

        return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
            return $this->getShowButtonAttribute().$this->getEditButtonAttribute();
    }
}
