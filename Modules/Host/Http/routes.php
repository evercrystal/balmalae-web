<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Host\Http\Controllers'], function()
{
    		/*
             * For DataTables
             */
            Route::post('host/get', 'HostTableController')->name('host.get');

            /*
             * User CRUD
             */
            Route::resource('host', 'HostController');

            Route::get('host/inactive/{host_id}', 'HostController@inActive')->name('host.inactive');
            Route::get('host/active/{host_id}', 'HostController@active')->name('host.active');


});