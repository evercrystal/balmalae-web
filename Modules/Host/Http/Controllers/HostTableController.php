<?php

namespace Modules\Host\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Host\Repositories\HostRepository;
use Modules\Host\Http\Requests\ManageHostRequest;

class HostTableController extends Controller
{
    /**
     * @var HostRepository
     */
    protected $host;

    /**
     * @param HostRepository $host
     */
    public function __construct(HostRepository $host)
    {
        $this->host = $host;
    }

    /**
     * @param ManageHostRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageHostRequest $request)
    {
        return DataTables::of($this->host->getForDataTable())
            ->addColumn('actions', function ($host) {
                return $host->action_buttons;
            })
            ->editColumn('township_id', function ($host) {
                return $host->township->name;
            })
            ->editColumn('city_id', function ($host) {
                return $host->city->name;
            })
            ->editColumn('host_category_id', function ($host) {
                return $host->host_category->host_category_name;
            })
            ->addColumn('status', function ($host) {
                if ($host->is_active == 1){
                    
                    return "<a href=".route( 'admin.host.inactive', $host->host_id)." data-toggle='tooltip' data-placement='top' title='Inactive' name='confirm_item'>
                            <span class='badge badge-success' style='cursor:pointer'>Active</span>
                    </a>";
                }else{
                    return "<a href=".route('admin.host.active', $host->host_id)." data-toggle='tooltip' data-placement='top' title='Active' name='confirm_item'>
                        <span class='badge badge-danger' style='cursor:pointer'>Inactive</span>
                    </a>";
                }
            })
            ->editColumn('host_qrcode', function ($host) {
                return '<img src="'. $host->host_qrcode .'" width="100" height="100">';
            })->rawColumns(['actions','host_qrcode','status'])
            ->make(true);
    }
}
