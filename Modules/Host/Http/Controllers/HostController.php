<?php

namespace Modules\Host\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Host\Entities\Host;
use Modules\State\Entities\State;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use Modules\Host\Http\Requests\ManageHostRequest;
use Modules\Host\Http\Requests\CreateHostRequest;
use Modules\Host\Http\Requests\UpdateHostRequest;
use Modules\Host\Http\Requests\ShowHostRequest;
use Modules\Host\Repositories\HostRepository;
use Modules\City\Repositories\CityRepository;
use Modules\Township\Repositories\TownshipRepository;
use Modules\State\Repositories\StateRepository;
use Modules\HostCategory\Entities\HostCategory;
use Modules\HostCategory\Repositories\HostCategoryRepository;
use Image;
use  Illuminate\Http\File;

class HostController extends Controller
{
 /**
     * @var HostRepository
     * @var CategoryRepository
     */
    protected $host;

    /**
     * @param HostRepository $host
     */
    public function __construct(HostRepository $host, CityRepository $city, StateRepository $state, TownshipRepository $township)
    {
        $this->host = $host;
        $this->state = $state;
        $this->city = $city;
        $this->township = $township;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('host::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('host::create');
    }

    public function active($host, ManageHostRequest $request)
    {
        $host = Host::find($host);
        $this->host->active($host);

        return redirect()->route('admin.host.index')->withFlashSuccess(trans('host::alerts.backend.host.active'));
    }

    public function inActive($host, ManageHostRequest $request)
    {
        $host = Host::find($host);
        $this->host->inActive($host);

        return redirect()->route('admin.host.index')->withFlashSuccess(trans('host::alerts.backend.host.inactive'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateHostRequest $request)
    {
        $this->host->create($request->except('_token','_method'));
        return redirect()->route('admin.host.index')->withFlashSuccess(trans('host::alerts.backend.host.created'));
    }

    /**
     * @param Host              $host
     * @param ManageHostRequest $request
     *
     * @return mixed
     */
    public function edit(Host $host, ManageHostRequest $request)
    {
        $states = $this->state->getAll('created_at', 'asc');
        $cities = $this->city->getAll('id','asc');
        $townships = $this->township->getAll('id','asc');
        $host_categories = HostCategory::select('host_category_id','host_category_name')->get();
        return view('host::edit', compact('states', 'cities', 'townships', 'host_categories'))
            ->withHost($host);
    }

    /**
     * @param Host              $host
     * @param UpdateHostRequest $request
     *
     * @return mixed
     */
    public function update(Host $host, UpdateHostRequest $request)
    {
        $request->validate([
            'host_mobileno' => 'required|valid_phone_number|unique:host,host_mobileno,'.$host->host_id.',host_id',
        ]);
        $input = $request->except('_token','_method');
        if($request->hasFile('host_logo')){            
            \Storage::disk('local')->delete($host->host_logo);
            $name=uniqid('host - ').'.'.$input['host_logo']->extension();
            $host_logo = Image::make($input['host_logo'])->save('uploads/host_logo/'.$name,80);
            $input['host_logo'] = 'host_logo/'.$host_logo->basename;
        }
        else{
            $input['host_logo'] = $host->host_logo;
        }
        $input['slug'] = str_slug($input['host_name']);
        $input['sick_status'] = isset($input['sick_status']) ? 1 : 0;
        $input['trip_status'] = isset($input['trip_status']) ? 1 : 0;
        $host_updated = $this->host->updateById($host->host_id,$input);
        $host_updated->host_qrcode = config('app.url').'/check-in/'.$host_updated->slug;;
        $host_updated->host_qrcode = 'data:image/png;base64,' . base64_encode(\QrCode::format('png')->size(200)->generate($host_updated->host_qrcode));
        $host_updated->update();

        return redirect()->route('admin.host.index')->withFlashSuccess(trans('host::alerts.backend.host.updated'));
    }

    /**
     * @param Host              $host
     * @param ManageHostRequest $request
     *
     * @return mixed
     */
    public function show(Host $host, ShowHostRequest $request)
    {
        return view('host::show')->withHost($host);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Host $host)
    {
        $this->host->deleteById($host->host_id);

        return redirect()->route('admin.host.index')->withFlashSuccess(trans('host::alerts.backend.host.deleted'));
    }
}
