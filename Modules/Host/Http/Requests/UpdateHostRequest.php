<?php

namespace Modules\Host\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateHostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'host_name' => 'required|max:100',
            'host_category_id' => 'required',
            'host_mmname' => 'required',
            'description' => 'required',
            'mm_description' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'township_id' => 'required',
            'address_detail' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'valid_phone_number' => 'Invalid Mobile No. or Not Support Mobile No.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('edit host');
    }
}
