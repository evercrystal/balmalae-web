<?php

Breadcrumbs::for('admin.host.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('host::labels.backend.host.management'), route('admin.host.index'));
});

Breadcrumbs::for('admin.host.create', function ($trail) {
    $trail->parent('admin.host.index');
    $trail->push(__('host::labels.backend.host.create'), route('admin.host.create'));
});

Breadcrumbs::for('admin.host.show', function ($trail, $id) {
    $trail->parent('admin.host.index');
    $trail->push(__('host::labels.backend.host.show'), route('admin.host.show', $id));
});

Breadcrumbs::for('admin.host.edit', function ($trail, $id) {
    $trail->parent('admin.host.index');
    $trail->push(__('host::labels.backend.host.edit'), route('admin.host.edit', $id));
});
