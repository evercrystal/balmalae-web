<?php

namespace Modules\Host\Repositories;

use Modules\Host\Entities\Host;
use Modules\Host\Entities\HostUser;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;
use Image;
use  Illuminate\Http\File;
use GMBF\PhoneNumber;


/**
 * Class HostRepository.
 */
class HostRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function __construct(Host $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }


    public function active($host)
    {   
        if ($host->is_active == 1) {
            throw new GeneralException(__('exceptions.backend.access.users.already_confirmed'));
        }

        $host->is_active = 1;

        $host->save();
        return $host;
    }

    public function inActive($host)
    {

        if ($host->is_active == 0) {
            throw new GeneralException(__('exceptions.backend.access.users.not_confirmed'));
        }

        $host->is_active = 0;
        
        $host->save();
        return $host;
    }

    public function create($input)
    {
        return DB::transaction(function () use ($input) {

            $host = new Host;
            $host->host_name = $input['host_name'];
            $host->host_mmname = $input['host_mmname'];
            $host->host_category_id = $input['host_category_id'];
            $host->host_mobileno = $input['host_mobileno'];
            $host->latitude = isset($input['latitude'])? $input['latitude'] : null;
            $host->longitude = isset($input['longitude'])? $input['longitude'] : null;
            $host->address_detail = $input['address_detail'];
            $host->description = $input['description'];
            $host->mm_description = $input['mm_description'];
            $host->city_id = isset($input['city_id'])? $input['city_id'] : null;
            $host->township_id = isset($input['township_id'])? $input['township_id'] : null;
            $host->state_id = $input['state_id'];
            $host->sick_status = isset($input['sick_status']) ? $input['sick_status'] : 0;
            $host->trip_status = isset($input['trip_status']) ? $input['trip_status'] : 0;
            $host->is_active = 0;

            //Image 
            $name=uniqid('host-').'.'.$input['host_logo']->extension();
            $img = Image::make($input['host_logo']); 
            $img->save('uploads/host_logo/'.$name,80);    //save locally  
            $host->host_logo = 'host_logo/'.$img->basename;
            \Storage::disk('local')->putFileAs('host', new File('uploads/host_logo/'.$img->basename), $img->basename);

            if ($host->save()) {
                
                //QR Code
                $host->host_qrcode = config('app.url').'/check-in/'.$host->slug;
                $host->host_qrcode = 'data:image/png;base64,' . base64_encode(\QrCode::format('png')->size(200)->generate($host->host_qrcode));

                $host->save();
                
                return $host->host_id;
            }  
                    
            throw new GeneralException(trans('host::exceptions.backend.host.create_error'));
        });
    }

    public function createHostUser($host_id)
    {
        $host_user = new HostUser;
        $host_user->user_id =auth()->user()->user_id;
        $host_user->host_id =$host_id;
        if ($host_user->save()) {
            return true;
        }
        throw new GeneralException(trans('host::exceptions.backend.host.create_error'));
    }

    public function getActiveAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->where('is_active', 1)
            ->orderBy($orderBy, $sort)
            ->get();
    }
}
