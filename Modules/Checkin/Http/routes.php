<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Checkin\Http\Controllers'], function()
{
    		/*
             * For DataTables
             */
            Route::post('checkin/get', 'CheckinTableController')->name('checkin.get');
            /*
             * User CRUD
             */
            Route::resource('checkin', 'CheckinController');


            Route::get('check_in/advanced_search', 'CheckinController@advanced_search')->name('checkin.advanced_search');
});