<?php

Breadcrumbs::for('admin.checkin.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('checkin::labels.backend.checkin.management'), route('admin.checkin.index'));
});

Breadcrumbs::for('admin.checkin.create', function ($trail) {
    $trail->parent('admin.checkin.index');
    $trail->push(__('checkin::labels.backend.checkin.create'), route('admin.checkin.create'));
});

Breadcrumbs::for('admin.checkin.show', function ($trail, $id) {
    $trail->parent('admin.checkin.index');
    $trail->push(__('checkin::labels.backend.checkin.show'), route('admin.checkin.show', $id));
});

Breadcrumbs::for('admin.checkin.edit', function ($trail, $id) {
    $trail->parent('admin.checkin.index');
    $trail->push(__('checkin::labels.backend.checkin.edit'), route('admin.checkin.edit', $id));
});

Breadcrumbs::for('admin.checkin.advanced_search', function ($trail) {
    $trail->push('Title Here', route('admin.checkin.advanced_search'));
});