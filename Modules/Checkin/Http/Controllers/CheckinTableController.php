<?php

namespace Modules\Checkin\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Checkin\Repositories\CheckinRepository;
use Modules\Checkin\Http\Requests\ManageCheckinRequest;

class CheckinTableController extends Controller
{
    /**
     * @var CheckinRepository
     */
    protected $checkin;

    /**
     * @param CheckinRepository $checkin
     */
    public function __construct(CheckinRepository $checkin)
    {
        $this->checkin = $checkin;
    }

    /**
     * @param ManageCheckinRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCheckinRequest $request)
    {
        return DataTables::of($this->checkin->getForDataTable())
            ->editColumn('visitor_id', function ($checkin) {
                return $checkin->visitor->visitor_name;
            })
            ->editColumn('mobile_no', function ($checkin) {
                return $checkin->visitor->mobile_no;
            })
            ->editColumn('host_id', function ($checkin) {
                return $checkin->host->host_name;
            })
            ->editColumn('is_sick', function ($checkin) {
                if ($checkin->is_sick == 1) {
                    return 'Yes';
                }
                return 'No';
            })
            ->editColumn('is_trip', function ($checkin) {
                if ($checkin->is_trip == 1) {
                    return 'Yes';
                }
                return 'No';
            })
            ->addColumn('actions', function ($checkin) {
                return $checkin->action_buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
