<?php

namespace Modules\Checkin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Checkin\Entities\Checkin;
use Modules\Visitor\Entities\Visitor;
use Modules\Host\Entities\Host;
use Modules\Checkin\Http\Requests\ManageCheckinRequest;
use Modules\Checkin\Http\Requests\CreateCheckinRequest;
use Modules\Checkin\Http\Requests\UpdateCheckinRequest;
use Modules\Checkin\Http\Requests\ShowCheckinRequest;
use Modules\Checkin\Repositories\CheckinRepository;

use Modules\Checkin\Entities\ExcelExports;
use Maatwebsite\Excel\Facades\Excel;
// use Excel;

class CheckinController extends Controller
{
 /**
     * @var CheckinRepository
     * @var CategoryRepository
     */
    protected $checkin;

    /**
     * @param CheckinRepository $checkin
     */
    public function __construct(CheckinRepository $checkin)
    {
        $this->checkin = $checkin;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('checkin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('checkin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateCheckinRequest $request)
    {
        $this->checkin->create($request->except('_token','_method'));
        return redirect()->route('admin.checkin.index')->withFlashSuccess(trans('checkin::alerts.backend.checkin.created'));
    }

    /**
     * @param Checkin              $checkin
     * @param ManageCheckinRequest $request
     *
     * @return mixed
     */
    public function edit(Checkin $checkin, ManageCheckinRequest $request)
    {
        return view('checkin::edit')
            ->withCheckin($checkin);
    }

    /**
     * @param Checkin              $checkin
     * @param UpdateCheckinRequest $request
     *
     * @return mixed
     */
    public function update(Checkin $checkin, UpdateCheckinRequest $request)
    {
        $this->checkin->updateById($checkin->checkin_id,$request->except('_token','_method'));

        return redirect()->route('admin.checkin.index')->withFlashSuccess(trans('checkin::alerts.backend.checkin.updated'));
    }

    /**
     * @param Checkin              $checkin
     * @param ManageCheckinRequest $request
     *
     * @return mixed
     */
    public function show(Checkin $checkin, ShowCheckinRequest $request)
    {
        return view('checkin::show')->withCheckin($checkin);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Checkin $checkin)
    {
        $this->checkin->deleteById($checkin->checkin_id);

        return redirect()->route('admin.checkin.index')->withFlashSuccess(trans('checkin::alerts.backend.checkin.deleted'));
    }


    public function advanced_search(Request $request)
    {
        $input = $request->all();

        $checkin_lists = $this->checkin->getCheckinListForAdvanceSearch($input);
        if (auth()->user()->isAdmin()) {
            $visitors =Visitor::select('visitor_id','mobile_no')->get();
            $hosts =Host::where('is_active', 1)->select('host_id','host_name')->get();
        }else{
            $host_id = $this->checkin->getHostId();
            $checkins = Checkin::where('deleted_at', NULL)->whereIn('host_id', $host_id)->get();
            $visitor_id =[];
            foreach ($checkins as $checkin) {
                $visitor_id[] = $checkin->visitor_id;
            }
            $visitor_id = array_unique($visitor_id);
            $visitors =Visitor::whereIn('visitor_id', $visitor_id)->select('visitor_id','mobile_no')->get();
            $hosts =Host::where('is_active', 1)->whereIn('host_id', $host_id)->select('host_id','host_name')->get();
        }
        $count = 0;$checkin_data = [];

        if($request->has('csv') && $request->csv == "CSV")
        {
            $checkins = $checkin_lists->get();

            
            foreach ($checkins as $checkin)
            {
                $checkin_data[$count]['No'] = $checkin->checkin_id;

                $checkin_data[$count]['Visitor Name'] = $checkin->visitor->visitor_name;
                $checkin_data[$count]['Visitor Phone No.'] = $checkin->visitor->mobile_no;
                $checkin_data[$count]['Visitor Address'] = $checkin->visitor->address;
                $checkin_data[$count]['Host Name'] = $checkin->host->host_name;
                if ($checkin->is_sick == 1) {
                    $checkin_data[$count]['Is Sick'] = 'Yes';
                }else{
                    $checkin_data[$count]['Is Sick'] = 'No';
                }
                if ($checkin->is_trip == 1) {
                    $checkin_data[$count]['Is Trip'] = 'Yes';
                }else{
                    $checkin_data[$count]['Is Trip'] = 'No';
                }
                $checkin_data[$count]['Host Name'] = $checkin->host->host_name;
                $checkin_data[$count]['Date'] = $checkin->updated_at;

                $count++;
            }
            
            return Excel::download(new ExcelExports(collect($checkin_data)), 'balmalae.xlsx');
            
        }
        else
        {
            $checkin_lists = $checkin_lists->paginate(10);

            return view('checkin::advanced_search',compact('checkin_lists' , 'input', 'visitors', 'hosts'));
        }
        
    }
}
