<?php

namespace Modules\Checkin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCheckinRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'host_id' => 'required',
            'name' => 'required',
            'mobile' => 'required|valid_phone_number',
            'address' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function messages()
    {
        return [
            'valid_phone_number' => 'Invalid Mobile No. or Not Support Mobile No.',
            'host_id.required' => 'Please search and select your location',
        ];
    }
}
