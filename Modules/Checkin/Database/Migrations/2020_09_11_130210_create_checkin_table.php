<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\Table;

class CreateCheckinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkin', function (Blueprint $table) {
            $table->increments('checkin_id');
            $table->integer('host_id')->unsigned();
            $table->foreign('host_id')->references('host_id')->on('host')->onDelete('cascade');;
            $table->integer('visitor_id')->unsigned();
            $table->foreign('visitor_id')->references('visitor_id')->on('visitor')->onDelete('cascade');;
            $table->tinyInteger('is_sick')->default(0);
            $table->tinyInteger('is_trip')->default(0);

            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkin');
    }
}
