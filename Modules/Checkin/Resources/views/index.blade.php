@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('checkin::labels.backend.checkin.management'))

@section('breadcrumb-links')
    @include('checkin::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('checkin::labels.backend.checkin.management') }} <small class="text-muted">{{ __('checkin::labels.backend.checkin.list') }}</small>
                </h4>
            </div><!--col-->

            {{--<div class="col-sm-7">
                @include('checkin::includes.header-buttons')
            </div>--}}<!--col-->

            <div class="col-sm-7">
                @include('checkin::includes.advance-search-button')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="checkin-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('checkin::labels.backend.checkin.table.id') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.visitor_name') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.visitor_ph') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.host_name') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.is_sick') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.is_trip') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#checkin-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.checkin.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'checkin_id', name: 'checkin_id'},
                    {data: 'visitor_id', name: 'visitor_id'},
                    {data: 'mobile_no', name: 'mobile_no', searchable: false, sortable: false},
                    {data: 'host_id', name: 'host_id'},
                    {data: 'is_sick', name: 'is_sick'},
                    {data: 'is_trip', name: 'is_trip'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush