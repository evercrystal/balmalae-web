
<div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
    <a href="{{ route('admin.checkin.advanced_search') }}" class="btn btn-info ml-1" data-toggle="tooltip" title="{{ __('checkin::menus.backend.checkin.advanced_search') }}"><i class="fas fa-search"></i> ADVANCE SEARCH</a>
</div>
<!--btn-toolbar-->
