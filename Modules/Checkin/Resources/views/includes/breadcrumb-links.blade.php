<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ __('checkin::menus.backend.checkin.main') }}</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.checkin.index') }}">{{ __('checkin::menus.backend.checkin.all') }}</a>
                @can('create checkin')
                {{--<a class="dropdown-item" href="{{ route('admin.checkin.create') }}">{{ __('checkin::menus.backend.checkin.create') }}</a>--}}
                @endcan
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>