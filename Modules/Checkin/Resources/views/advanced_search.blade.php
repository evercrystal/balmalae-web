@extends ('backend.layouts.app')
@section ('title', app_name() . ' | ' . __('checkin::labels.backend.checkin.advance'))

@section('breadcrumb-links')
    @include('checkin::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
    {{ style('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
    {{ style('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}
    {{ style('assets/plugins/select2/css/select2.min.css') }}
    {{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        

        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('checkin::labels.backend.checkin.advance') }} <small class="text-muted">{{ __('checkin::labels.backend.checkin.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('checkin::includes.advance-search-button')
            </div><!--col-->
        </div><!--row-->
        <hr>
        
        <div class="row">
            {{  html()->form('get', route('admin.checkin.advanced_search'))->class('form-horizontal')->open()  }}
            <input type="hidden" name="csv" value="">
            <div class="col">
                <div class="row">
                    
                    <div class="col-md-4 form-group">
                        <input type="text" value="{{ isset($input['type']) ? ($input['date']) ? $input['date'] : '' : ''}}" class="form-control" style="width: 100%;" name="date" placeholder="Select Date">

                    </div><!--col-->

                    <div class="col-md-4 form-group">
                        <select id="host_id" class="select2" name="host_id" data-placeholder="Choose Host">
                            <option></option>
                            @foreach($hosts as $host)
                                @if(isset($input['host_id']) && $host->host_id == $input['host_id'])
                                    <option value="{{ $host->host_id }}" selected>{{ $host->host_name }}</option>
                                @else
                                    <option value="{{ $host->host_id }}">{{ $host->host_name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div><!--col-md-4-->

                    <div class="col-md-4 form-group">
                        <select id="visitor_id" class="select2" name="visitor_id" data-placeholder="Choose Visitor">
                            <option></option>
                            @foreach($visitors as $visitor)
                                @if(isset($input['visitor_id']) && $visitor->visitor_id == $input['visitor_id'])
                                    <option value="{{ $visitor->visitor_id }}" selected>{{ $visitor->mobile_no }}</option>
                                @else
                                    <option value="{{ $visitor->visitor_id }}">{{ $visitor->mobile_no }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div><!--col-md-4-->

                    <div class="col-md-4 form-group">
                        <button class="btn btn-success" type="submit">
                            {{ __('checkin::labels.backend.checkin.search') }}
                        </button>
                        <input type="submit" id="csv" class="btn btn-warning" value="CSV">
                        <button class="btn btn-danger" id="reset" type="button">
                            {{ __('checkin::labels.backend.checkin.resetbutton') }}
                        </button>
                    </div>

                </div><!--col-->
            </div>
            {{ html()->closeModelForm() }}
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="checkin-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('checkin::labels.backend.checkin.table.id') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.visitor_name') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.visitor_ph') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.host_name') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.is_sick') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.is_trip') }}</th>
                            <th>{{ __('checkin::labels.backend.checkin.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($checkin_lists as $checkin)
                                    <tr>
                                        <td>{{ $checkin->checkin_id }}</td>
                                        <td>
                                            {{ $checkin->visitor->visitor_name}} 
                                        </td>
                                        <td>
                                            {{ $checkin->visitor->mobile_no}} 
                                        </td>

                                        <td>
                                            {{ $checkin->host->host_name}}
                                        </td>

                                        <td>
                                            {{ $checkin->is_sick ?  'Yes':  'No' }}
                                        </td>

                                        <td>
                                            {{ $checkin->is_trip ?  'Yes':  'No' }}
                                        </td>

                                        <td>
                                            {{ $checkin->updated_at }}
                                        </td>

                                        <td>
                                            {!! $checkin->action_buttons !!}
                                        </td>
                                        
                                    </tr>
                            @empty
                                <tr>    
                                    <th colspan="11" style="text-align: center; color: red;">Not Found! There is no checkin.</th>
                                </tr>
                            @endforelse
                            
                        </tbody>
                    </table>
                    {{ $checkin_lists->appends(@$input)->links() }}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}
    {{ script('build/assets/plugins/moment.min.js') }}
    {{ script('assets/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}
    {{ script('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}
    {{ script('assets/plugins/select2/js/select2.full.min.js')}}
    {{ script("assets/plugins/select2/component/components-select2.js") }}

    <script>

        $(function(){

            $(".select").select2({
                placeholder: 'Choose city',
                width: '100%'
            });
        });

        $(function() {
            
            var old_date = `{!! json_encode(@explode(' - ',@$input['date'])) !!}`;
            if(old_date)old_date = JSON.parse(old_date);
            if((old_date).length == 2){
                var start = moment(old_date[0]);
                var end = moment(old_date[1]);
            }
            else{
                var start = moment();
                var end = moment().add(10,'days'); 
            }


            function cb(start, end) {
                $('#reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));

            }

            $('input[name="date"]').daterangepicker({
                opens: 'right',
                format: 'MM/DD/YYYY',
                separator: ' - ',
                startDate: start,
                endDate: end,
                dateLimit: {
                    'days': 30
                },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1,'days'), moment().subtract(1,'days')],
                    'Last 7 Days': [moment().subtract(7,'days'), moment()],
                    // 'Last 10 Days': [moment().subtract(10,'days'), moment()],
                },
                locale: {
                  format: 'MM/DD/YYYY'
                },
            }, cb);
       

            $('#reset').on('click', function(e) {
                $('input[name=date]').val(null).trigger("change");
                $('#visitor_id').val(null).trigger("change");
                $('#host_id').val(null).trigger("change");
                table.draw();
                e.preventDefault();
            });

            $('#csv').on('click',function(e){
                $('input[name=csv]').val('CSV');
            });

        });
    </script>
@endpush