@extends ('backend.layouts.app')

@section ('title', __('checkin::labels.backend.checkin.management'))

@section('breadcrumb-links')
    @include('checkin::includes.breadcrumb-links')
@endsection

@push('after-styles')

@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('checkin::labels.backend.checkin.management') }}
                    <small class="text-muted">{{ __('checkin::labels.backend.checkin.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>{{ __('checkin::labels.backend.checkin.table.visitor_name') }}</th>
                                <td>{{ $checkin->visitor->visitor_name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('checkin::labels.backend.checkin.table.visitor_ph') }}</th>
                                <td>{{ $checkin->visitor->mobile_no }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('checkin::labels.backend.checkin.table.address') }}</th>
                                <td>{{ $checkin->visitor->address }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('checkin::labels.backend.checkin.table.host_name') }}</th>
                                <td>{{ $checkin->host->host_name }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('checkin::labels.backend.checkin.table.is_sick') }}</th>
                                <td>{{ $checkin->is_sick ?  'Yes':  'No' }}</td>
                            </tr>
                            <tr>
                                <th>{{ __('checkin::labels.backend.checkin.table.is_trip') }}</th>
                                <td>{{ $checkin->is_trip ?  'Yes':  'No' }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('checkin::labels.backend.checkin.table.created') }}:</strong> {{ $checkin->updated_at->timezone(get_user_timezone()) }} ({{ $checkin->created_at->diffForHumans() }}),
                    <strong>{{ __('checkin::labels.backend.checkin.table.last_updated') }}:</strong> {{ $checkin->created_at->timezone(get_user_timezone()) }} ({{ $checkin->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush