<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'checkin' => 'Checkin',
                'checkin_bin' => 'Checkin Bin',
            ],
            'checkin' => [
                'all'        => 'All Checkin',
                'create'     => 'Create Checkin',
                'advanced_search'=> 'Advance Search',
                'edit'       => 'Edit Checkin',
                'show'       => 'Show Checkin',
                'management' => 'Checkin Management',
                'main'       => 'Checkin',
            ]
        ]
];