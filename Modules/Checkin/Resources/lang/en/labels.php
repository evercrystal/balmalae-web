<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'checkin' => [
                    'create'     => 'Create Checkin',
                    'edit'       => 'Edit Checkin',
                    'management' => 'Checkin Management',
                    'list'       => 'Checkin List',
                    'show'       => 'Checkin Detail',
                    'advance'    => 'Advance Search Checkin',
                    'resetbutton'=>'Reset',
                    'search'     =>'Search',
                    'csv'        =>'CSV',

                    'table' => [
                        'number_of_users' => 'Number of Checkins',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'address'          => 'Visitor Address',
                        'host_name'        => 'Host Name',
                        'visitor_name'     => 'Visitor Name',
                        'visitor_ph'       => 'Visitor Phone No.',
                        'is_sick'          => 'Sick Record',
                        'is_trip'          => 'Trip Record',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'checkin total|checkin total',
                    ]
                ]
            ]

];