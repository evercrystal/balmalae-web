<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'checkin' => [
            'created' => 'The checkin was successfully created.',
            'deleted' => 'The checkin was successfully deleted.',
            'updated' => 'The checkin was successfully updated.'
        ]
    ]
];