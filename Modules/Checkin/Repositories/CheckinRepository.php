<?php

namespace Modules\Checkin\Repositories;

use Modules\Checkin\Entities\Checkin;
use Modules\Host\Entities\HostUser;
use Modules\Visitor\Entities\Visitor;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CheckinRepository.
 */
class CheckinRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function __construct(Checkin $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        if (auth()->user()->isAdmin()) {
            return $this->model
            ->select('*');
        }

        $host_id = $this->getHostId();
        return $this->model
        ->whereIn('host_id', $host_id)
        ->select('*');
    }

    public function getHostId()
    {
        $user_id = auth()->user()->user_id;
        $hosts = HostUser::where('user_id', $user_id)->select('host_id')->get();
        $host_id =[];
        foreach ($hosts as $host) {
            $host_id[] = $host->host_id;
        }
        return $host_id;
    }

    public function getForCheckInHistory()
    {
        $visitor = Visitor::where('user_id', auth()->user()->user_id)->first();
        return $this->model
            ->where('visitor_id', $visitor->visitor_id)
            ->select('*');
    }

    public function create($input)
    {
        
        return DB::transaction(function () use ($input) {

            $checkin = new Checkin;
            $checkin->host_id = $input['host_id'];
            $checkin->visitor_id = $input['visitor_id'];
            $checkin->is_sick = isset($input['is_sick']) ? 1 : 0;
            $checkin->is_trip = isset($input['is_trip']) ? 1 : 0;


            if ($checkin->save()) 
            {
                return $checkin->checkin_id;
            }  
                    
            throw new GeneralException(trans('checkin::exceptions.backend.checkin.create_error'));
        });
            
    }

    public function getCheckinListForAdvanceSearch($input)
    {
        if (auth()->user()->isAdmin()) {
            $checkins = Checkin::where('deleted_at', NULL)->orderBy('created_at', 'asc');
        }else{
            $host_id = $this->getHostId();
            $checkins = Checkin::where('deleted_at', NULL)->whereIn('host_id', $host_id)->orderBy('created_at', 'asc');
        }


        if (isset($input)) {
            if(isset($input['date']) && isset($input['visitor_id']) && isset($input['host_id']))
            {
                $date = $this->get_start_end_date($input['date']);
                $checkins->whereBetween('created_at', [$date['from'], $date['to']])->where('visitor_id',$input['visitor_id'])->where('host_id',$input['host_id']); 
                return $checkins;   
            }
            
            if(isset($input['visitor_id']) && isset($input['date']))
            {
                $date = $this->get_start_end_date($input['date']);
                $checkins->whereBetween('created_at', [$date['from'], $date['to']])->where('visitor_id',$input['visitor_id']); 
                return $checkins;   
            }
            if(isset($input['host_id']) && isset($input['date']))
            {
                $date = $this->get_start_end_date($input['date']);
                $checkins->whereBetween('created_at', [$date['from'], $date['to']])->where('host_id',$input['host_id']);
                return $checkins;    
            }
            if(isset($input['host_id']) && isset($input['visitor_id']))
            {
               
                $checkins->where('visitor_id',$input['visitor_id'])->where('host_id',$input['host_id']);
                return $checkins;    
            }
            if(isset($input['date']))
            {
                $date = $this->get_start_end_date($input['date']);
                $checkins->whereBetween('created_at', [$date['from'], $date['to']]);
                return $checkins;    
            }
            if(isset($input['visitor_id']))
            {
                $checkins->where('visitor_id',$input['visitor_id']); 
                return $checkins;   
            }
            if(isset($input['host_id']))
            {
                $checkins->where('host_id',$input['host_id']);
                return $checkins;    
            }
            
            
        }
            
        return $checkins;
            
    }

    public function get_start_end_date($date)
    {
        $date = explode(' - ', $date);
        $start = date('Y-m-d', strtotime($date[0]));
        $end = date('Y-m-d', strtotime($date[1]));
        $from = $start . ' 00:00:00';
        $to = $end . ' 23:59:59';

        return [
            'from' => $from,
            'to' => $to
        ];
    }
}
