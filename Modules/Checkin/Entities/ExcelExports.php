<?php

namespace Modules\Checkin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExcelExports implements FromCollection, WithHeadings
{   
    public function __construct($checkin_data){
        $this->checkin_data = $checkin_data;
       
    }
    /**
        * @return \Illuminate\Support\Collection
        */
        public function collection()
        {   
            // dd(User::all());
            return $this->checkin_data; 
        }

        public function headings():array{
            return [
                'ID',
                'VISITOR_NAME',
                'VISITOR_Phone_No.',
                'VISITOR_Address',
                'HOST_NAME',
                'Is_Sick',
                'Is_Trip',
                'DATE'
            ];
        }
}
