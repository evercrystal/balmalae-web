<?php

namespace Modules\Checkin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\Host\Entities\Host;
use Modules\Visitor\Entities\Visitor;

class Checkin extends Model
{
    use SoftDeletes;
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'checkin';

    protected $primaryKey = 'checkin_id';

    protected $fillable = ["checkin_id", "host_id", "visitor_id", "is_sick", "is_trip"];

    public function host()
    {
        return $this->hasOne(Host::class, 'host_id', 'host_id');
    }

    public function visitor()
    {
        return $this->hasOne(Visitor::class, 'visitor_id', 'visitor_id');
    }
       /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
    	if(auth()->user()->can('view checkin')){
        	return '<a href="'.route('admin.checkin.show', $this->checkin_id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
    	if(auth()->user()->can('edit checkin')){
        	return '<a href="'.route('admin.checkin.edit', $this->checkin_id).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
       	return '';
    }

     /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete checkin')) {
            return '<a href="'.route('admin.checkin.destroy', $this->checkin_id).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
            return $this->getShowButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
