@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('country::labels.backend.country.management'))

@section('breadcrumb-links')
    @include('country::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('country::labels.backend.country.management') }} <small class="text-muted">{{ __('country::labels.backend.country.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('country::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="country-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('country::labels.backend.country.table.id') }}</th>
                            <th>{{ __('country::labels.backend.country.table.name') }}</th>
                            <th>{{ __('country::labels.backend.country.table.country_code') }}</th>
                            <th>{{ __('country::labels.backend.country.table.last_updated') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#country-table').DataTable({
                processing: true,
                language:{processing: "<div class='overlay custom-loader-background' style='font-size:2em;'><i class='fa fa-cog fa-spin custom-loader-color'></i></div>"},
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.country.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'name', name: 'name'},
                    {data: 'country_code', name: 'country_code'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush