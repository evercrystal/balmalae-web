<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'country' => [
            'created' => 'The country was successfully created.',
            'deleted' => 'The country was successfully deleted.',
            'not_deleted' => 'This country was used in other table. Cannot deleted yet.',
            'updated' => 'The country was successfully updated.'
        ]
    ]
];