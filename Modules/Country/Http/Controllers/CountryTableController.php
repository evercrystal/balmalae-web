<?php

namespace Modules\Country\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\Country\Repositories\CountryRepository;
use Modules\Country\Http\Requests\ManageCountryRequest;

class CountryTableController extends Controller
{
    /**
     * @var CountryRepository
     */
    protected $country;

    /**
     * @param CountryRepository $country
     */
    public function __construct(CountryRepository $country)
    {
        $this->country = $country;
    }

    /**
     * @param ManageCountryRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageCountryRequest $request)
    {
        return DataTables::of($this->country->getForDataTable())
            ->addColumn('actions', function ($country) {
                return $country->action_buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }
}
