<?php

namespace Modules\Country\Repositories;

use Modules\Country\Entities\Country;
use Modules\City\Entities\City;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CountryRepository.
 */
class CountryRepository extends BaseRepository
{
    /**
     * @return string
     */
    private $city;

    public function __construct(Country $model, City $city)
    {
        $this->model = $model;
        $this->city = $city;
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }
}
