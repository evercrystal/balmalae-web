<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\Table;

class CreateSubrouteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Table::SUB_ROUTE, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('to_township_id')->unsigned();
            $table->foreign('to_township_id')->references('id')->on(Table::TOWNSHIP)->onDelete('cascade');
            $table->integer('from_township_id')->unsigned();
            $table->foreign('from_township_id')->references('id')->on(Table::TOWNSHIP)->onDelete('cascade');
            $table->integer('type');
            $table->float('max_weight');
            $table->float('max_height');
            $table->float('max_width');
            $table->float('max_length')->nullable();
            $table->float('mmk_price');
            $table->float('usd_price');
            $table->timestamp('updated_at');
            $table->timestamp('created_at')->useCurrent();
            $table->softDeletes();
        });

        Schema::create(Table::PIVOT_ROUTE, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('route_id')->unsigned();
            $table->foreign('route_id')->references('id')->on(Table::ROUTE)->onDelete('cascade');
            $table->integer('sub_route_id')->unsigned();
            $table->foreign('sub_route_id')->references('id')->on(Table::SUB_ROUTE)->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Table::SUB_ROUTE);
        Schema::dropIfExists(Table::PIVOT_ROUTE);
    }
}
