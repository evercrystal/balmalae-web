<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'sidebar' => [
                'subroute' => 'SubRoute',
                'subroute_bin' => 'SubRoute Bin',
            ],
            'subroute' => [
                'all'        => 'All SubRoute',
                'create'     => 'Create SubRoute',
                'edit'       => 'Edit SubRoute',
                'show'       => 'Show SubRoute',
                'management' => 'SubRoute Management',
                'main'       => 'SubRoute',
            ]
        ]
];