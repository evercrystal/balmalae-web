<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
            'subroute' => [
                'create_error'      => 'There was a problem creating this subroute. Please try again.',
                'delete_error'      => 'There was a problem deleting this subroute. Please try again.',
                'not_found'         => 'That subroute does not exist.',
                'update_error'      => 'There was a problem updating this subroute. Please try again.',
                'save_route_error'  => 'Something Went Wrong.Unable to save your routes!',
            ]
        ]
];
