<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */
  'backend' => [
                'subroute' => [
                    'create'     => 'Create SubRoute',
                    'edit'       => 'Edit SubRoute',
                    'management' => 'SubRoute Management',
                    'list'       => 'SubRoute List',
                    'show'       => 'SubRoute Detail',

                    'table' => [
                        'number_of_users' => 'Number of SubRoutes',
                        'sort'             => 'Sort',
                        'id'               => 'ID',
                        'name'             => 'Name',
                        'to_township'      => 'To Township',
                        'from_township'    => 'From Township',
                        'type'             => 'Type',
                        'max_width'        => 'Max Width',
                        'max_height'       => 'Max Height',
                        'max_weight'       => 'Max Weight',
                        'max_length'       => 'Max Length',
                        'mmk_price'        => 'MMK Price',
                        'usd_price'        => 'USD Price',
                        'route'            => 'Choose Route',
                        'routes'           => 'Routes',
                        'description'      => 'Description',
                        'created'          => 'Created',
                        'last_updated'     => 'Last Updated',
                        'total'            => 'subroute total|subroute total',
                    ]
                ]
            ]

];