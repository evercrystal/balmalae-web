<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'subroute' => [
            'created' => 'The subroute was successfully created.',
            'deleted' => 'The subroute was successfully deleted.',
            'updated' => 'The subroute was successfully updated.'
        ]
    ]
];