@extends ('backend.layouts.app')

@section ('title', __('subroute::labels.backend.subroute.management') . ' | ' . __('subroute::labels.backend.subroute.edit'))

@section('breadcrumb-links')
    @include('subroute::includes.breadcrumb-links')
@endsection

@push('after-styles')
{{ style('assets/plugins/select2/css/select2.min.css') }}
{{ style('assets/plugins/select2/css/select2-bootstrap.min.css') }}
@endpush

@section('content')
{{ html()->modelForm($subroute, 'PATCH', route('admin.subroute.update', $subroute->id))->class('form-horizontal')->open() }}
<div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('subroute::labels.backend.subroute.management') }}
                        <small class="text-muted">{{ __('subroute::labels.backend.subroute.create') }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr />

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('subroute::labels.backend.subroute.table.name'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.from_township').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('type') }}

                        <div class="col-md-10">
                            <select name="from_township_id" id="" class="form-control select2">
                                @foreach($townships as $township)
                                    <option value="{{$township->id}}" @if($township->id == $subroute->from_township_id) selected @endif>{{ $township->name}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('subroute::labels.backend.subroute.table.to_township').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('type') }}

                        <div class="col-md-10">
                            <select name="to_township_id" id="" class="form-control select2">
                                @foreach($townships as $township)
                                    <option value="{{$township->id}}" @if($township->id == $subroute->to_township_id) selected @endif >{{ $township->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.type').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('type') }}

                        <div class="col-md-10">
                            <select name="type" id="type" class="form-control select2">
                                @foreach($types as $key=>$value)
                                    <option value="{{$key}}" >{{ $value}}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                      
                    <div class="form-group row">
                        {{ html()->label(__('subroute::labels.backend.subroute.table.route').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('type') }}

                        <div class="col-md-10">
                            <select class="js-example-basic-multiple select2" name="routes[]" multiple="multiple">
                                @foreach($routes as $route)
                                    <option value="{{$route->id}}"  
                                    @foreach($routeIds as $id)
                                        @if($route->id == $id) selected @endif
                                    @endforeach
                                        >{{ $route->fromCity->name }} to {{ $route->toCity->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.max_height').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('max_height') }}

                        <div class="col-md-10">
                            {{ html()->text('max_height')
                                ->class('form-control')
                                ->placeholder(__('subroute::labels.backend.subroute.table.max_height'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.max_weight').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('max_weight') }}

                        <div class="col-md-10">
                            {{ html()->text('max_weight')
                                ->class('form-control')
                                ->placeholder(__('subroute::labels.backend.subroute.table.max_weight'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.max_width').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('max_width') }}

                        <div class="col-md-10">
                            {{ html()->text('max_width')
                                ->class('form-control')
                                ->placeholder(__('subroute::labels.backend.subroute.table.max_width'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.max_length'))->class('col-md-2 form-control-label')->for('max_length') }}

                        <div class="col-md-10">
                            {{ html()->text('max_length')
                                ->class('form-control')
                                ->placeholder(__('subroute::labels.backend.subroute.table.max_length'))
                                ->attribute('maxlength', 191)
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.mmk_price').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('mmk_price') }}

                        <div class="col-md-10">
                            {{ html()->text('mmk_price')
                                ->class('form-control')
                                ->placeholder(__('subroute::labels.backend.subroute.table.mmk_price'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('subroute::labels.backend.subroute.table.usd_price').'<span class="text-danger">*</span>')->class('col-md-2 form-control-label')->for('usd_price') }}

                        <div class="col-md-10">
                            {{ html()->text('usd_price')
                                ->class('form-control')
                                ->placeholder(__('subroute::labels.backend.subroute.table.usd_price'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.subroute.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->{{ html()->closeModelForm() }}
@endsection

@push('after-scripts')

{{ script('assets/plugins/select2/js/select2.full.min.js')}}
{{ script("assets/plugins/select2/component/components-select2.js") }}
<script>
    $(function(){

        $(".select").select2({
            placeholder: 'Choose city',
            width: '100%'
        });
    });
</script>
@endpush