@extends ('backend.layouts.app')

@section ('title', app_name() . ' | ' . __('subroute::labels.backend.subroute.management'))

@section('breadcrumb-links')
    @include('subroute::includes.breadcrumb-links')
@endsection

@push('after-styles')
    {{ style("https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css") }}
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('subroute::labels.backend.subroute.management') }} <small class="text-muted">{{ __('subroute::labels.backend.subroute.list') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('subroute::includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table id="subroute-table" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.id') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.name') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.from_township') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.to_township') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.max_weight') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.max_height') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.max_width') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.mmk_price') }}</th>
                            <th>{{ __('subroute::labels.backend.subroute.table.usd_price') }}</th>
                            <th>{{ __('labels.general.actions') }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection

@push('after-scripts')
    {{ script("https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js") }}
    {{ script("js/plugin/datatables/dataTables-extend.js") }}

    <script>
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#subroute-table').DataTable({
                serverSide: true,
                ajax: {
                    url: '{!! route("admin.subroute.get") !!}',
                    type: 'post',
                    error: function (xhr, err) {
                        if (err === 'parsererror')
                            location.reload();
                        else swal(xhr.responseJSON.message);
                    }
                },
                columns: [
                    {data: 'id', name: 'id'},
                    {data:'name', name: 'name'},
                    {data:'to_township_id', name: 'to_township_id'},
                    {data:'from_township_id', name: 'from_township_id'},
                    {data:'max_weight', name: 'max_weight'},
                    {data:'max_height', name: 'max_height'},
                    {data:'max_width', name: 'max_width'},
                    {data:'mmk_price', name: 'mmk_price'},
                    {data:'usd_price', name: 'usd_price'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
                order: [[0, "asc"]],
                searchDelay: 500,
                fnDrawCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    load_plugins();
                }
            });
        });
    </script>
@endpush