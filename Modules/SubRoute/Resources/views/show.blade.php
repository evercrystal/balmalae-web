@extends ('backend.layouts.app')

@section ('title', __('subroute::labels.backend.subroute.management'))

@section('breadcrumb-links')
    @include('subroute::includes.breadcrumb-links')
@endsection

@push('after-styles')
<style>
    .route-style{
        background-color: #8bdf8b;
        padding: 7px 5px;
        border-radius: 10px;
        margin-right: 5px;
    }
</style>
@endpush

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('subroute::labels.backend.subroute.management') }}
                    <small class="text-muted">{{ __('subroute::labels.backend.subroute.show') }}</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.name') }}</th>
                            <td>{{ $subroute->name }}</td>
                        </tr>

                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.to_township') }}</th>
                            <td>{{ $subroute->toTownship->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.from_township') }}</th>
                            <td>{{ $subroute->fromTownship->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.routes') }}</th>
                            
                            <td>
                                @foreach($subroute->route->sortByDesc('pivot.create_at') as $route)
                                <span class="route-style">
                                    {{ $route->fromCity->name }} -  {{ $route->toCity->name }}
                                </span>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.type') }}</th>         
                            <td>{!! $deliveryType !!}</td>
                        </tr>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.max_height') }}</th>
                            <td>{{ $subroute->max_height }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.max_weight') }}</th>
                            <td>{{ $subroute->max_weight }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.max_width') }}</th>
                            <td>{{ $subroute->max_width }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.max_length') }}</th>
                            <td>{{ $subroute->max_length }}</td>
                        </tr>

                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.mmk_price') }}</th>
                            <td>{{ $subroute->mmk_price }}</td>
                        </tr>

                        <tr>
                            <th>{{ __('subroute::labels.backend.subroute.table.usd_price') }}</th>
                            <td>{{ $subroute->usd_price }}</td>
                        </tr>

                    </tbody>
                </table>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>{{ __('subroute::labels.backend.subroute.table.created') }}:</strong> {{ $subroute->updated_at->timezone(get_user_timezone()) }} ({{ $subroute->created_at->diffForHumans() }}),
                    <strong>{{ __('subroute::labels.backend.subroute.table.last_updated') }}:</strong> {{ $subroute->created_at->timezone(get_user_timezone()) }} ({{ $subroute->updated_at->diffForHumans() }})
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection

@push('after-scripts')

<script>


</script>
@endpush