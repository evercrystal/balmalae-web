<?php

namespace Modules\SubRoute\Repositories;

use Modules\SubRoute\Entities\SubRoute;
use Modules\Route\Entities\Route;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SubRouteRepository.
 */
class SubRouteRepository extends BaseRepository
{
    /**
     * @return string
     */
    private $route;
    public function __construct(SubRoute $model, Route $route)
    {
        $this->model = $model;
        $this->route = $route;
    }


    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getAll($orderBy = 'created_at', $sort = 'desc')
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->get();
    }

    /**
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model
            ->select('*');
    }

    public function create(array $input) : bool
    {
        \DB::beginTransaction();
        try {
            $subroute = new SubRoute;
            $subroute->create($input);      
            $subrouteData=$subroute->latest()->first();   
            $subrouteData->route()->attach($input['routes']);

        } catch (\Exception $e) {
            DB::rollback();
            throw new GeneralException(trans('subroute::exceptions.backend.subroute.save_route_error'));
        }

        \DB::commit();
        return true;
    }
}
