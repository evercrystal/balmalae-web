<?php

namespace Modules\SubRoute\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Enums\Table;
use Modules\Township\Entities\Township;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Entities\SubRoute;

class PivotRoute extends Model
{
	public $timestamps = false;
    
    protected $table = Table::PIVOT_ROUTE;

    protected $fillable = ["id", "route_id", "sub_route_id"];

       /**
     * @return string
     */
    public function route()
    {
        return $this->belongsTo(Route::class, 'route_id');

    }

    public function subroute()
    {
        return $this->belongsTo(SubRoute::class, 'sub_route_id');

    }

    public function toTownship()
    {
        return $this->belongsTo(Township::class, 'to_township_id');
    }
    
    public function fromTownship()
    {
        return $this->belongsTo(Township::class, 'from_township_id');
    }
    
}
