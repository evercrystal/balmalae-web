<?php

namespace Modules\SubRoute\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Enums\Table;
use Modules\Township\Entities\Township;
use Modules\Route\Entities\Route;
class SubRoute extends Model
{
    use SoftDeletes;
	 /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = Table::SUB_ROUTE;

    protected $fillable = ["id", "name", "from_township_id","to_township_id", "type", "max_height", "max_length", "max_width", "max_weight", "mmk_price", "usd_price"];

       /**
     * @return string
     */
    public function route()
    {
        return $this->belongsToMany(Route::class, Table::PIVOT_ROUTE);
    }

    public function toTownship()
    {
        return $this->belongsTo(Township::class, 'to_township_id');
    }
    
    public function fromTownship()
    {
        return $this->belongsTo(Township::class, 'from_township_id');
    }
    
    public function getShowButtonAttribute()
    {
    	if(auth()->user()->can('view subroute')){
        	return '<a href="'.route('admin.subroute.show', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.view').'" class="btn btn-info"><i class="fas fa-eye"></i></a>';
        }
       	return '';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
    	if(auth()->user()->can('edit subroute')){
        	return '<a href="'.route('admin.subroute.edit', $this).'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
        }
       	return '';
    }

     /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        if (auth()->user()->can('delete subroute')) {
            return '<a href="'.route('admin.subroute.destroy', $this).'" data-method="delete"
                 data-trans-button-cancel="'.__('buttons.general.cancel').'"
                 data-trans-button-confirm="'.__('buttons.general.crud.delete').'"
                 data-trans-title="'.__('strings.backend.general.are_you_sure').'" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.delete').'" class="btn btn-danger"><i class="fas fa-trash"></i></a> ';
        }

        return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
            return $this->getShowButtonAttribute().$this->getEditButtonAttribute().$this->getDeleteButtonAttribute();
    }
}
