<?php

namespace Modules\SubRoute\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Http\Requests\ManageSubRouteRequest;
use Modules\SubRoute\Http\Requests\CreateSubRouteRequest;
use Modules\SubRoute\Http\Requests\UpdateSubRouteRequest;
use Modules\SubRoute\Http\Requests\ShowSubRouteRequest;
use Modules\SubRoute\Repositories\SubRouteRepository;
use Modules\Township\Repositories\TownshipRepository;
use Modules\Route\Repositories\RouteRepository;
use Modules\Township\Entities\Township;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Enum\DeliveryType;

class SubRouteController extends Controller
{
 /**
     * @var SubRouteRepository
     * @var CategoryRepository
     */
    protected $subroute;
    protected $township;
    protected $route;
    /**
     * @param SubRouteRepository $subroute
     */
    public function __construct(SubRouteRepository $subroute, TownshipRepository $township, RouteRepository $route)
    {
        $this->subroute = $subroute;
        $this->township = $township;
        $this->route = $route;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('subroute::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $townships = $this->township->getAll('id','asc');
        $routes = $this->route->getAll('id','asc');
        $types = DeliveryType::AVAILABLES;
        return view('subroute::create', compact('townships', 'types','routes'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(CreateSubRouteRequest $request)
    {
        $input = $request->except('_token','_method');
        $this->subroute->create($input);
        return redirect()->route('admin.subroute.index')->withFlashSuccess(trans('subroute::alerts.backend.subroute.created'));
    }

    /**
     * @param SubRoute              $subroute
     * @param ManageSubRouteRequest $request
     *
     * @return mixed
     */
    public function edit(SubRoute $subroute, ManageSubRouteRequest $request)
    {
        $townships = $this->township->getAll('id','asc');
        $routes = $this->route->getAll('id','asc');
        $types = DeliveryType::AVAILABLES;
        foreach($subroute->route as $route)
        {
            $routeIds []= $route->id;
        }
        return view('subroute::edit',compact('subroute','townships','routes','types','routeIds'));
    }

    /**
     * @param SubRoute              $subroute
     * @param UpdateSubRouteRequest $request
     *
     * @return mixed
     */
    public function update(SubRoute $subroute, UpdateSubRouteRequest $request)
    {
        $input = $request->except('_token','_method');
        $this->subroute->updateById($subroute->id,$input);
        $subroute->route()->sync($input['routes']);
        return redirect()->route('admin.subroute.index')->withFlashSuccess(trans('subroute::alerts.backend.subroute.updated'));
    }

    /**
     * @param SubRoute              $subroute
     * @param ManageSubRouteRequest $request
     *
     * @return mixed
     */
    public function show(SubRoute $subroute, ShowSubRouteRequest $request)
    {
        $types = DeliveryType::AVAILABLES;
        $deliveryType = $types[$subroute->type];
        return view('subroute::show',compact('subroute','deliveryType'));
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(SubRoute $subroute)
    {
        $this->subroute->deleteById($subroute->id);
        $subroute->route()->detach();
        return redirect()->route('admin.subroute.index')->withFlashSuccess(trans('subroute::alerts.backend.subroute.deleted'));
    }
}
