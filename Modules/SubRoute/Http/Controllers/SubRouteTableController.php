<?php

namespace Modules\SubRoute\Http\Controllers;

use Illuminate\Routing\Controller;
use DataTables;
use Modules\SubRoute\Repositories\SubRouteRepository;
use Modules\SubRoute\Http\Requests\ManageSubRouteRequest;

class SubRouteTableController extends Controller
{
    /**
     * @var SubRouteRepository
     */
    protected $subroute;

    /**
     * @param SubRouteRepository $subroute
     */
    public function __construct(SubRouteRepository $subroute)
    {
        $this->subroute = $subroute;
    }

    /**
     * @param ManageSubRouteRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageSubRouteRequest $request)
    {
        return DataTables::of($this->subroute->getForDataTable())
            ->editColumn('from_township_id', function ($subroute) {
                return $subroute->fromTownship->name;
            })
            ->editColumn('to_township_id', function ($subroute) {
                return $subroute->toTownship->name;
            })
            ->addColumn('actions', function ($subroute) {
                return $subroute->action_buttons;
            })
            ->rawColumns(['actions','from_township_id','to_township_id'])
            ->make(true);
    }
}
