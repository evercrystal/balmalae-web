<?php

Route::group(['middleware' => ['web','admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\SubRoute\Http\Controllers'], function()
{
    /*
        * For DataTables
        */
    Route::post('subroute/get', 'SubRouteTableController')->name('subroute.get');
    /*
        * User CRUD
        */
    Route::resource('subroute', 'SubRouteController');
});