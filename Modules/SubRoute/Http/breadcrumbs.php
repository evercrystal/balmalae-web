<?php

Breadcrumbs::for('admin.subroute.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('subroute::labels.backend.subroute.management'), route('admin.subroute.index'));
});

Breadcrumbs::for('admin.subroute.create', function ($trail) {
    $trail->parent('admin.subroute.index');
    $trail->push(__('subroute::labels.backend.subroute.create'), route('admin.subroute.create'));
});

Breadcrumbs::for('admin.subroute.show', function ($trail, $id) {
    $trail->parent('admin.subroute.index');
    $trail->push(__('subroute::labels.backend.subroute.show'), route('admin.subroute.show', $id));
});

Breadcrumbs::for('admin.subroute.edit', function ($trail, $id) {
    $trail->parent('admin.subroute.index');
    $trail->push(__('subroute::labels.backend.subroute.edit'), route('admin.subroute.edit', $id));
});
