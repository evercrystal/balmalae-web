<?php

namespace Modules\SubRoute\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSubRouteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'from_township_id' => 'required',
            'to_township_id'   => 'required',
            'type'             => 'required',
            'routes'           => 'required',
            'max_weight'       => 'required|numeric',
            'max_height'       => 'required|numeric',
            'max_width'        => 'required|numeric',
            'mmk_price'        => 'required|numeric',
            'usd_price'        => 'required|numeric'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->can('create subroute');
    }
}
