<?php
namespace Modules\SubRoute\Enum;

class DeliveryType
{
    const ID_CAR = 1;
    const ID_BICYCLE = 2;

    const NAME_CAR = "Car";
    const NAME_BICYCLE = "Bicycle";

    const AVAILABLES = [
        self::ID_CAR => self::NAME_CAR,
        self::ID_BICYCLE => self::NAME_BICYCLE
    ];
}
