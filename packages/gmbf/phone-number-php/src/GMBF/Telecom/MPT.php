<?php 

namespace GMBF\Telecom;

class MPT implements TelecomInterface
{
    /**
     * Check for Provided Phone Number is belongs to MPT Network
     *
     * @param $number Phone Number
     * @return bool
     */
	public function check($number)
	{
		$area_code = substr($number,0,2);
		if ($area_code == '09') {
			return preg_match(
				'/^(09|\+?959)((20|21|22|23|24|50|51|52|53|54|55|56|83|85|86|87)\d{5}|(41|43|47|49|73|91)\d{6}|(25|26|40|42|44|45|48|88|89)\d{7})$/', 
				$number
			) ? true : false;
		}else{
			return preg_match(
				'/^(01|02|081|069|061|052|059|070|044|073|084|082|066|083|045|063|057|064|065|071|032|074|062|042|053|085|067|072|075|056|068|054|060)\d{6,8}$/', $number
			) ? true : false;
		}

		
	}
}