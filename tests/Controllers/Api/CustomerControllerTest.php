<?php

namespace Tests\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Modules\Customer\Entities\Customer;
use Modules\Township\Entities\Township;
use App\Services\HashService;
use Tests\TestCase;

class CustomerControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testVerifyCustomerFailValidationError()
    {
        $response = $this->post('/api/v1/verify-customer', []);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'mobile' => ['The mobile field is required.'],
                    'customerRefNo' => ['The customer ref no field is required.'],
                    'hashValue' => ['The hash value field is required.']
                ]
        ], $responseData);
    }

    public function testVerifyCustomerFailError()
    {
        $response = $this->post('/api/v1/verify-customer', [
            'mobile'=>'959954555372',
            'customerRefNo'=>'BNF-WAHBTQ',
            'hashValue'=>'5D85937464DEE96FD16CFEDEBFC4DF186C24147D7B92300DCFB97ED37D9C0AB2'
        ]);
        $response->assertStatus(500);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 500,
                'message' => trans("errors.customer_does_not_exist"),
                'errors' => []
        ], $responseData);

        $customer = factory(Customer::class)->create();

        $response = $this->post('/api/v1/verify-customer', [
            'mobile'=>$customer->mobile,
            'customerRefNo'=>$customer->ref_id,
            'hashValue'=>'5D85937464DEE96FD16CFEDEBFC4DF186C24147D7B92300DCFB97ED37D9C0AB2'
        ]);
        $response->assertStatus(500);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 500,
                'message' => trans("errors.invalid_hash_value"),
                'errors' => []
        ], $responseData);
    }

    public function testVerifyCustomerSuccess()
    {
        $customer = factory(Customer::class)->create();

        $response = $this->post('/api/v1/verify-customer', [
            'mobile'=>$customer->mobile,
            'customerRefNo'=>$customer->ref_id,
            'hashValue'=> (new HashService)->encrypt('mobile='.$customer->mobile.'&customerRefNo='.$customer->ref_id, $customer->hash_key)
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();
        $this->assertEquals([
                'http_code' => 200,
                'message' => 'success'
        ], $responseData);
    }

    public function testGetLocationFailValidationError()
    {
        $response = $this->post('/api/v1/get-location-data', []);
        $response->assertStatus(400);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals([
                'http_code' => 400,
                'message' => 'Bad Request',
                'errors' => [
                    'customerRefNo' => ['The customer ref no field is required.'],
                    'hashValue' => ['The hash value field is required.']
                ]
        ], $responseData);
    }

    public function testGetLocationSuccess()
    {
        $customer = factory(Customer::class)->create();

        $township = factory(Township::class)->create();

        $response = $this->post('/api/v1/get-location-data', [
            'customerRefNo'=>$customer->ref_id,
            'hashValue'=> (new HashService)->encrypt('customerRefNo='.$customer->ref_id, $customer->hash_key)
        ]);
        $response->assertStatus(200);
        $responseData = $response->decodeResponseJson();

        $this->assertEquals(1, sizeof($responseData['countries']['data']));
        $this->assertEquals(1, sizeof($responseData['states']['data']));
        $this->assertEquals(1, sizeof($responseData['cities']['data']));
        $this->assertEquals(1, sizeof($responseData['townships']['data']));
    }
}
