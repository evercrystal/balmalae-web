<?php

namespace Tests\Controllers\Backend;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class AppSettingControllerTest.
 */
class AppSettingControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testAppSettingIndexSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/appsetting');

        $response->assertStatus(200);
    }


    public function testAppSettingUpdateSuccess()
    {
        $this->loginAsAdmin();

        $content = file_get_contents(base_path() . '/.env.testing');

        $response = $this->post('/admin/appsetting', [
            'tab' => 'basic',
            'qr_prefix' => 'BNF',
            'filename' => 'env.testing',
        ]);

        $response->assertSessionHas(['flash_success' => __('appsetting::alerts.backend.appsetting.updated')]);

         $response = $this->post('/admin/appsetting', [
            'tab' => 'email_sms',
            'filename' => 'env.testing',
            'mail_driver' => 'smtp',
            'filename' => 'env.testing',

        ]);

        $response->assertSessionHas(['flash_success' => __('appsetting::alerts.backend.appsetting.updated')]);
    }

    public function testAppSettingUpdateFail()
    {
        $this->loginAsAdmin();

        $content = file_get_contents(base_path() . '/.env.testing');

        $response = $this->post('/admin/appsetting', [
            'number_test' => '200',
            'filename' => 'env.testing'
        ]);

        if (strpos($content, 'NUMBER_TEST="200"') === false) {
            $response->assertSessionHas(['flash_danger' => __('appsetting::alerts.backend.appsetting.updated_error')]);
        }
    }
}
