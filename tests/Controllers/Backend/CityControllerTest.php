<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\City\Entities\City;
use Modules\State\Entities\State;
use Modules\Township\Entities\Township;

use App\Enums\Table;

class CityControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCityIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/city');
        $response->assertStatus(200);
    }
    public function testCityCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/city/create');
        $response->assertStatus(200);
    }

    public function testCityCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/city', []);

        $response->assertSessionHasErrors(['name', 'state_id']);

        $name = 'a';
        $state = factory(State::class)->create(); 
        for($i=0 ; $i < 200 ;$i++) $name .= 'a';
        $response=$this->post('/admin/city',[
            'name' => $name,
            'state_id' => $state->id 
        ]);


        $response->assertSessionHasErrors(['name']);

        $state = factory(State::class)->create(); 

        $this->post('/admin/city', [
            'name' => 'Myanmar',
            'state_id' => $state->id
        ]);

        $response = $this->post('/admin/city', [
            'name' => 'Myanmar',
            'state_id' => '9'
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testCityCreateSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/city', [
            'name' => 'test',
            'state_id' => '9'
        ]);

        $this->assertDatabaseHas(
            Table::CITY,
            [
                'name' => 'test',
                'state_id' => '9'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('city::alerts.backend.city.created')]);
    }
    public function testCityUpdate()
    {
        $this->loginAsAdmin();
        
        $city  = factory(City::class)->create();

        $response = $this->get('/admin/city/'.$city->id.'/edit');

        $response->assertStatus(200);
    }

    public function testCityUpdateValidationError()
    {
        $this->loginAsAdmin();

        $city = factory(City::class)->create();

        $response = $this->patch('/admin/city/'.$city->id, []);

        $response->assertSessionHasErrors(['name', 'state_id']);

        factory(City::class)->create([
            'name' => 'bago',
            'state_id' => '9'
        ]);

        $existingCity = factory(City::class)->create([
            'name' => 'Yangon',
            'state_id' => '1'
        ]);

        $this->patch('/admin/city/'.$existingCity->id, [
            'name' => 'bago',
            'state_id' => '9'
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testCityUpdateSuccess()
    {
        $this->loginAsAdmin();

        $city = factory(City::class)->create();

        $response = $this->patch('/admin/city/'.$city->id, [
            'name' => 'Yangon',
            'state_id' => '2'
        ]);

        $this->assertDatabaseHas(
            Table::CITY,
            [
                'name' => 'Yangon',
                'state_id' => '2'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('city::alerts.backend.city.updated')]);
    }
    public function testCityDeleteFail()
    {
        $this->loginAsAdmin();
        $city = factory(city::class)->create();
        factory(Township::class)->create([
            'name' => 'Yangon',
            'city_id' => $city->id
        ]);

        $response = $this->delete("/admin/city/{$city->id}");
        
        $response->assertSessionHas(['flash_danger' => __('city::alerts.backend.city.not_deleted')]);
    }
    public function testCityDeleteSuccess()
    {
        $this->loginAsAdmin();

        $city = factory(City::class)->create();

        $response = $this->delete("/admin/city/{$city->id}");

        $response->assertSessionHas(['flash_success' => __('city::alerts.backend.city.deleted')]);
        $this->assertDatabaseMissing(Table::CITY, ['id' => $city->id, 'deleted_at' => null]);
    }
}
