<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\DeliverySupplier\Entities\DeliverySupplier;
use Modules\Country\Entities\Country;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use App\Models\Auth\Role;
use App\Models\Auth\User;
use App\Enums\Table;

class DeliverySupplierControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testDeliverySupplierIndexSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/deliverysupplier');

        $response->assertStatus(200);
    }

    public function testDeliverySupplierCreate()
    {
        $this->loginAsAdmin();
        
        $response = $this->get('/admin/deliverysupplier/create');

        $response->assertStatus(200);
    }

    public function testDeliverySupplierCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/deliverysupplier', []);

        $response->assertSessionHasErrors(['name', 'mobile','type','country_id', 'state_id','city_id','township_id','email','password','password_confirmation','active','confirmed']);

        $township = factory(Township::class)->create();

        $name = 'a';
        for ($i=0 ; $i < 200 ;$i++) {
            $name .= 'a';
        }
        $response = $this->post('/admin/deliverysupplier', [
            'name' => $name,
            'mobile' => '09954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'
        ]);

        $response->assertSessionHasErrors(['name']);

        $response = $this->post('/admin/deliverysupplier', [
            'name' => 'testing',
            'mobile' => '1234567',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['mobile']);

        $response = $this->post('/admin/deliverysupplier', [
            'name' => 'testing',
            'mobile' => '09954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing1234',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['email']);

        Role::create(['name' => config('access.users.default_role')]);
        $this->post('/admin/deliverysupplier', [
            'name' => 'testing',
            'mobile' => '959954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);

        $response = $this->post('/admin/deliverysupplier', [
            'name' => 'testing',
            'mobile' => '959954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['email']);
    }

    public function testDeliverySupplierCreateSuccess()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();

        Role::create(['name' => config('access.users.default_role')]);

        $response = $this->post('/admin/deliverysupplier', [
              'name' => 'testing',
              'mobile' => '959954555372',
              'type' => '1',
              'country_id' => $township->city->state->country_id,
              'state_id' => $township->city->state_id,
              'city_id' => $township->city_id,
              'township_id' => $township->id,
              'email' => 'testing@gmail.com',
              'password' => 'secret',
              'password_confirmation' => 'secret',
              'active' => '1',
              'confirmed' => '1'
        ]);

        $this->assertDatabaseHas(
            'users',
            [
                'email'=>'testing@gmail.com'
            ]
        );

        $this->assertDatabaseHas(
            Table::DELIVERY_SUPPLIER,
            [
                'name' => 'testing',
                'mobile' => '959954555372',
                'type' => 1,
            ]
        );

        $response->assertSessionHas(['flash_success' => __('deliverysupplier::alerts.backend.deliverysupplier.created')]);
    }

    public function testDeliverySupplierUpdate()
    {
        $this->loginAsAdmin();
        
        $supplier = factory(DeliverySupplier::class)->create();

        $response = $this->get('/admin/deliverysupplier/'.$supplier->id.'/edit');

        $response->assertStatus(200);
    }

    public function testDeliverySupplierUpdateValidationError()
    {
        $this->loginAsAdmin();

        $customer = factory(DeliverySupplier::class)->create();

        $response = $this->patch('/admin/deliverysupplier/'.$customer->id, []);

        $response->assertSessionHasErrors(['name','mobile','email','type','country_id','city_id','township_id']);

        $user = factory(User::class)->create([
            'email' => 'testing@gmail.com'
        ]);

        factory(DeliverySupplier::class)->create([
            'user_id' => $user->id
        ]);

        $existingDeliverySupplier = factory(DeliverySupplier::class)->create();

        $this->patch('/admin/deliverysupplier/'.$existingDeliverySupplier->id, [
            'name' => 'Testing',
            'mobile' => '959954555372',
            'email' => 'testing@gmail.com'
        ]);

        $response->assertSessionHasErrors(['email']);
    }

    public function testDeliverySupplierUpdateSuccess()
    {
        $this->loginAsAdmin();

        $supplier = factory(DeliverySupplier::class)->create();

        $township = factory(Township::class)->create();

        $response = $this->patch('/admin/deliverysupplier/'.$supplier->id, [
            'name' => 'Supplier1',
            'mobile' => '959954555372',
            'email' => 'testing@gmail.com',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id
        ]);

        $this->assertDatabaseHas(
            Table::DELIVERY_SUPPLIER,
            [
                'name' => 'Supplier1',
                'mobile' => '959954555372'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('deliverysupplier::alerts.backend.deliverysupplier.updated')]);
    }

    public function testDeliverySupplierDeleteSuccess()
    {
        $this->loginAsAdmin();

        $supplier = factory(DeliverySupplier::class)->create();

        $response = $this->delete("/admin/deliverysupplier/{$supplier->id}");

        $response->assertSessionHas(['flash_success' => __('deliverysupplier::alerts.backend.deliverysupplier.deleted')]);
        $this->assertDatabaseMissing(Table::DELIVERY_SUPPLIER, ['id' => $supplier->id, 'deleted_at' => null]);
    }

    public function testDeliverySupplierShowPage()
    {
        $this->loginAsAdmin();

        $supplier = factory(DeliverySupplier::class)->create();
        
        $response = $this->get("/admin/deliverysupplier/{$supplier->id}");

        $response->assertStatus(200);
    }

}
