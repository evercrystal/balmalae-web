<?php

namespace Tests\Controllers\Backend;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\City\Entities\City;
use Modules\Route\Entities\Route;
use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Entities\PivotRoute;
use App\Enums\Table;

class RouteControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRouteIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/route');
        $response->assertStatus(200);
    }

    public function testRouteCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/route/create');
        $response->assertStatus(200);
    }

    public function testRouteCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/route', []);

        $response->assertSessionHasErrors(['from_city_id', 'to_city_id']);

    }

    public function testRouteCreateSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/route', [
            'from_city_id' => '1',
            'to_city_id' => '2',
            'is_active'  => '1'
        ]);

        $this->assertDatabaseHas(
            Table::ROUTE,
            [
                'from_city_id' => '1',
                'to_city_id' => '2',
                'is_active'  => '1'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('route::alerts.backend.route.created')]);
    }

    public function testRouteUpdate()
    {
        $this->loginAsAdmin();
        
        $route  = factory(Route::class)->create();

        $response = $this->get('/admin/route/'.$route->id.'/edit');

        $response->assertStatus(200);
    }

    public function testCityUpdateValidationError()
    {
        $this->loginAsAdmin();

        $route = factory(Route::class)->create();

        $response = $this->patch('/admin/route/'.$route->id, []);

        $response->assertSessionHasErrors(['from_city_id', 'to_city_id']);

    }

    public function testRouteUpdateSuccess()
    {
        $this->loginAsAdmin();

        $route = factory(Route::class)->create();

        $response = $this->patch('/admin/route/'.$route->id, [
            'from_city_id' => '1',
            'to_city_id' => '1',
            'is_active' => '2'
        ]);

        $this->assertDatabaseHas(
            Table::ROUTE,
            [
                'from_city_id' => '1',
                'to_city_id' => '1',
                'is_active' => '2'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('route::alerts.backend.route.updated')]);
    }

    public function testRouteDeleteFail()
    {
        $this->loginAsAdmin();

        $route = factory(Route::class)->create();

        $subRoute = factory(SubRoute::class)->create();

        factory(PivotRoute::class)->create([
            'route_id' => $route->id,
            'sub_route_id' => $subRoute->id
        ]);

        $response = $this->delete("/admin/route/{$route->id}");
        
        $response->assertSessionHas(['flash_danger' => __('route::alerts.backend.route.not_deleted')]);
    }

    public function testRouteDeleteSuccess()
    {
        $this->loginAsAdmin();

        $route = factory(Route::class)->create();

        $response = $this->delete("/admin/route/{$route->id}");

        $response->assertSessionHas(['flash_success' => __('route::alerts.backend.route.deleted')]);
        $this->assertDatabaseMissing(Table::ROUTE, ['id' => $route->id, 'deleted_at' => null]);
    }
}
