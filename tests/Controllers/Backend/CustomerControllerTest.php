<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Entities\CustomerLocation;
use Modules\Country\Entities\Country;
use Modules\City\Entities\City;
use Modules\Township\Entities\Township;
use App\Models\Auth\Role;
use App\Models\Auth\User;
use App\Enums\Table;

class CustomerControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCustomerIndexSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/customer');

        $response->assertStatus(200);
    }

    public function testCustomerCreate()
    {
        $this->loginAsAdmin();
        
        $response = $this->get('/admin/customer/create');

        $response->assertStatus(200);
    }

    public function testCustomerCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/customer', []);

        $response->assertSessionHasErrors(['name', 'mobile','type','country_id','city_id','township_id','email','password','password_confirmation','active','confirmed']);

        $township = factory(Township::class)->create();

        $name = 'a';
        for ($i=0 ; $i < 200 ;$i++) {
            $name .= 'a';
        }
        $response = $this->post('/admin/customer', [
            'name' => $name,
            'mobile' => '09954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'
        ]);

        $response->assertSessionHasErrors(['name']);

        $response = $this->post('/admin/customer', [
            'name' => 'testing',
            'mobile' => '1234567',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['mobile']);

        $response = $this->post('/admin/customer', [
            'name' => 'testing',
            'mobile' => '09954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id,
            'email' => 'testing1234',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['email']);

        Role::create(['name' => config('access.users.default_role')]);
        $this->post('/admin/customer', [
            'name' => 'testing',
            'mobile' => '959954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);

        $response = $this->post('/admin/customer', [
            'name' => 'testing',
            'mobile' => '959954555372',
            'type' => 1,
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id,
            'email' => 'testing@gmail.com',
            'password' => '123456',
            'password_confirmation' => '123456',
            'active' => '1',
            'confirmed' => '1'

        ]);
        $response->assertSessionHasErrors(['email']);
    }

    public function testCustomerCreateSuccess()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();

        Role::create(['name' => config('access.users.default_role')]);

        $response = $this->post('/admin/customer', [
              'name' => 'testing',
              'mobile' => '959954555372',
              'type' => '1',
              'country_id' => $township->city->state->country_id,
              'state_id' => $township->city->state_id,
              'city_id' => $township->city_id,
              'township_id' => $township->id,
              'email' => 'testing@gmail.com',
              'password' => 'secret',
              'password_confirmation' => 'secret',
              'active' => '1',
              'confirmed' => '1'
        ]);

        $this->assertDatabaseHas(
            'users',
            [
                'email'=>'testing@gmail.com'
            ]
        );

        $this->assertDatabaseHas(
            Table::CUSTOMER,
            [
                'name' => 'testing',
                'mobile' => '959954555372',
                'type' => 1,
            ]
        );

        $this->assertDatabaseHas(
            Table::CUSTOMER_LOCATION,
            [
                'country_id' => $township->city->state->country_id,
                'city_id' => $township->city_id,
                'township_id' => $township->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('customer::alerts.backend.customer.created')]);
    }

    public function testCustomerUpdate()
    {
        $this->loginAsAdmin();
        
        $customer = factory(Customer::class)->create();

        $response = $this->get('/admin/customer/'.$customer->id.'/edit');

        $response->assertStatus(200);
    }

    public function testCustomerUpdateValidationError()
    {
        $this->loginAsAdmin();

        $customer = factory(Customer::class)->create();

        $response = $this->patch('/admin/customer/'.$customer->id, []);

        $response->assertSessionHasErrors(['name','mobile','email']);

        $user = factory(User::class)->create([
            'email' => 'testing@gmail.com'
        ]);

        factory(Customer::class)->create([
            'user_id' => $user->id
        ]);

        $existingCustomer = factory(Customer::class)->create();

        $this->patch('/admin/customer/'.$existingCustomer->id, [
            'name' => 'Testing',
            'mobile' => '959954555372',
            'email' => 'testing@gmail.com'
        ]);

        $response->assertSessionHasErrors(['email']);
    }

    public function testCustomerUpdateSuccess()
    {
        $this->loginAsAdmin();

        $customer = factory(Customer::class)->create();

        $response = $this->patch('/admin/customer/'.$customer->id, [
            'name' => 'Customer1',
            'mobile' => '959954555372',
            'email' => 'testing@gmail.com',
        ]);

        $this->assertDatabaseHas(
            Table::CUSTOMER,
            [
                'name' => 'Customer1',
                'mobile' => '959954555372'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('customer::alerts.backend.customer.updated')]);
    }

    public function testCustomerDeleteSuccess()
    {
        $this->loginAsAdmin();

        $customer = factory(Customer::class)->create();

        $response = $this->delete("/admin/customer/{$customer->id}");

        $response->assertSessionHas(['flash_success' => __('customer::alerts.backend.customer.deleted')]);
        $this->assertDatabaseMissing(Table::CUSTOMER, ['id' => $customer->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing(Table::CUSTOMER_LOCATION, ['customer_id' => $customer->id, 'deleted_at' => null]);
    }

    public function testCustomerShowPage()
    {
        $this->loginAsAdmin();

        $customer = factory(Customer::class)->create();
        
        $response = $this->get("/admin/customer/{$customer->id}");

        $response->assertStatus(200);
    }

    public function testCustomerNewLocationCreatePage()
    {
        $this->loginAsAdmin();

        $customer = factory(Customer::class)->create();
        
        $response = $this->get("/admin/customer/{$customer->id}/location-create");

        $response->assertStatus(200);
    }

    public function testCustomerNewLocationStore()
    {
        $this->loginAsAdmin();

        $customer = factory(Customer::class)->create();
        
        $response = $this->get("/admin/customer/{$customer->id}/location-create");

        $response->assertStatus(200);
    }

    public function testCustomerLocationCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/customer/{$customer->id}/location-create', []);

        $response->assertSessionHasErrors(['country_id','city_id','township_id']);
    }

    public function testCustomerNewLocationCreateSuccess()
    {
        $this->loginAsAdmin();
        
        $customer = factory(Customer::class)->create();

        $township = factory(Township::class)->create();

        $response = $this->post("/admin/customer/{$customer->id}/location-create", [
            'country_id' => $township->city->state->country_id,
            'state_id' => $township->city->state_id,
            'city_id' => $township->city_id,
            'township_id' => $township->id
        ]);

        $this->assertDatabaseHas(
            Table::CUSTOMER_LOCATION,
            [
                'customer_id' => $customer->id,
                'country_id' => $township->city->state->country_id,
                'state_id' => $township->city->state_id,
                'city_id' => $township->city_id,
                'township_id' => $township->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('customer::alerts.backend.customer.location_created')]);
    }

    public function testCustomerNewLocationUpdatePage()
    {
        $this->loginAsAdmin();
        
        $location = factory(CustomerLocation::class)->create();

        $response = $this->get('/admin/customer-location/'.$location->id.'/edit');

        $response->assertStatus(200);
    }

    public function testCustomerLocationUpdateValidationError()
    {
        $this->loginAsAdmin();

        $location = factory(CustomerLocation::class)->create();

        $response = $this->patch('/admin/customer-location/'.$location->id, []);

        $response->assertSessionHasErrors(['country_id','city_id','township_id']);
    }

    public function testCustomerLocationUpdateSuccess()
    {
        $this->loginAsAdmin();

        $location = factory(CustomerLocation::class)->create();

        $township = factory(Township::class)->create();

        $response = $this->patch('/admin/customer-location/'.$location->id, [
            'country_id' => $township->city->state->country_id,
            'city_id' => $township->city_id,
            'state_id' => $township->city->state_id,
            'township_id' => $township->id
        ]);

        $this->assertDatabaseHas(
            Table::CUSTOMER_LOCATION,
            [
                'country_id' => $township->city->state->country_id,
                'city_id' => $township->city_id,
                'state_id' => $township->city->state_id,
                'township_id' => $township->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('customer::alerts.backend.customer.location_updated')]);
    }

    public function testCustomerLocationDeleteSuccess()
    {
        $this->loginAsAdmin();

        $location = factory(CustomerLocation::class)->create();

        $response = $this->delete("/admin/customer-location/{$location->id}");

        $response->assertSessionHas(['flash_success' => __('customer::alerts.backend.customer.location_deleted')]);
        $this->assertDatabaseMissing(Table::CUSTOMER_LOCATION, ['is' => $location->id, 'deleted_at' => null]);
    }
}
