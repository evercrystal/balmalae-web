<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\Township\Entities\Township;
use Modules\SubRoute\Entities\SubRoute;
use Modules\SubRoute\Entities\PivotRoute;
use Modules\Route\Entities\Route;
use Tests\TestCase;
use App\Enums\Table;

class SubRouteControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSubRouteIndexSuccess()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/subroute');
        $response->assertStatus(200);
    }

    public function testSubRouteCreate()
    {
        $this->loginAsAdmin();
        $response = $this->get('/admin/subroute/create');
        $response->assertStatus(200);
    }

    public function testSubRouteCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/subroute', []);

        $response->assertSessionHasErrors(['name', 'from_township_id', 'to_township_id', 'type','routes', 'max_weight','max_height','max_width', 'mmk_price', 'usd_price']);

    }

    public function testSubRouteCreateSuccess()
    {
        $this->loginAsAdmin();

        $township = factory(Township::class)->create();

        $route = factory(Route::class)->create();

        $routeArr = $route->pluck('id')->toArray();

        $response = $this->post('/admin/subroute', [
            'name' => 'Testing',
            'to_township_id' => $township->id,
            'from_township_id' => $township->id,
            'routes'         => $routeArr,
            'type'             => 1,
            'max_weight'       => 10,
            'max_height'       => 10,
            'max_width'        => 10,
            'mmk_price'        => 10,
            'usd_price'        => 10, 
        ]);

        $this->assertDatabaseHas(
            Table::SUB_ROUTE,
            [
                'name' => 'Testing',
                'to_township_id' => $township->id,
                'from_township_id' => $township->id,
                'type'             => 1,
                'max_weight'       => 10,
                'max_height'       => 10,
                'max_width'        => 10,
                'mmk_price'        => 10,
                'usd_price'        => 10, 
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_ROUTE,
            [
                'route_id' => $route->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.created')]);

        $route2 = factory(Route::class)->create();

        $routeArrs = Route::pluck('id')->toArray();

        $response = $this->post('/admin/subroute', [
            'name' => 'Testing',
            'to_township_id' => $township->id,
            'from_township_id' => $township->id,
            'routes'         => $routeArrs,
            'type'             => 1,
            'max_weight'       => 10,
            'max_height'       => 10,
            'max_width'        => 10,
            'mmk_price'        => 10,
            'usd_price'        => 10, 
        ]);

        $this->assertDatabaseHas(
            Table::SUB_ROUTE,
            [
                'name' => 'Testing',
                'to_township_id' => $township->id,
                'from_township_id' => $township->id,
                'type'             => 1,
                'max_weight'       => 10,
                'max_height'       => 10,
                'max_width'        => 10,
                'mmk_price'        => 10,
                'usd_price'        => 10, 
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_ROUTE,
            [
                'route_id' => $route->id
            ]
        );

        $this->assertDatabaseHas(
            Table::PIVOT_ROUTE,
            [
                'route_id' => $route2->id
            ]
        );

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.created')]);
    }


    public function testSubRouteUpdateValidationError()
    {
        $this->loginAsAdmin();

        $subroute = factory(SubRoute::class)->create();

        $response = $this->patch('/admin/subroute/'.$subroute->id, []);
        $response->assertSessionHasErrors(['name', 'from_township_id', 'to_township_id', 'type','routes', 'max_weight','max_height','max_width', 'mmk_price', 'usd_price']);

    }

    public function testRouteUpdateSuccess()
    {
        $this->loginAsAdmin();

        $route1 = factory(Route::class)->create();
        $route2 = factory(Route::class)->create();

        $subroute = factory(SubRoute::class)->create();

        $pivotRoute1 = factory(PivotRoute::class)->create([
            'route_id' => $route1->id,
            'sub_route_id' => $subroute->id
        ]);

        $pivotRoute2 = factory(PivotRoute::class)->create([
            'route_id' => $route2->id,
            'sub_route_id' => $subroute->id
        ]);

        $routeArr[] = $route1->id;

        $response = $this->patch('/admin/subroute/'.$subroute->id, [
            'name' => 'Testing',
            'to_township_id' => $subroute->from_township_id,
            'from_township_id' => $subroute->to_township_id,
            'routes'           => $routeArr,
            'type'             => 1,
            'max_weight'       => 10,
            'max_height'       => 10,
            'max_width'        => 10,
            'mmk_price'        => 10,
            'usd_price'        => 10, 
        ]);

        $this->assertDatabaseHas(
            Table::SUB_ROUTE,
            [
                'name' => 'Testing',
                'to_township_id' => $subroute->from_township_id,
                'from_township_id' => $subroute->to_township_id,
                'type'             => 1,
                'max_weight'       => 10,
                'max_height'       => 10,
                'max_width'        => 10,
                'mmk_price'        => 10,
                'usd_price'        => 10, 
            ]
        );

        $this->assertDatabaseMissing(Table::PIVOT_ROUTE, ['route_id' => $route2->id,'sub_route_id' => $subroute->id]);

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.updated')]);
    }

    public function testSubRouteDeleteSuccess()
    {
        $this->loginAsAdmin();

        $route1 = factory(Route::class)->create();

        $subroute = factory(SubRoute::class)->create();

        $pivotRoute1 = factory(PivotRoute::class)->create([
            'route_id' => $route1->id,
            'sub_route_id' => $subroute->id
        ]);

        $response = $this->delete("/admin/subroute/{$subroute->id}");

        $response->assertSessionHas(['flash_success' => __('subroute::alerts.backend.subroute.deleted')]);
        $this->assertDatabaseMissing(Table::SUB_ROUTE, ['id' => $subroute->id, 'deleted_at' => null]);
        $this->assertDatabaseMissing(Table::PIVOT_ROUTE, ['route_id' => $route1->id,'sub_route_id' => $subroute->id]);
    }
}
