<?php

namespace Tests\Controllers\Backend;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Modules\Country\Entities\Country;
use Modules\City\Entities\City;
use Modules\State\Entities\State;
use App\Enums\Table;

class CountryControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCountryIndexSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->get('/admin/country');

        $response->assertStatus(200);
    }

    public function testPageCreate()
    {
        $this->loginAsAdmin();
        
        $response = $this->get('/admin/country/create');

        $response->assertStatus(200);
    }

    public function testCountryCreateValidationError()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/country', []);

        $response->assertSessionHasErrors(['name', 'country_code']);

        $name = 'a';
        for($i=0 ; $i < 200 ;$i++) $name .= 'a';

        $response = $this->post('/admin/country', [
            'name' => $name,
            'country_code' => '111',
        ]);

        $response->assertSessionHasErrors(['name']);

        $countryCode = '1';
        for($i=0 ; $i < 200 ;$i++) $countryCode .= '1';

        $response = $this->post('/admin/country', [
            'name' => 'Myanmar',
            'country_code' => $countryCode,
        ]);

        $response->assertSessionHasErrors(['country_code']);

        $this->post('/admin/country', [
            'name' => 'Myanmar',
            'country_code' => '959'
        ]);

        $response = $this->post('/admin/country', [
            'name' => 'Myanmar',
            'country_code' => '959'
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testCountryCreateSuccess()
    {
        $this->loginAsAdmin();

        $response = $this->post('/admin/country', [
            'name' => 'Myanmar',
            'country_code' => '959'
        ]);

        $this->assertDatabaseHas(
            Table::COUNTRY,
            [
                'name' => 'Myanmar',
                'country_code' => '959'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('country::alerts.backend.country.created')]);
    }

    public function testCountryUpdate()
    {
        $this->loginAsAdmin();
        
        $country = factory(Country::class)->create();

        $response = $this->get('/admin/country/'.$country->id.'/edit');

        $response->assertStatus(200);
    }

    public function testPageUpdateValidationError()
    {
        $this->loginAsAdmin();

        $country = factory(Country::class)->create();

        $response = $this->patch('/admin/country/'.$country->id, []);

        $response->assertSessionHasErrors(['name', 'country_code']);

        factory(Country::class)->create([
            'name' => 'Myanmar',
            'country_code' => '959'
        ]);

        $existingCountry = factory(Country::class)->create([
            'name' => 'Singapore',
            'country_code' => '5'
        ]);

        $this->patch('/admin/country/'.$existingCountry->id, [
            'name' => 'Myanmar',
            'country_code' => '959'
        ]);

        $response->assertSessionHasErrors(['name']);
    }

    public function testPageUpdateSuccess()
    {
        $this->loginAsAdmin();

        $country = factory(Country::class)->create();

        $response = $this->patch('/admin/country/'.$country->id, [
            'name' => 'Myanmar',
            'country_code' => '959'
        ]);

        $this->assertDatabaseHas(
            Table::COUNTRY,
            [
                'name' => 'Myanmar',
                'country_code' => '959'
            ]
        );

        $response->assertSessionHas(['flash_success' => __('country::alerts.backend.country.updated')]);
    }

    public function testCountryDeleteNotSuccess()
    {
        $this->loginAsAdmin();

        $country = factory(Country::class)->create();
        factory(State::class)->create([
            'name' => 'Yangon',
            'country_id' => $country->id
        ]);

        $response = $this->delete("/admin/country/{$country->id}");
        
        $response->assertSessionHas(['flash_danger' => __('country::alerts.backend.country.not_deleted')]);
    }
    public function testCountryDeleteSuccess()
    {
        $this->loginAsAdmin();

        $country = factory(Country::class)->create();
    

        $response = $this->delete("/admin/country/{$country->id}");

        $response->assertSessionHas(['flash_success' => __('country::alerts.backend.country.deleted')]);
        $this->assertDatabaseMissing(Table::COUNTRY, ['id' => $country->id, 'deleted_at' => null]);
    }
}